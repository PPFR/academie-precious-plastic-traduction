# Fichier de traitement à priori des fichiers de documentation
# L'objectif est
# 1)de nettoyer les fichiers préfixés par OK_ (notant qu'ils ont été intégrés dans la doc web)
# 2)de corriger l'erreur sur la partie métadonnée introduite par l'éditeur Nextcloud

import os
import fileinput

def process(filename):
    extension = filename.split(".")[-1]
    name = filename.split("/")[-1]
    if extension!="md":
        return
    
    if "OK_" in filename:
        os.remove(filename)
        print("suppression de ",filename)
        return
    
    with open(filename,"r") as f:
        ligne = f.readline()
        if "---" in ligne:
            ligne = f.readline()
            ligne = f.readline()
            if "#" in ligne:
                print("Je traite le fichier:", f)
                print("soucis>", ligne)
                
                with open("/tmp/"+name,"w") as g:
                    with open(filename,"r") as f2:
                        g.write(f2.readline())
                        f2.readline()
                        ligne = f2.readline()
                        ligne2=ligne.split(" ")
                        for i in range(1,(len(ligne2)-1)//2+1):
                            print(i,":",ligne2[2*i-1],":",ligne2[2*i])
                            g.write(ligne2[2*i-1]+ligne2[2*i]+"\n")
                        g.write("---")
                        g.write(f2.read())
                with open("/tmp/"+name,"r") as g:
                    with open(filename,"w") as f2:
                        f2.write(g.read())
                    
        
for mddir in os.walk("../docs"):
    processed_path = mddir[0]
    files = (processed_path+"/"+mdfile for mdfile in mddir[2])
    for f in files:
        process(f)
        
    
    
