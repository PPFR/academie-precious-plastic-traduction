---
id: download
title: Download
sidebar_label: Download Here
title: Download
sidebar_label: Download-Kit
---

<style>
:root {
  --highlight: #ffe084;
  --links: #29bbe3;
  --hover: rgb(131, 206, 235);
}
</style>

# Les kits Precious Plastic

Nous avons créé un **très gros** fichier .zip. Il a un volume proche de 700 Mo et contient tout ce dont vous avez besoin pour commencer: plans détaillés, posters, étiquettes, modèles CAO etc... C'est sympa. Mais vous n'aurez probablement pas besoin de toutes ces informations. C'est pourquoi nous avons également créé des kits de démarrage, de plus petits ensembles centrés sur des informations spécifiques et des exemples, plus simples à comprendre et à digérer. Jetez un coups d'oeil à nos [kits de démarrage](https://preciousplastic.com/starterkits/overview.html) pour voir ce qui pourrait vous convenir. Merci de nous envoyer une image quand vous aurez fini :)


<br><br>
<center>
<a id="kit" class="downloadButton js-getLinkDetails" href="https://api.github.com/repos/ONEARMY/precious-plastic-kit/releases">TÉLÉCHARGER LE KIT V4 🤙</a><br>
<span align="center" id="kit-downloadCount" class="fileStats">0</span>
<span align="center" id="kit-fileSize" class="fileStats">0</span>
<span align="center" id="kit-version" class="fileStats">0</span>
</center>
<img src="assets/download/arrow.png"/>
<br>
## Kits de démarrage

| Logo  | Type       | Taille du fichier | Version | Téléchargement |
|---|----------------|--------|--------|--------|
| <img src="assets/universe/badge-workspace.png" width="60"/>| Espace de broyage<br>[consulter](https://preciousplastic.com/starterkits/showcase/shredder.html)  |  <span id="shredder-fileSize"></span> | <span id="shredder-version"></span>  | <a id="shredder" class="small downloadButton js-getLinkDetails" href="https://api.github.com/repos/ONEARMY/precious-plastic-starterkit-shredder/releases">TÉLÉCHARGER</a><div id="shredder-downloadCount"></div> |
| <img src="assets/universe/badge-workspace.png" width="60"/>| Espace d'injection<br>[consulter](https://preciousplastic.com/starterkits/showcase/injection.html)   | <span id="injection-fileSize"></span> | <span id="injection-version"></span>       | <a id="injection" class="small downloadButton js-getLinkDetails" href="https://api.github.com/repos/ONEARMY/precious-plastic-starterkit-injection/releases">TÉLÉCHARGER</a><div id="injection-downloadCount"></div>   |
| <img src="assets/universe/badge-workspace.png" width="60"/>| Espace d'extrusion<br>[consulter](https://preciousplastic.com/starterkits/showcase/extrusion.html)   | <span id="extrusion-fileSize"></span> | <span id="extrusion-version"></span>      | <a id="extrusion" class="small downloadButton js-getLinkDetails" href="https://api.github.com/repos/ONEARMY/precious-plastic-starterkit-extrusion/releases">TÉLÉCHARGER</a><div id="extrusion-downloadCount"></div>  |
| <img src="assets/universe/badge-workspace.png" width="60"/>| Espace de pressage</a>[consulter](https://preciousplastic.com/starterkits/showcase/sheetpress.html)  | <span id="sheetpress-fileSize"></span> | <span id="sheetpress-version"></span>        | <a id="sheetpress" class="small downloadButton js-getLinkDetails" href="https://api.github.com/repos/ONEARMY/precious-plastic-starterkit-sheetpress/releases">TÉLÉCHARGER</a><div id="sheetpress-downloadCount"></div>    |
| <img src="assets/universe/badge-workspace.png" width="60"/>| Espace mixte<br>[consulter](https://preciousplastic.com/starterkits/showcase/mix.html)  | <span id="mix-fileSize"></span> | <span id="mix-version"></span>      | <a id="mix" class="small downloadButton js-getLinkDetails" href="https://api.github.com/repos/ONEARMY/precious-plastic-starterkit-mix/releases">TÉLÉCHARGER</a><div id="mix-downloadCount"></div>    |
| <img src="assets/universe/badge-community-point.png" width="60"/>| Point communautaire<br>[consulter](https://preciousplastic.com/starterkits/showcase/community-point.html)  | <span id="community-fileSize"></span> | <span id="community-version"></span>         | <a id="community" class="small downloadButton js-getLinkDetails" href="https://api.github.com/repos/ONEARMY/precious-plastic-starterkit-community/releases">TÉLÉCHARGER</a><div id="community-downloadCount"></div>    |
| <img src="assets/universe/badge-collection-point.png" width="60"/>| Point de collecte<br>[consulter](https://preciousplastic.com/starterkits/showcase/collection-point.html)  | <span id="collection-fileSize"></span> | <span id="collection-version"></span>       | <a id="collection" class="small downloadButton js-getLinkDetails" href="https://api.github.com/repos/ONEARMY/precious-plastic-starterkit-collection/releases">TÉLÉCHARGER</a><div id="collection-downloadCount"></div>     |
| <img src="assets/universe/badge-machine-shop.png" width="60"/>| Magasin à machines <br>[consulter](https://preciousplastic.com/starterkits/showcase/machine-shop.html)  | <span id="machine-fileSize"></span> | <span id="machine-version"></span>           | <a id="machine" class="small downloadButton js-getLinkDetails" href="https://api.github.com/repos/ONEARMY/precious-plastic-starterkit-machine/releases">TÉLÉCHARGER</a><div id="machine-downloadCount"></div>     |
