---
id: universe
title: Univers
sidebar_label: L'univers PP expliqué
---

<style>
:root {
  --highlight: #f090b3;
  --links: #f090b3;
  --hover: #f2a5c1;
}
</style>

<img src="../../assets/universe/universe.gif"/>    

# Comment fonctionne l’Univers

Hmm oui.. c’est une question un peu difficile. Il est peut-être préférable de commencer avec notre petit bout. 
**L’univers de Precious Plastic. Comment ça fonctionne, quels sont les rôles et les lignes directrices, comment pouvons-nous aider etc.** 

L’univers est organisé de manière à être un écosystème global, où des gens à travers le monde collaborent sur le recyclage du plastique. De manière à faire cela correctement il nous faut des personnes différentes avec des expertises différentes. Donc au sein de l’univers, il y différents rôles à choisir, certains plus techniques, d’autres plus sociaux. Ci-dessous un résumé des rôles que nous avons actuellement.



# Les Espaces de l’Univers
Il existe différents espaces au sein de l’Univers. Chaque espace joue un rôle spécifique. En ce sens, chacun peut se focaliser sur ses propres tâches mais travailler ensemble localement. Différents espaces ont différentes tâches et vous pouvez les reconnaitre grâce à leur icône et logo.

| Badge   |  Mission |
|----------|----------------------|
| <img src="../../assets/universe/badge-member.png" width="150"/>           | __Membre__ <br> Les membres Precious Plastic sont la première et dernière chaîne du maillon du recyclage plastique. Ils fournissent en déchets ménagers et achètent des produits recyclés Precious Plastic.    |
| <img src="../../assets/universe/badge-workspace.png" width="150"/>        |  __Atelier__ <br> Un atelier Precious Plastic est un lieu où le plastique est transformé de l’état de déchets à celui de matériau ou produit précieux. Il y a 5 ateliers : Broyage, Extrusion, Injection, Presse à Plaque et Mix. |
| <img src="../../assets/universe/badge-machine-shop.png" width="150"/>     |  __Fabricants de Machines__ <br> Les fabricants de machines produisent des moules, des parties de machines ou des machines prêtes à l’emploi pour d’autres ateliers et projets dans le réseau de recyclage local. |
| <img src="../../assets/universe/badge-community-point.png" width="150"/>  |  __Point Communautaire__ <br> Les Espaces Communautaires connectent  et font grandir le réseau local. Ils renforcent la communauté locale existante tout en lui apportant de plus en plus de monde. |
| <img src="../../assets/universe/badge-collection-point.png" width="150"/> |  __Point de Collecte__ <br> Les Points de Collecte rassemblent le plastique auprès des voisins, associations et entreprises pour être utilisés dans les Ateliers de Broyage.  |


<p class="note">Note:A l’heure actuelle, ce sont les Points de Collecte qui ont la tâche la plus rude. Leur mission requière beaucoup de travail et nécessite de sensibiliser les acteurs locaux, tout en ayant un business modèle très difficile (nous sommes toujours penchés sur la question). Ils s'appuient énormément sur la force humaine. Alors pensez à bien les choyer ! Apportez-leur des gâteaux! 🍪</p>


# Le nombre d’Espaces locaux

Il est difficile de dire combien d'Ateliers sont nécessaires à votre ville/département/région. Ca dépend beaucoup sur la densité de population et leur consommation de plastique. Mais pour vous donner une indication, vous pouvez voir ici le ratio entre les différents Espaces. A noter :Il y a un fort besoin de Points de Collecte pour rassembler du plastique, surtout au début pour éduquer la population. Et seulement 1 Point Communautaire. En ce sens, il y a un Espace où les ateliers peuvent se retrouver et collaborer.

<img src="../../assets/universe/number-spaces.jpg"/>


# Démarre ton Espace dans l’Univers

Il y a quelques éléments à prendre en considération lorsqu’on démarre un Espace.

- **Quelles sont vos compétences**: Le plus important concerne vos compétences et ce que vous aimez faire. Les Fabricants de Machines nécessitent des compétences et techniques très pointues, Les Points Communautaires ont une dimension beaucoup plus sociale. Chaque Esapce est bien différent, on vous conseille donc d’explorer et de les comparer dans la partie Kit de Démarrage de notre site. Ca devrait vous donner une bonne indication de ce qui vous correspond le mieux.

- **Quels sont les besoins locaux**: Combien d’Espaces sont déjà en place et quels sont les besoins dans mon département / ma région. Le mieux est de consulter notre carte, regarder autour de chez vous et prendre contact avec un Point Communautaire afin d’en apprendre plus sur l’écosystème local.

- **Quel est votre budget**: On aime beaucoup l’idée que des gens puissent subvenir à leurs besoins sur le long terme. Voir votre Atelier comme s’il s’agissait d’une entreprise aide beaucoup, mais chaque espace y parvient différemment. Certains nécessitent de gros investissements et d’autres pas. Prenez ça en considération avant de vous lancer

# Devenez un maillon de cet écosystème mondial

Qu’est ce que c’est agréable de faire partie d’une communauté mondiale constitué de personnes avec le même état d’esprit, vous pouvez trouver où dormir tout autour du monde :) Mais c’est aussi super utile. D’un côté vous pouvez utiliser notre [Discord](https://discordapp.com/invite/rnx7m4t) pour partager des idées, des suggestions ou trouver des réponses à vos questions. De l’autre côté nous avons notre [plateforme communautaire](https://community.preciousplastic.com) où vous pouvez trouver des infos, des modifications de machines ou des améliorations fournies par des membres du monde entier. Vous avez la possibilité de vous limiter à prendre ces informations, mais vous pouvez aussi rendre la pareille et partager avec nous ce que vous apprenez. Donc on travaille tous localement, mais on partage les connaissances mondialement.

<b>Envie de partager des retours d’experience, discuter de l’Univers ou en apprendre plus de la communauté ? Filez sur le salon [#Universe](https://discordapp.com/invite/QUw8A3w) sur Discord. On y discute là de votre place dans l’Univers, des rôles, de l’image de marque et de votre logo.</b>
