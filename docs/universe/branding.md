---
id: branding
title: Charte Graphique
sidebar_label: Charte Graphique
---

<style>
:root {
  --highlight: #f090b3;
  --links: #f090b3;
  --hover: #f2a5c1;
}
</style>

# La Charte Graphique derrière Precious Plastic

Precious Plastic est une communauté mondiale open-source. Cela veut dire que les gens peuvent prendre et faire ce qu’ils veulent. Nous avons donc des personnes, des groupes et nous au QG qui faisons du support graphique comme des posters, sites web ou autocollants. Mais nous aimons collaborer sur une échelle mondiale. Au lieu de faire plein de choses aléatoires, nous aimons garder les choses cohérentes. Si quelqu’un crée quelque chose, quelqu’un d’autre de l’autre côté de la planète peut l’utiliser aussi. Dans ce but, nous avons mis en place des guides pour le style Precious Plastic.

<img src="../../assets/universe/our-logos.png" />

# Nos différents logos
Nous utilisons 3 différents types de logos. Chacun a sa propre fonction.
1.**Logo Officiel**: Ce logo est uniquement utilisé par le QG. Vous pouvez le voir sur le site internet ainsi que sur les productions graphiques que nous réalisons. 
Nous utilisons un logo différent car nous n'opérons pas comme un Espace Precious Plastic. Nous faisons du développement et gérons l’infrastructure de cette communauté. Ce logo permet d’identifier ce qui est officiel. **N’utilisez pas ce logo.**

2.**Logo Communautaire**: Ce logo est utilisé pour les choses faites par la communauté pour la communauté. Par exemple des stickers, affiches, objets ou tee-shirts proposés par une communauté locale. Des choses dont tous les autres membres autour du monde peuvent se saisir pour les réutiliser.

3. **Logo Personnalisé**: Ces logos sont utilisés par les [Espace](../spaces) à travers le monde. Les Points Communautaires, les Ateliers, les Fabricants de Machines, et les Points de Collecte. Ce logo peut être personnalisé selon vos besoins avec votre nom, plus d’info dans le [chapitre suivant](../universe/yourlogo). Ces logos peuvent être utilisés pour promouvoir pour Espace, cartes de visites, pancarte à l'extérieur de votre atelier, comptes de réseaux sociaux etc. 

<img src="../../assets/universe/ourfonts.png" />

# Utilisation de nos polices de caractères

Nous utilisons principalement 3 polices différentes. 
Pour les titres, sous titres et texte écrit. Pour les titres nous utilisons notre police faites maison Precious Plastic, que vous pouvez retrouver dans le kit en téléchargement. Nous utilisons Varela Round pour les sous titres, comme ici dans l’academy. Nous préférons l’utiliser en gras et en majuscules. Elle est utilisée pour des posters et c’est aussi celle utilisée pour le logo ! 
Enfin, pour le texte nous utilisons des Polices Système qui sont déjà installées sur nos ordinateurs. Ça rend les choses plus faciles et utilise moins de bande passante. Le rendu est différent selon les appareils.


<img src="../../assets/universe/title-font.png" />


# Les couleurs de Precious Plastic
Les couleurs sont clés pour nous, on les adore. Elles sont amicales et représentent pour nous la diversité des plastiques broyés. 
Mais nous aimons aussi qu’elles restent bien organisées et rendent les choses claires. On ne les utilise pas de manière aléatoire, elles sont nichées dans notre ADN et Conception UX. Nous avons commencé à les utiliser pour l’Academie il y a de ça quelques années et avons continué à construire avec. Voici nos couleurs principales


## Couleurs de l’Académie
| Couleur    |  Utilisation et code hexa        | Couleur | Utilisation et code hexa                 |
|----------|---------------|--|--------|
| <img src="../../assets/universe/CDCFD5.jpg" width="100%" height="50px" /> | __Intro__ <br> #CDCFD5    | <img src="../../assets/universe/F6B67B.jpg" width="100%" height="50px"/> | __Business__ <br> #F6B67B	|
| <img src="../../assets/universe/95D2EF.jpg" width="100%" height="50px" /> | __Plastic__ <br> #95D2EF |  <img src="../../assets/universe/2DAE9E.jpg" width="100%" height="50px"/> | __Spaces__ <br> #2DAE9E |  
| <img src="../../assets/universe/F19093.jpg" width="100%" height="50px" /> | __Build__ <br> #F19093 |    <img src="../../assets/universe/798BC5.jpg" width="100%" height="50px"/> | __Research__ <br> #798BC5 |
| <img src="../../assets/universe/C0ADD4.jpg" width="100%" height="50px" /> | __Collect__ <br> #C0ADD4 |  <img src="../../assets/universe/F18DAF.jpg" width="100%" height="50px"/> | __Universe__ <br> #F18DAF |
| <img src="../../assets/universe/FCDE8A.jpg" width="100%" height="50px" /> | __Create__ <br> #FCDE8A |  <img src="../../assets/universe/CDCFD5.jpg" width="100%" height="50px"/> | __Support & Download__ <br> #CDCFD5 |

## Couleurs des Espaces
|  Couleur  |  Utilisation et code hexa          | Couleur  |  Utilisation et code hexa                   |
|----------|---------------|--|--------|
| <img src="../../assets/universe/8ec685.jpg" width="100%" height="50px" /> | __Community Points__ <br> #8FC487    | <img src="../../assets/universe/95D2EF.jpg" width="100%" height="50px"/> | __Workspaces__ <br> #95D2EF	|
| <img src="../../assets/universe/C0ADD4.jpg" width="100%" height="50px" /> | __Collection Points__ <br> #C0ADD4    | <img src="../../assets/universe/F19093.jpg" width="100%" height="50px"/> | __Machine shop__ <br> #F19093	|


# Faire des visuels pour Precious Plastic

Les visuels sont des outils très puissants pour communiquer un message. 
Nous les utilisons pour partager de l’information purement fonctionnelle comme les différentes températures de fontes ou les différents types de plastique. 
Nous les utilisons pour déclencher quelque chose chez les gens, qu’ils changent le regard qu’ils ont sur le plastique, comme par exemple avec cette campagne de posters des Points de Collecte. Nous l’utilisons également de manière digitale, sur nos réseaux sociaux et notre site internet. Il y a très certainement différentes applications et choses à faire, nous n’avons pas d’exemple ou de guide en profondeur pour tous les cas de figure. La meilleure chose à faire est de jeter un œil aux productions graphiques que nous fournissons, les prendre comme exemple ou en créer un template. 
Et si vous n’êtes pas sûr du fait que ca rende bien, partagez les dans le Discord, beaucoup de personnes seront ravis de vous donner leur opinion :)

<img src="../../assets/universe/posters.png" />

<b>Vous avez des questions, avez besoin d’aide pour votre poster ou souhaitez simplement partager ce que vous avez fait ? Allez sur le salon [#Universe](https://discordapp.com/invite/QUw8A3w) de notre Discord. On y discute là de votre place dans l’Univers, des rôles, de l’image de marque et de votre logo.</b>
