---
id: community-program
title: Programme Communautaire
sidebar_label: Programme Communautaire
---
![community-programm-cover](https://user-images.githubusercontent.com/17761877/113297730-4959ca80-92fb-11eb-802d-6a02f61ff471.jpg)

# Bienvenue dans le Programme Communautaire Precious Plastic

La problématique autour du plastique est vaste. Chez Precious Plastic, nous pensons qu'il ne peut être abordé que par un mouvement fort et uni de personnes et de projets collaborant ensemble.

Precious Plastic peut faire beaucoup pour aider les Espaces Precious Plastic autour du globe. Et à leur tour, les Espaces Precious Plastic du monde entier peuvent faire beaucoup pour aider à développer le mouvement Precious Plastic. Plus Precious Plastic (le mouvement) grandit, plus les Espaces Precious Plastic seront forts, plus l'impact sur la pollution plastique sera important.
<br>
> **Tip: Plus vous aidez Precious Plastic, plus Precious Plastic vous aidera!** 🤜 🤛
<br>
<br>

# Comment ça marche?
Le Programme Communautaire vise à aider et récompenser les personnes et les projets qui contribuent le plus au développement du mouvement Precious Plastic. Il existe différents niveaux, pré-requis et récompenses. Jetez un oeil ci-dessous pour comprendre comment vous pouvez en faire partie.
<br>
<br>
🚩 Type | 🤝 Pré-requis| 💫 Récompenses
--- | --- | ---
<b>Membre/Espace</b> <br> <br> <img style="margin-left: 0;" src="../../assets/build/community-program-member.png" width="200px"/> | - Être gentil<br>- Ne pas spammer |- Créer un profil <br>- Poster des How-To <br>- Trucs utiles <br>- Commenter (bientôt)
<b>Allié</b> <br> <br> <img style="margin-left: 0;" src="../../assets/build/all-spaces-with-no-member.png" width="200px"/> | - Les éléments précédents <br> - Obtenez 6 étoiles ou plus dans la [Checklist de l’Allié](#ally-checklist-) | - Créer une épingle sur la carte <br> - Éligible à la communication et aux ventes  <br> - Éligible à devenir un profil Vérifié <br> 
<b>Vérifié</b> <br> <br> <img style="margin-left: 0;" src="../../assets/build/community-program-verified.png" width="200px"/> | - Les éléments précédents <br> - 8+ étoiles sur la [Checklist de l’Allié](#ally-checklist-)  <br> - Entité Juridique depuis plus d’1 an <br>- 1500 abonnés sur le réseau social principal utilisé <br> - 2 employés à plein temps | - Badge vérifié   <br>- Canaux Slack vérifiés  <br>- Communication à venir  <br>- Opportunités de vente  <br>- Éligible pour les collaborations 



<br>
<br>
# Comment devenir un Allié ✅
Pour devenir un allié Precious Plastic, un Espace doit obtenir au moins 6 étoiles dans la Checklist de l’Allié. C’est une liste de choses que tout espace peut faire pour aider à promouvoir le mouvement Precious Plastic. Elle nous permet également de voir quels Espaces contribuent à faire grandir le mouvement. Quel est votre score? Vérifiez ci-dessous!

<iframe width="600px" height="1050px" src="//jsfiddle.net/PreciousPlastic/xubr3gLz/11/embedded/result/" allowfullscreen="allowfullscreen" allowpaymentrequest frameborder="0"></iframe>
<br>
<br>

# Comment devenir un Profil Vérifié  ⭐️
Nous examinons plusieurs facteurs objectifs lors de l'évaluation des projets pour devenir un espace de travail vérifié. En plus d'être un allié, nous exigeons d'avoir 8 étoiles ou plus sur la Checklist de l’Allié, une entité légale depuis plus d'un an, au moins 1500 adeptes sur votre compte de médias sociaux le plus actif, 2 employés rémunérés à temps plein ainsi que des références professionnelles. Chacune de ces exigences doit être remplie pour devenir un espace de travail vérifié. Ces catégories objectives nous permettent de déterminer la qualité (machines, produits, moules, ateliers, etc.), la fiabilité, les antécédents, la mentalité open-source, etc. Vous pouvez postuler pour devenir un espace de travail vérifié [ici](https://forms.monday.com/forms/094b47dcc2528236d552a1fcad7d46c6?r=use1).
<br>
<br>

##  💎 Ce que Precious Plastic peut faire pour les Espaces
Precious Plastic fournit déjà tous les outils et plateformes en accès libre pour que le monde entier puisse les utiliser. En plus de cela, Precious Plastic peut aider les Espaces autour du globe en les mettant en avant sur ses canaux de communication (130K+ combinés), en les mettant en avant sur les canaux de vente (IG store, Bazar et autres), et en proposant des opportunités de partenariat. Entre autres choses, Precious Plastic peut:

• Créer des postes IG, stories et guides mettant en avant les Espaces et le travail qui y est réalisé, à plus de 80 000 abonnés
• Créer des postes FB incluant les Espaces et leur travail à 50 000 abonnés
• Intégrer les Espaces dans la boutique en ligne IG Shopping, à venir, pour augmenter les ventes
• Collaborer sur des projets commerciaux et des partenariats avec les Espaces

![Precious Plastic can do for spaces](https://user-images.githubusercontent.com/17761877/113735716-cd95be80-96fc-11eb-906f-a080db811974.jpg)

<br>
<br>
##  🎁 Ce que les Espaces peuvent faire pour Precious Plastic
Les Espaces Precious Plastic peuvent grandement aider Precious Plastic en créditant et en mentionnant qu’ils font partie de Precious Plastic. Ainsi, plus de personnes peuvent entrer en interaction avec Precious Plastic, et aider à développer le mouvement. Entre autres choses les Espaces peuvent:

• Mentionner et créditer Precious Plastic sur leur site web
• Mentionner et créditer Precious Plastic sur leurs réseaux sociaux (par exemple: “Partie de la communauté @realPreciousPlastic”)
• Utiliser #preciousplastic sur leurs postes
• Utiliser notre générateur de logo
• Créer des How-To afin de partager le savoir acquis avec la communauté
• Utiliser une méthode proactive pour faire connaître le mouvement


![Precious Plastic mention](https://user-images.githubusercontent.com/17761877/114057728-1bdcc600-9893-11eb-9cae-904e0175c03d.jpg)


<br>
<br>
<br>
