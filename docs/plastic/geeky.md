---
id: nerdy
title: Devenir un Pro du Plastique
sidebar_label: Devenir Pro
---

<div class="videocontainer"> <iframe width="800" height="400" src="https://www.youtube.com/embed/T5lAKQij1F4" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe> </div>

<style> :root { --highlight: #ffe084; --links: #29bbe3; --hover: rgb(131, 206, 235); } </style>

# Devenir Super Pro du Plastique

<div class="videoChapters"> <div class="videoChaptersMain">

### **Approfondissons quelques connaissances sur ce matériel abondant.**

Quand le plastique est-il arrivé dans notre société, comment est-il passé d'un matériau de niche à être disponible dans tous les coins du monde ? Nous allons couvrir un peu d'histoire, et ensuite de science, avant d’expliquer pourquoi nous travaillons exclusivement avec des thermoplastiques. Nous allons passer en revue les méthodes de production industrielle ainsi que la fin du cycle de vie d’un objet plastique. Après avoir lu ceci, vous serez le geek ou la geekette de plastique (comme Jerry) 🤓 

> _Conseil d'expert_: être un professionnel du plastique dans votre communauté vous aidera, vous et votre entourage, à faire de meilleurs choix en matière de plastique. Le savoir, c'est le pouvoir !

</div> <div class="videoChaptersSidebar">

### Video Chapters

* General Knowledge on Plastic 00.36
* Industrial Production Methods 06.58
* The Afterlife of Plastics 14.56

</div> </div>

# **Connaissances générales sur le plastique**

Quand vous pensez au plastique, quelle est la première chose qui vous vient à l'esprit ? Un sac à provisions, un contenant de yaourt, une carte de crédit, un seau rose, non ? Eh bien, qu'est-ce que ces objets en plastique ont à voir avec une gomme, un chapeau en laine ou une chaise ? Ils appartiennent tous à la même classe de matériaux: les polymères. Les polymères sont de très longues molécules répétitives constituées de monomères: *poly* signifiant plusieurs, *mers* signifiant parties et *mono* signifiant l’unité. Ainsi, les polymères sont constitués de nombreuses unités identiques, imaginez qu'ils sont de longues chaînes répétitives. Leur différence n'étant pas la façon dont ils sont formés, mais les polymères dont ils sont composés.

![Polymerization](assets/plastic/polymerization.svg)

Il existe des matériaux à base de polymères naturels ayant toujours existé comme la laine, le caoutchouc, les cheveux, la cellulose, l'amidon, ainsi que des matériaux en polymères synthétiques comme les nombreux types de plastique que nous avons aujourd'hui. Et dans le cas du plastique, les polymères sont dérivés du carbone (tout cela a du sens maintenant, non?).

## Un peu d'histoire

Le premier polymère intégralement synthétique a été appelé polyoxybenzylméthylenglycolanhydride (yikes). Il a été créé en 1907 par le chimiste Leo Baekeland et appelé publiquement Bakelite - ouf, beaucoup plus facile. C'était un plastique thermodurcissable (plus à ce sujet plus tard) qui était célèbre pour ses propriétés: il était résistant à la chaleur et ne conduisait pas l'électricité, donc c'était un très bon isolant. En 1924, Time Magazine l'appelait «un matériau aux mille utilisations». Ils avaient partiellement raison, cela ressemble plus à cent mille utilisations.

<img style="margin-left: 0;" src="../../assets/plastic/time_magazine.jpg" width="800" />

## Un peu de science

Comme nous l’avons vu, le plastique est composé de polymères synthétiques. Poly signifie multiple et mers signifie unités. Dans ce diagramme, un groupe de monomères crée un polymère lors d’un processus appelé polymérisation.

<img style="margin-left: 0;" src="../../assets/jerry/jerry1.svg" width="100%" />

En ce qui concerne le plastique, les monomères proviennent de combustibles fossiles et, grâce à un processus appelé craquage, deviennent des polymères plastiques. Avec différentes méthodes de production et / ou peaufinage de la structure polymère, nous avons des centaines de types de plastiques différents, avec des propriétés et des caractéristiques différentes. Cette polyvalence est bien illustrée par le chimiste Andrea Sella:

Le polytéréphtalate d'éthylène (PET) est aujourd'hui utilisé pour fabriquer des bouteilles de boisson gazeuse, car il est suffisamment solide pour contenir 2 ATM de pression. Mais un gant d'hiver doux, ou une feuille de plastique pour emballer les fleurs ? C'est le même matériau, la seule différence est la façon dont il a été coulé. Et ce n'est qu'un plastique ! Un autre exemple, une bouteille de lait standard est en polytéréphtalate, fabriqué à partir d'un bloc de construction C2H4. Si vous ajoutez un seul carbone et passez au polypropylène, vous avez un matériau beaucoup plus robuste.

<p class="note">Note: Les plastiques ont remplacé des centaines d'articles ménagers et industriels en étain, papier, vitrocéramique. Ils avaient été annoncés comme ayant une durée de vie quasi-éternelle et étaient bon marché à produire. Mais cet avantage, comme nous le savons maintenant, est aussi son plus grand inconvénient.</p> <br>

Ok, revenons à la science. Au total, il existe des centaines de types de plastiques différents et, comme indiqué dans la section précédente, ils entrent dans différentes catégories: thermodurcissables, thermoplastiques et élastomères. Les plastiques avec lesquels nous travaillons sont des thermoplastiques (car ils peuvent être chauffés et remodelés) et auront souvent un logo de recyclage avec 1-6 (PET, HDPE, PP, PS LDPE et PVC).

Un résumé rapide:

<img style="margin-left: 0;" src="../../assets/jerry/jerry2.svg" width="100%" />

<b>Thermodurcissables</b>: ces plastiques contiennent des polymères qui se réticulent et créent une liaison irréversible, ce qui signifie qu'ils ne peuvent pas être fondus - une fois leur forme prise, ils resteront solidifiés pour toujours. Considérez les thermodurcissables comme du pain: une fois que le pain est fait, si vous essayez de le chauffer, il brûle. Aucun de ces plastiques ne peut être recyclé.

<b>Thermoplastiques</b>: un polymère plastique qui devient mou lorsqu'il est chauffé et dur une fois refroidi. Les matériaux thermoplastiques peuvent être refroidis et chauffés plusieurs fois: lorsqu'ils sont chauffés, ils fondent en liquide et lorsqu'ils refroidissent, ils deviennent durs. Considérez les thermoplastiques comme du beurre: il peut être chauffé et refroidi plusieurs fois, il fond et se fige à nouveau.

<b>Elastomères</b>: quelque part entre le thermodurcissable et le thermoplastique et des exemples de ceux-ci sont le caoutchouc naturel, le silicone ou par exemple le néoprène (oui, le même matériau qui compose les combinaisons de plongée).

# Méthodes de production industrielles

La matière première de vos objets en plastique est généralement du granulat. Après le processus de polymérisation, le plastique est transformé en petites billes ou en flocons, ce qui le rend extrêmement efficace à transporter et facile à fondre. Le recyclage du plastique a un processus similaire - vous déchiquetez le plastique en petits morceaux (trié selon son type bien entendu) et il est prêt à être transformé en nouveaux produits.

### Extrusion

La méthode de production la plus simple est **l’extrusion**. Le plastique pénètre dans la partie d'alimentation où le plastique est poussé à travers une vis sans fin jusqu'à l’ouverture au bout. La forme de la vis change de diamètre du début à la fin, permettant à la matière d’emmagasiner beaucoup de chaleur et de pression résultant en un plastique entièrement fondu lorsqu'elle atteint l'extrémité de la vis. De là, il entre dans le moule - vous pouvez utiliser différentes formes pour différents produits.

<img style="margin-left: 0;" src="../../assets/jerry/jerry3.svg" width="600px"/>

### Moulage par soufflage

Vous pouvez également utiliser des techniques telles que le [__moulage par soufflage__](https://www.youtube.com/watch?v=QpVNyCZ3gjw), où vous prenez ce qui sort de l'extrudeuse lorsqu'il est encore chaud et le mettez en forme à l'aide d'air comprimé et d'un moule.

<img style="margin-left: 0;" src="../../assets/jerry/jerry4.svg" width="600px"/>

### Moulage par injection

La deuxième méthode, légèrement plus complexe, est le [__moulage par injection__](https://www.youtube.com/watch?v=KYqpR50ES5o). Pour ce processus, le plastique est injecté dans un moule, solidifié puis refroidi. Lorsqu'il est refroidi, il conserve la forme du moule. Vous pouvez fabriquer de nombreux produits avec le moulage par injection, par exemple des pièces de Lego, des brosses à dents, des boîtiers de CD ou des couverts. Vous voulez en savoir plus sur les moules?

<img style="margin-left: 0;" src="../../assets/jerry/jerry5.svg" width="600px"/>

### Thermoformage

Un autre procédé de production industrielle est le [__thermoformage__](https://www.youtube.com/watch?v=hZzTg2M64-g). Des grandes feuilles de plastique sont chauffées à une température qui les rend flexibles, puis formées selon une forme spécifique, puis découpées une fois refroidies. Ce processus est souvent utilisé pour créer des gobelets, récipients, couvercles, plateaux jetables - un grand nombre d'articles en plastique à parois minces qui sont utilisés dans les industries alimentaires, médicales et de la vente au détail en général.

<img style="margin-left: 0;" src="../../assets/jerry/jerry6.svg" width="600px"/>

### **Rotomoulage (ou Moulage par rotation)**

Ce processus est utilisé pour créer de gros objets en plastique, mais en faible quantité. Les pastilles en plastique sont placées dans un moule chauffé uniformément  afin d’y être fondues. Le processus est assez flexible car le moule n'a pas à résister à une pression élevée. Cependant, cela demande beaucoup de travail et prend beaucoup de temps.

<img style="margin-left: 0;" src="../../assets/jerry/jerry7.svg" width="100%"/>

### Fabrication additive (ou impression 3D)

Il s'agit d'un processus de production relativement nouveau. Ici, plusieurs couches 2D composent une forme 3D, l'avantage étant l’absence de besoin d'un moule et ainsi pouvoir modifier la conception autant que vous le souhaitez. Cependant, ce processus est lent et il est actuellement assez difficile de fabriquer des filaments d'impression 3D à partir de déchets ménagers.

<img style="margin-left: 0;" src="../../assets/jerry/jerry8.svg" width="600px"/>

### La façon manuelle

Enfin, vous pouvez également traiter le plastique de manière plus manuelle. Vous pouvez le percer, le fraiser, le travailler sur un tour, le poncer. Cela ressemble plus à de l’artisanat mais il y a de beaux résultats. Vous souhaitez en savoir plus sur ces techniques ? [Découvrez nos How-Tos](https://community.preciousplastic.com/how-to)!

<img style="margin-left: 0;" src="../../assets/jerry/jerry9.svg" width="600px"/>

# L'au-delà des plastiques

Nous avons donc appris que la plupart des plastiques sur le marché peuvent être identifiés, triés, déchiquetés et transformés en de nouveaux produits, et idéalement, cela fonctionnerait localement et mondialement. Mais malheureusement, ce n'est pas le cas - environ 2% des emballages en plastique sont recyclés dans un processus en boucle fermée (cela signifie que seulement 2% des emballages sont recyclés pour fabriquer de nouveaux emballages) et 8% sont sous-cyclés, c’est à dire que la qualité est dégradée donc qu’un produit de qualité inférieure est fabriqué.

Le recyclage du plastique est difficile car nos produits sont souvent mélangés à d'autres matériaux, difficiles à séparer car plusieurs plastiques sont mélangés ou parce que de nombreux additifs ont été ajoutés au plastique.

![Recycling Cycle](assets/plastic/recyclingcycle.svg)

### **Donc, si vous avez du plastique et ne voyez aucun numéro, comment faire ?**

Voici quelques-unes des différentes méthodes d'identification du plastique:

**Test de densité** - cette technique tire parti des différentes densités en fonction du type de plastique. Chaque type a une densité spécifique qui le fera flotter différemment dans les liquides. Le liquide peut être de l'eau salée, de l'alcool, de l'huile végétale ou de la glycérine et cette technique est utilisée intensivement par l'industrie. Cependant, il ne peut séparer que 2 types de plastiques - si vous avez un lot de plastique mixte, vous devrez répéter l’opération plusieurs fois. En plus de cela, les résultats peuvent être très délicat à interpréter, en particulier à cause des additifs mélangés au plastique qui peuvent changer sa densité, la rendant imprécise.

**Test infrarouge** - ce processus est très efficace et consiste à diriger une lumière infrarouge (généralement sur un tapis roulant) vers différents plastiques. Chaque plastique ayant une structure moléculaire différente, la réponse en retour peut être facilement identifiée. Malheureusement, là où la technologie en est actuellement, des pigments plus sombres dans le plastique peuvent mélanger le signal, il y a donc souvent des erreurs de lecture du type de plastique. Nous nous y plongerons dans la section Recherche de l'Academy, dans le chapitre «Tri robotisé».

<img style="margin-left: 0;" src="../../assets/jerry/jerryinfrared.svg" width="600px"/>

**Test de brûlure** - c'est une technique simple consistant à découper un petit morceau de plastique et le brûler à l’aide d’une flamme (cela produit des fumées toxiques, nous ne recommandons donc pas de faire ceci régulièrement!). En brûlant un morceau, il convient d’observer la couleur, la nature et l'odeur de la flamme.

**Tri manuel** - il s'agit d'un processus manuel lors duquel le plastique entre sur un tapis roulant afin que des personnes identifient les plastiques courants ou utilisent les codes SPI. Le problème de ce processus étant que certains objets peuvent avoir la même apparence, mais sont fabriqués à partir de matériaux différents. [__Vous voulez en voir un aux Pays-Bas ? Visite de Dave en 2013__](https://www.youtube.com/watch?v=Ktkn8TeoTA8).

## Quelques éléments pour vous aider à vous en souvenir:

* Les produits chimiques sont souvent stockés dans des bouteilles en PEHD.
* Le PP est très flexible, il peut par exemple être utilisé avec des charnières et se plie plusieurs fois sans casser.
* Si vous frappez le PP avec un marteau, il se brisera.
* Si vous écrasez du PE, il se pliera ou se déformera.
* Pour identifier facilement un thermoplastique d'un thermodurcissable, le premier sera lisse lorsqu’il est coupé avec une lame tranchante et le second produira de petites particules.

<b>Vous souhaitez partager vos commentaires, discuter du plastique ou en savoir plus sur la communauté ? Allez sur le canal [\#plastic](https://discordapp.com/invite/n5d8Vrr)  sur Discord. Nous parlons ici de plastique, de sécurité, de fumées et de propriétés des matériaux.</b>

<p class="note">NdT : Depuis le début du projet PP la communauté a beaucoup grandi. ☝️ Le lien ci-dessus vous emmène sur les salons "monde" 🌍 où on s'exprime en anglais. <b>Si vous préférez échanger en français sur ces sujets, il existe maintenant 👉<a href="https://discord.gg/Qa3wTrn">un Discord Francophone, à rejoindre par ici</a>👈! </b> Plus d'excuses pour ne pas  venir vous présenter, vous et vos recherches 😉 </p>
