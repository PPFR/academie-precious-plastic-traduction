---
id: safety 
title: Sécurité et Fumées 
sidebar_label: Sécurité et Fumées
---

<div class="videocontainer"> <iframe width="800" height="400" src="https://www.youtube.com/embed/bsj6qHHLynk" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe> </div>

<style> :root { --highlight: #ffe084; --links: #29bbe3; --hover: rgb(131, 206, 235); } </style>

# Sécurité et Fumées

<div class="videoChapters"> <div class="videoChaptersMain">

### Il est important de prendre cet aspect au sérieux.

La sécurité est un sujet primordial lorsqu'on travaille avec du plastique, il est extrêmement important de prendre les bonnes précautions afin de travailler en toute sécurité. Nous allons vous guider à travers les dangers de l'exposition aux fumées ainsi que les plastiques avec lesquels il est dangereux de travailler, et voir comment nous configurons les systèmes de filtration chez Precious Plastic.

> __Conseil d’expert__: Essayez autant que possible de faire fondre votre plastique à la température la plus basse et pour la durée la plus courte possible.

</div> <div class="videoChaptersSidebar">

### Video Chapters

* 00\.18 Fume Dangers
* 03:01 Fume Test
* 06\.14 Tips
* 07\.53 Filters

</div> </div>

# Fumées = Danger

![Don't Burn Plastic](assets/plastic/dontburnplastic.svg)

La première règle: ne pas brûler de plastique. C’est très important. Pourquoi ? Il est extrêmement nocif pour vous, votre entourage et l'environnement. Des toxines très nocives sont libérées lors de la combustion du plastique et peuvent considérablement augmenter les risques de cancer, de maladies respiratoires et de malformations congénitales. Il peut également fortement endommager les organes internes et le système hormonal.

Lors de la combustion du PVC, des dioxines sont libérées, un composé hautement toxique dont il a été démontré qu'il augmentait le risque de cancers, de problèmes de fertilité et de dommages au système immunitaire. Chez Precious Plastic, nous ne travaillons pas avec le PVC.

Si vous avez besoin de brûler un petit morceau de plastique pour identifier son type, faites-le en prenant les précautions appropriées: en portant un masque et à côté d'un système de filtration approprié. La chose la plus importante à propos du travail du plastique: vous pouvez le faire fondre, mais ne pouvez tout simplement pas le brûler. Et c'est une différence importante.

## Composés Organiques Volatils (ou COV)

Tous les plastiques sont constitués de grosses molécules, qui, une fois fondues, produisent de plus petites molécules sous forme de fumées. Autrement appelées composés organiques volatils, ils sont très dangereux - les effets immédiats étant une irritation sévère des yeux, du nez et des poumons. Les effets d’une exposition prolongée aux fumées de tout plastique synthétique sans précaution de sécurité peut entraîner des cancers, des malformations congénitales et des maladies.

Comme le montre le graphique ci-dessous, le contenu des fumées de plastiques courants a été étudié par l'Air and Waste Management Association, l'Académie chinoise des sciences et l'Université de Tokyo. Les études ont conclu que l'ABS et le PS créent le plus de fumées : environ 5 à 7 fois plus que les autres plastiques.</b>

![PP Image](assets/plastic/fume-graph.png)

Les fumées d'ABS et de PS contiennent du styrène, du benzène et de l'éthylbenzène - ces composés sont appelés composés cycliques, ils sont très nocifs et peuvent provoquer des cancers. Le benzène est un cancérogène pour l'homme, et le styrène ainsi que l'éthylbenzène sont des cancérogènes probables (cancérogène probable signifie que les composés ne se sont pas encore révélés cancérigènes). Heureusement, tous ces composés cycliques peuvent être facilement filtrés avec du charbon actif, mais nous verrons cela plus tard !

Les plastiques les plus sûrs à fondre sont le PP (5) et le PE (2) car ils sont faibles en composés cycliques et ils sont essentiellement de la cire raffinée. Le PVC et le PA sont pauvres en fumées mais leur contenu est beaucoup plus nocif. 

# Tests de fumées

Pour le test des fumées, nous utilisons un détecteur PID, qui signifie Détecteur par Photo-Ionisation - il détecte les niveaux de sécurité des fumées pour les COV et peut nous indiquer d'où proviennent exactement les fumées. L'avantage d'utiliser un capteur PID est qu'il est très précis et peut être tenu à la main, ce qui nous permet de concevoir un système flexible d'extraction des fumées que nous pouvons déplacer.

Lorsque l'air pénètre dans le PID, une lumière UV interagit avec les molécules de l'air (les composés organiques libèrent des ions chargés positivement lorsqu'ils traversent la lumière qui sont ensuite capturés par une plaque chargée négativement, produisant un courant électrique mesurable).

**Nos résultats**:

Dans notre espace de travail, nous avons détecté des fumées à 2 cm de la source dont le niveau était environ 14 fois plus élevé que le maximum recommandé. Le PS a créé 5 fois plus de fumées que le HDPE et le PP : cela signifie que le HDPE et le PP sont très faibles en composés cycliques. Nous avons également détecté la provenance des fumées dans chaque machine (plus d'informations ci-dessous).

Nous respectons une réglementation couramment utilisée : la concentration maximale de 3 heures de teneur en hydrocarbures est de 0,24 ppm, à ne pas dépasser pendant plus d'un an [2]. En conclusion, notre plus grande menace est la vapeur de styrène formée par la fusion du PS.</b>

Pour plus de détails, consultez nos notes: 🗐 https://tinyurl.com/y5r2u3u4

## Sources de fumées des machines Precious Plastic

Ok, alors d'où sortent les fumées des machines Precious Plastic ?

<b>La presse à injecter</b>

1. Des fumées sortent de la buse lorsque vous retirez le moule
2. Des fumées sortent de la trémie lorsque la poignée de l'injecteur est relevée

<img style="margin-left: 0;" src="../../assets/plastic/injection_fumes.jpg" width="300" />

<b>La machine d'extrusion</b>

1. La plupart des fumées sortent de la buse
2. Certaines fumées de la trémie lorsqu'elles ne sont pas bloquées par des granulés et par les ouvertures du moule
3. Par exemple, lors de la fabrication d'un tasseau, des fumées sortent à l'extrémité du moule

<img style="margin-left: 0;" src="../../assets/plastic/extrusion_fumes.jpg" width="300" />

<b>La presse à plaques</b>

1. La plupart des fumées sortent des quatre côtés
2. Si le chauffage est inégal, des fumées sortent de certaines zones surchauffées
3. Les fumées sortaient encore pendant 7 minutes lors de l'étape de refroidissement de la presse

<img style="margin-left: 0;" src="../../assets/plastic/sheetpress_fumes.jpg" width="300" />

# Conseils

● Bien laver le plastique avant de le faire fondre. Les résidus de produits de nettoyage peuvent créer des fumées nocives.  
● Ne faites jamais fondre du plastique non séparé, car différents types de plastique ont des températures de fusion différentes, ce qui signifie que certains plastiques brûleront avant que d'autres ne fondent.   
● Lorsque vous travaillez avec du PS ou de l'ABS, assurez-vous de ne pas le faire fondre plus de 8 heures par semaine, et utilisez toujours un masque à gaz et une ventilation.  
● N'utilisez pas un masque anti-poussière ordinaire, cela ne fonctionnera pas ! Assurez-vous d'utiliser un masque à gaz à filtre à charbon actif. Et assurez-vous que votre masque à gaz est bien ajusté !  
● Essayez de faire fondre le plastique à la température la plus basse possible pendant le temps le plus court.  
● Si vous vous sentez étourdi ou si vous avez du mal à respirer, éloignez-vous des fumées pour l’air frais.   
● Éloignez-vous de la source de fumée - elle est 14 fois plus élevée que l'exposition maximale recommandée. S'il n'y a pas de ventilation, vous devez avoir un masque et vous tenir à environ 2 mètres.   
● Des caddies de fumées avec un bras réglable sont nécessaires pour l'extrusion et l'injection. Pour la presse à plaques, une hotte d'extraction des fumées doit être incluse dans la conception de la machine.   
● Achetez un détecteur de COV bon marché pour vérifier si l'air dans la zone de l'atelier est sûr et pour vérifier si le filtre fonctionne correctement.

# Equipement de sécurité

Lorsque vous travaillez avec du plastique, vous devriez avoir un masque à gaz à filtre à charbon actif et un filtre d'extraction autonome, qui utilise également du charbon actif. C’est un matériau incroyable et une bouée de sauvetage ! Nous avons choisi de l'utiliser car il filtre très bien les composés cycliques et se trouve facilement partout dans le monde. Nous pouvons également construire des systèmes LEV avec des filtres à charbon actif pour des machines ou des zones spécifiques de l'atelier.

## Ventilation (LEV)

<img style="margin-left: 0;" src="../../assets/plastic/ventilation_three.jpg" width="600"/>

LEV signifie Local Exhaust Ventilation, qui est une norme de l'industrie pour ventiler efficacement l'air toxique. Ce système est composé de trois parties : une hotte pour permettre aux nuages de contaminants de pénétrer dans le LEV, un conduit qui transfère l'air et le contaminant de la hotte au point de décharge, et fixé à un ventilateur centrifuge de 780-1000 mètres cubes par heure.

## Charbon actif

L'utilisation d'un filtre à charbon actif (ACF) est la méthode de filtrage la plus propre et la plus simple. Il a une grande surface et est traité pour avoir plus de pores que le carbone normal - les fumées adhèrent très bien à ces pores, ce qui en fait un excellent matériau pour filtrer les choses désagréables.   
L’ACF élimine très bien les COV suivants : toluène, xylène, styrène, alcool, benzène, décane, éthylbenzène, heptane et octane, et les gaz suivants : pentane, acétone, hexane [3]. La porosité est la caractéristique la plus déterminante du charbon actif. Mais ils ne durent pas éternellement, vous devez changer le filtre. Bien que les composés soient invisibles pour nous, ils ont une odeur distincte qui est normalement filtrée. Des que vous pouvez les sentir, il est temps de changer le filtre.

### Filtres autonomes

<img style="margin-left: 0;" src="../../assets/plastic/carbonfilters.jpg" width="400"/>

Un filtre comme celui-ci coûte environ 40 à 50 euros et contient environ 2-3 kg de charbon actif granulaire qui doit être changé chaque mois (vous pouvez également obtenir un filtre plus gros qui durera plus longtemps). Ces filtres peuvent être fixés aux ventilateurs centrifuges pour la ventilation. Conditions préférentielles d'utilisation de ces filtres: humidité inférieure à 70% et température ambiante inférieure à 80 ° C.

### Achat de granules de carbone (pour l'élimination des COV)

<img style="margin-left: 0;" src="../../assets/plastic/carbongranules.jpg" width="400"/>

Lorsque le filtre doit être changé, vous n'avez pas besoin d'acheter un nouveau filtre. Au lieu de cela, vous pouvez acheter des granules de charbon actif pour changer le charbon actif épuisé - ce charbon épuisé peut être placé dans une décharge municipale. Il est préférable d'acheter du charbon actif avec un indice d'iode supérieur à 1050 mg / g - l'indice d'iode est une indication de la surface active (plus l'indice d'iode est grand, meilleure est la filtration).

### Masque à gaz

<img style="margin-left: 0;" src="../../assets/plastic/maskgas.jpg" width="400"/>

Un masque à gaz est également extrêmement important car la filtration n'est pas efficace à 100% et il agit comme une barrière supplémentaire. Même les grands acteurs industriels du recyclage et de la fabrication n'ont pas atteint l'air pur, et les masques à gaz sont donc une pratique courante. Les masques les plus courants sont les masques 3M et comportent quatre parties : l'embout buccal, la cartouche filtrante, un porte-filtre à poussière et un filtre à poussière. Ils peuvent être utilisés pendant 50 heures avant de remplacer la cartouche et les filtres à poussière.

# Voilà, c'est tout !

Si vous prenez les précautions de sécurité appropriées d'un masque à gaz et d'une filtration d'extraction appropriés, vous êtes en sécurité. Ne travaillez pas avec du PVC ou de l'ABS, les plastiques les plus sûrs étant le PP (5), le LDPE (4) et le HDPE (2). N'oubliez pas, vous ne pouvez faire fondre le PS que jusqu'à 8 heures par semaine.

📁 [Documents complémentaires (en anglais)](https://drive.google.com/drive/folders/1MSfb8R3ZWbrJv6FxL1JpLioZ9i8KFD0D?usp=sharing)

1. [Caractéristiques des polluants, évalation des risques santaires des Composés Organiques Volatils(VOC) émis par les différents déchets plastiques solides (en anglais)](http://iehpc.gdut.edu.cn/2015-7.pdf)
2. [Éliminer les VOCs de l'air pollué (en anglais)](http://beta.chem.uw.edu.pl/people/AMyslinski/nowy/zarzadzanie_01/literature_HWW/02.pdf)
3. [Évaluation des filtres GAV pour l'élimination des VOCs (en anglais)](https://www.isiaq.org/docs/papers/940.pdf)

<b>Vous souhaitez partager vos retours, discuter du plastique ou en savoir plus sur la communauté ? Allez sur le canal [#plastic](https://discordapp.com/invite/n5d8Vrr) sur Discord. Nous y discutons de plastique, de sécurité, de vapeurs et de propriétés des matériaux.</b>


<p class="note">NdT : Depuis le début du projet PP la communauté a beaucoup grandi. ☝️ Le lien ci-dessus vous emmène sur les salons "monde" 🌍 où on s'exprime en anglais. <b>Si vous préférez échanger en français sur ces sujets, il existe maintenant 👉<a href="https://discord.gg/Qa3wTrn">un Discord Francophone, à rejoindre par ici</a>👈! </b> Plus d'excuses pour ne pas  venir vous présenter, vous et vos recherches 😉 </p> 
