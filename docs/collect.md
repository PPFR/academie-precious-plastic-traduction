---
id: collect
title: Collect
sidebar_label: notre Système de Collecte
---

<div class="videocontainer">
  <iframe width="800" height="400" src="https://www.youtube.com/embed/BtrfTSSGHEo" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
</div>

<style>
:root {
  --highlight: #b79ecb;
  --hover: #b79ecb;
}
</style>

# Notre système de collecte

<div class="videoChapters">
<div class="videoChaptersMain">

### Pas de collecte, pas de recyclage! 

La collecte du plastique est cruciale - nous allons vous montrer ici ce que cela signifie de collecter du plastique, comment cela fonctionne dans le monde et comment nous envisageons un système de collecte au sein de l'univers Precious Plastic !

> _Conseil d'expert_: demandez aux magasins et aux entreprises autour de vous s'ils ont des déchets plastiques dont ils veulent se débarrasser. Si vous leur fournissez des bacs de collecte, ils pourraient même les trier pour vous !


</div>
<div class="videoChaptersSidebar">

### Chapitres Vidéo (en anglais) 

- 00:19 Collecte des déchets
- 01:21 Situation Actuelle
- 02:05 Défis
- 02:32 Le Plan Precious Plastic
- 03:00 Notre stratégie
- 03:42 Avantages de cette approche
- 04:26 Outils



</div>
</div>

# Collecte des déchets

![Plastiques du quotidien](assets/Collect/dailyplastic.svg)

Chaque jour, nous achetons, utilisons et produisons une énorme quantité de plastique. Une fois ce plastique utilisé, généralement après une très courte durée, il est mis au rebut. Selon l'endroit où vous vivez, vous pouvez le déposer dans un point de recyclage local ou le faire enlever à votre domicile pour qu'il soit traité. C'est normalement là que les problèmes commencent.

Mais pourquoi ? Le plastique est recyclé, non ? Eh bien, le plastique que les gens jettent dans leur bac de recyclage n'est pas trié, est souvent sale et mélangé à d'autres matériaux (la bouteille de soda avec l'étiquette en film, par exemple). Comme nous l'avons appris, la séparation des plastiques est fondamentale pour un recyclage correct, et bien sûr, personne ne va le faire pour nous. Le tri industriel est possible, avec le balayage infrarouge, les techniques de densité de l'eau et le tri manuel, mais il est loin d'être assez efficace pour trier les 348 millions de tonnes produites chaque année. Ouaip.

### Ok. C'est sinistre. Alors qu'est-ce qu'il devient? 

Parfois, le plastique est recyclé. Comme vous l'avez probablement lu quelque part, c'est environ 9%. Il doit passer par un processus très complexe, coûteux, long et gourmand en énergie, et la plupart des pays n'ont même pas cette possibilité.

![Devenir du Plastique](assets/Collect/burnlandfillocean.svg) 

L'autre scénario, plus probable, consiste à brûler le plastique, à l'envoyer dans une décharge, à le jeter dans l'océan ou à l'envoyer par cargo en Asie ou en Afrique (où l'on ne dispose certainement pas de l'infrastructure nécessaire pour le traiter). Ces solutions provoquent d'énormes dégâts environnementaux et une oppression sociale. Vous êtes-vous déjà surpris à penser : "Je ne vois aucun déchet nulle part ! Dans les pays occidentaux, nos rues sont propres. Mais dans certains endroits d'Asie ou d'Afrique, elles sont souvent considérées comme sales et jonchées de détritus - on en retrouve partout dans l'océan et on les brûle dans les rues. Cela s'explique par le fait que les systèmes de collecte doivent être subventionnés par l'État avec l'argent des contribuables, ce qui est tout simplement impossible dans de nombreux endroits. Mais ne nous méprenons pas : l'Occident reste le plus gros producteur de déchets plastiques de la planète. Et pour couronner le tout, les systèmes de collecte dans le monde ne sont pas standardisés. Chaque pays et chaque municipalité au sein de ces pays ont des réglementations différentes. Cela laisse les gens très perplexes (nous y compris).

## Défis
La manière dont le plastique est actuellement collecté pose de nombreux problèmes :

- Le plastique collecté est souvent sale et mélangé à d'autres matériaux.
- Elle est très gourmande en ressources : faire circuler une flotte de camions et de chauffeurs dans la ville 24 heures sur 24 et 7 jours sur 7 est extrêmement coûteux.
- Elle encourage une culture du jetable ( ??)

# Mais ne vous inquiétez pas - nous avons un plan ! 💪

## Le système de collecte de Precious Plastic

Vous le savez probablement, mais Precious Plastic est axé sur les citoyens : des gens, comme nous, comme vous, qui agissent. Nous vous invitons à vous impliquer davantage dans le cycle de vie de vos déchets plastiques et à faire partie de la solution. En fournissant les informations et l'infrastructure, la communauté peut être un outil de changement.

Alors comment cela fonctionne-t-il pour la collecte du plastique ? Excellente question 😉

![Collection Network](assets/Collect/collectionnetwork.svg)

<p class="note">Note: quand on parle de citoyens, cela parle de tout le monde dans la société. Quand on parle des gens de notre communauté, il faut comprendre ceux qui contribuent au projet Precious Plastic (si ce n’est pas encore fait, rejoignez-nous !).</p>

<b>Premièrement.</b> Nous donnons aux gens toutes les informations dont ils ont besoin pour nettoyer correctement leur plastique à la maison, puis nous leur montrons où ils peuvent l'apporter pour être sûrs qu'il soit correctement recyclé. L'éducation est au cœur de tout ce que nous faisons (savoir = pouvoir). Enseigner aux citoyens l'importance d'un plastique propre, sans étiquette, peut à lui seul créer un énorme changement.

<b>Deuxièmement.</b> Nous montrons aux membres de notre communauté comment créer des points de collecte dans leur région pour accepter le plastique propre des citoyens. Comme nous n'acceptons que ce plastique propre et sans étiquette, nous montrons également aux citoyens et à notre communauté à quel point ce matériau peut être précieux.

## Avantages de cette approche

Cette nouvelle approche offre différents avantages, tant pour les personnes que pour l'environnement :

1. Le premier (et le plus important) est que le matériau collecté est de haute qualité. La mise en place de points de collecte locaux gérés par des membres de la communauté permet d'obtenir des matériaux de meilleure qualité, propres et utilisables.

2. Deuxièmement, <b> la collecte reste locale.</b> Le plastique collecté sera recyclé et transformé dans l'un des espaces de travail locaux de Precious Plastic, et non expédié à travers la planète en espérant que quelqu'un d'autre s'en occupera.

3. Troisièmement, <b>l'éducation.</b> Cette approche vise à informer et à éduquer les citoyens sur le plastique et la façon de le recycler correctement. Il s'agit d'une stratégie long-termiste qui prend beaucoup de temps au début, mais que nous pensons payante sur le long terme. 

Enfin, le projet repose sur des personnes, et pas sur le gouvernement ou des entreprises. C'est donc un système résilient, humain et accessible.

# Outils

Vous êtes de la partie ? Nous l'espérons ! Nous avons développé une série d'outils pour les citoyens, ainsi que pour les personnes de notre communauté.

Pour les citoyens, nous avons un site Web qui les informe sur le système de recyclage de Precious Plastic, sur la façon de nettoyer le plastique et sur la manière d’utiliser la carte pour trouver le point de collecte le plus proche.

![All Tools](../../assets/Collect/alltools.svg)

Pour les personnes de notre communauté, nous avons développé :

- Un <b>kit de démarrage:</b> si la mise en place d’un point de collecte vous intéresse, ce kit vous donnera tous les éléments dont vous avez besoin pour commencer.
- Une <b>plateforme web</b> pour connecter tous les membres de la communauté. Vous pouvez utiliser la carte pour trouver des espaces de travail locaux, ou l'outil "How-To" pour découvrir les autres stratégies de collecte utilisées dans le monde entier.
- Des <b>Affiches</b> pour votre espace de travail ou votre communauté afin de faire passer le message.
- Un <b>outil pour relever la quantité de plastique que vous traitez</b>, afin que nous puissions l'additionner et visualiser l'impact global de l'univers Precious Plastic.

<b>Vous souhaitez faire part de vos commentaires, discuter de la collecte du plastique ou en apprendre davantage sur la communauté? endez-vous sur le canal [#Collect](https://discordapp.com/invite/kpnYaEr) ur Discord. Nous y parlons du système de collecte, des points de ramassage et de la façon de trier le plastique.</b>
