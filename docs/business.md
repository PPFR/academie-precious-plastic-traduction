---
id: business
title: Business
sidebar_label: Intro
---

<style>
:root {
  --highlight: #f7b77b;
  --hover: #f7b77b;
}
</style>

![Business Tools](assets/Business/businesspresentation.jpg)

# Les outils commerciaux

### Comment gagner de l'argent avec le recyclage du plastique ?

Nous sommes ravis de vous présenter quelques outils essentiels qui vous aideront non seulement à avoir un impact sur votre communauté locale, mais aussi à devenir financièrement viable pour pouvoir le faire pendant longtemps. Joseph (notre homme d'affaires) va vous présenter les trois outils suivants pour passer de rien à une entreprise !


> Conseil d'expert: les entreprises qui réussissent le mieux sont celles qui se concentrent vraiment sur ce qu'elles veulent offrir à leurs clients ou à leur public.

![Business Tools](assets/Business/businessintro.svg)

## Les outils commerciaux sont une série en trois parties :

- Le **Plan d'action** est un plan d'affaires rapide et sale qui vous permet de planifier votre modèle d'affaires.
- Le **calculateur d'Atelier** est un outil de prévision financière qui vous permet de comprendre la viabilité financière de votre espace de travail.
- Le **modèle de plan d'affaires** est l'endroit où vous rassemblez tout et créez un plan d'affaires complet !

C'est parti !
