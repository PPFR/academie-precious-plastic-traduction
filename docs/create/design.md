---
id: design 
title: Concevoir pour le plastique recyclé 
sidebar_label: Bonne conception
---

<div class="videocontainer"> <iframe width="800" height="400" src="https://www.youtube.com/embed/yoidAZlYHLQ" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe> </div>

<style> :root { --highlight: #ffe084; --links: rgb(131, 206, 235); --hover: rgb(131, 206, 235); } </style> <div class="videoChapters"> <div class="videoChaptersMain">

# Concevoir pour le plastique recyclé

### Le plastique recyclé est un matériau précieux et polyvalent

Nous concevons des produits aux côtés de la communauté depuis de nombreuses années maintenant et avons appris des choses précieuses sur les propriétés de chaque type de plastique et sur ceux qui sont les meilleurs à utiliser avec nos machines. La première chose à faire pour concevoir et produire de bons produits est d'être sûr, assurez-vous de mémoriser les informations de la section Sécurité et fumées pour vous assurer que vous n'exposez pas vous-même ou quelqu'un d'autre à des fumées dangereuses.

> Conseil d'expert : concentrez-vous sur une seule technique et maîtrisez votre/vos produit(s) !

</div> <div class="videoChaptersSidebar">

### Video Chapters

* 00:07 Introduction
* 00:16 Plastic Types
* 01:05 Compression vs Injection/Extrusion
* 02:32 Value of Plastic
* 03:46 Plastic Design Ethics
* 05:28 Sheetpress
* 07:18 Injection
* 09:23 Extrusion

</div> </div>

### Chapitres

1. Pour commencer
2. Faites-le circulairement
3. Concevoir pour les machines Precious Plastic
4. Machine d'extrusion.
5. Presse à plaques
6. Injection
7. mode d'emploi

## Pour commencer

Comme nous l'avons vu dans la section Les bases du plastique, chaque type de plastique a des propriétés différentes et se comporte donc de manière différente. Il est important d'apprendre à bien connaître le plastique que vous avez choisi, ce qui demande du temps et des essais (et de la patience, bien sûr !).

<b>La compression</b> est assez explicite : elle consiste à comprimer le plastique déchiqueté ou les granulés ensemble par la chaleur et la pression, ce qui crée souvent un léger écoulement mais conserve la forme initiale des granulés. Cette opération peut être réalisée à l'aide d'une machine comme notre nouvelle Presse à Plaque ou l'ancienne machine de compression.

Produits fabriqués avec la machine Sheetpress ou la machine à compression :

![Sheet Products](assets/create/sheets-products.jpg)

<b>L'extrusion</b> "extrude" le plastique à travers un fourreau métallique chaud en une ligne unique de plastique, le plastique sera complètement fondu et les couleurs seront uniformes.

Produits fabriqués avec la machine d'extrusion :

![Extrusion Products](assets/create/extrusion-products.jpg)

<b>L' injection</b> fait également fondre le plastique dans un autre type de récipient en métal chaud, mais cette fois dans un moule. Vous pouvez jouer avec la couleur et la texture avec les moules d'injection selon différents types de techniques.

Produits fabriqués avec la machine à injection :

![Injection Products](assets/create/injection-products.jpg)

## Quelques principes directeurs :

Le recyclage du plastique est souvent un processus long et intense en énergie, ce qui fait de ce matériau une ressource très précieuse. Voici quelques principes directeurs que vous pouvez suivre tout au long de votre processus de conception et utiliser comme des coches pour savoir si vous tirez le meilleur parti de ce matériau :

* Optimisez la quantité de matériau que vous utilisez dans votre conception, utilisez des parois fines mais solides pour fabriquer des pièces creuses. Le plastique peut être très résistant lorsqu'il est façonné dans la bonne forme.
* Soyez attentif aux étapes de votre processus de production : essayez de réfléchir stratégiquement à la quantité de temps et d'énergie nécessaire à chaque étape et concevez une chaîne de fabrication efficace et productive, sans compromettre la qualité ou la durabilité.
* À chaque étape de la production, il y aura toujours des sous-produits tels que de la poussière de plastique, des chutes ou des matériaux résiduels en raison de la nature du processus. Veillez à les collecter et à les conserver soigneusement, propres et séparés, afin de pouvoir les réutiliser dans votre propre installation ou les vendre/donner à d'autres installations de production qui les traitent correctement.
* Enfin, essayez de mettre en valeur ses qualités esthétiques uniques : il est important de produire des produits bien conçus, bien faits et beaux pour que les gens les apprécient. Faites attention aux détails et évitez de fabriquer des produits bon marché qui seront facilement jetés.

# Faites-le circulairement

### Que signifie être "circulaire" ?

La circularité consiste à prendre en compte l'ensemble du cycle de vie d'un produit dès le départ. Cela signifie concevoir des produits composés de matériaux recyclables et/ou pouvant être clairement recyclés (y compris le désassemblage et la séparation des matériaux). Cela signifie qu'il faut concevoir des produits qui ne produisent pas de déchets et qui tiennent compte de tous les matériaux qui peuvent être obtenus ou utilisés à d'autres fins.

### Le modèle linéaire "prendre-fabriquer-utiliser-déchets

La transition vers une économie circulaire est cruciale pour notre survie sur la planète Terre. Notre modèle linéaire actuel signifie que les ressources sont extraites de la terre, transformées en un produit, puis jetées, dans un cycle de vie "prendre-fabriquer-utiliser-déchets". Voyons comment cela fonctionne pour le plastique :

<b>Prendre:</b> le pétrole de la terre par une extraction nocive et d'énormes quantités d'énergie.<br> <b>Fabriquer : </b> du plastique à partir de processus nocifs supplémentaires, produisant des fumées toxiques et des produits chimiques qui s'infiltrent dans les cours d'eau, l'atmosphère et la terre.<br> <b>Utiliser:</b> 0% des 300 millions de tonnes produites chaque année sont à usage unique. Sa durée de vie est donc de quelques minutes à quelques jours. <br> <b>Déchets:</b> utilisés pendant quelques minutes, les plastiques d'usage courant durent au moins 500 ans avant de commencer à se dégrader.

Les conséquences de cette économie sont dévastatrices, et nous devons donc penser différemment : d'où proviennent les matériaux ? quels sont les sous-produits de la production ? quelle est la durée de vie réaliste de ce nouveau produit ? Comment peut-il être recyclé, désassemblé ou réutilisé ? Si ce sujet vous intéresse, Cradle to Cradle est un excellent livre à lire.

Il y a donc beaucoup de matière à travailler 😉 .

# Concevoir pour les machines Precious Plastic

Afin de concevoir de bons produits, vous devez d'abord comprendre les capacités de chaque méthode de production. Nous avons décrit ce que nous avons appris, mais n'ayez pas peur d'essayer de nouvelles choses, de repousser les limites de votre créativité et, bien sûr, de partager vos découvertes avec la communauté Precious Plastic !

## La machine d'extrusion

L'extrusion est un processus continu où le plastique broyé entre dans la trémie, est chauffé et pressé par une vis à travers un long baril. Le résultat est une ligne régulière de plastique, et comme cette machine fonctionne en continu, si vous avez assez de plastique et un processus bien rationalisé, vous pourriez (techniquement) fabriquer des produits 24 heures sur 24, 7 jours sur 7 !

Comment fonctionne la machine d'extrusion :

![How Extrusion Machine Works](assets/create/extruder_design.svg)

> Astuce: L'extrusion du plastique permet de mélanger le plastique de manière uniforme et de créer de nouvelles couleurs. Les couleurs les plus foncées dominent - assurez-vous de vous entraîner avec différentes combinaisons.

### 1\. Travailler le filament

Cette technique permet de créer des produits frais en enroulant le filament autour d'une forme ou d'un moule. Cela demande beaucoup d'entraînement (faites-nous confiance !) et vous pouvez créer toutes sortes de bols, de récipients, de lampes, ou votre propre création. Vous pouvez contrôler la forme du filament en changeant la buse - pensez à la mise en forme des pâtes ou de la Play-Doh, vous pouvez obtenir des carrés, des cercles, des diamants, un mélange... à vous de voir !

![Extrusion Products](assets/create/extrusion-products2.jpg)

### 2\. Fabrication de barres

Cette technique vous permet d'extruder des profilés en plastique solide en fixant un moule métallique à la buse et en y extrudant le plastique. Vous pouvez créer des barres de différentes formes et tailles, des plus petites aux plus grandes, des plus simples aux plus complexes, droites ou avec des angles.

<b>Moules et techniques :</b>

* [Make a flat nozzle for the extrusion machine](https://community.preciousplastic.com/how-to/make-a-flat-nozzle-for-the-extrusion-machine)
* [Extrude different textures](https://community.preciousplastic.com/how-to/make-a-mould-to-extrude-beams)
* [Make a t-shape beam](https://community.preciousplastic.com/how-to/make-a-tshape-beam-)
* [Make glasslike beams](https://community.preciousplastic.com/how-to/make-glasslike-beams)
* [Extrude into a closed mould](https://community.preciousplastic.com/how-to/extrude-into-a-closed-mould)
* [Make a quick release for the extrusion machine](https://community.preciousplastic.com/how-to/make-a-quick-release-for-the-extrusion-machine)

Quelques expériences ont été nécessaires pour comprendre comment la couleur est extrudée et durcit. En gros, lorsque le plastique se déplace dans le moule de la pièce, il durcit au contact du métal et le plastique suivant remplit le milieu de la pièce. Cela signifie que la couleur que vous mettez en premier créera la surface de la barre.

![Bench](assets/create/bench.jpg)

> Astuce: si vous chauffez d'abord le moule, vous pouvez obtenir une surface lisse. Mais attention car cela augmente souvent les temps de refroidissement et peut être très inefficace du point de vue énergétique car il faut constamment chauffer et refroidir une pièce de métal.

### 3\.  Utilisation de moules

Vous pouvez aussi remplir des moules fermés, c'est plus rapide et cela permet de remplir des volumes plus importants que la machine à injecter. Si vous concevez en tenant compte des points forts de chaque technique et que vous rationalisez votre processus de production, vous pouvez fabriquer une grande quantité de produits de manière très efficace. Par exemple, la brique V4 est fabriquée à l'aide de la machine d'extrusion et un moule creux est conçu pour réduire la quantité de matériau sans compromettre la résistance de l'objet - cela permet de réduire la quantité de matériau nécessaire pour faire plus avec moins, ainsi que de réduire le temps de production et le transport en raison du poids. Victoire !

Voilà le type de décisions de conception à prendre en compte avant de se lancer dans la production : circularité, efficacité et coût. N'oubliez pas d'inclure toutes les étapes de la production lorsque vous évaluez la conception de votre produit - les temps de refroidissement, les processus de démoulage et le temps nécessaire pour refixer un moule sont des exemples de différentes variables.

![Bench](assets/create/bricks.jpg)

<b>Exemples de Produits </b>

* [Make extruded plastic bricks](https://community.preciousplastic.com/how-to/make-extruded-plastic-bricks)
* [Build brick structures](https://community.preciousplastic.com/how-to/build-brick-structures)
* [Make a bench with beams](https://community.preciousplastic.com/how-to/make-a-bench-with-beams)
* [Make a lamp with beams](https://community.preciousplastic.com/how-to/make-a-lamp-with-beams)
* [Create an extruded lamp](https://community.preciousplastic.com/how-to/create-an-extruded-lamp)

## La presse à plaques

Cette machine crée des feuilles jusqu'à 1 x 1 mètre avec une gamme d'épaisseurs entre 6 mm et 30 mm. Lors de la fabrication d'un moule en plaques, il existe de nombreuses configurations différentes en termes de taille, d'épaisseur et de finition de surface. Faites vos recherches, lisez les différents modes d'emploi et regardez les produits sur le Bazar avant d'investir dans le meilleur moule qui répondra à vos besoins. Voici quelques méthodes différentes :

* [Cut plastic jigsaw](https://community.preciousplastic.com/how-to/cut-plastic-jigsaw)
* [Drill and screw through plastic](https://community.preciousplastic.com/how-to/drill-and-screw-through-plastic)
* [Cut plastic with the table saw](https://community.preciousplastic.com/how-to/cut-plastic-with-the-table-saw)
* [Make your sheet shiny](https://community.preciousplastic.com/how-to/make-your-sheet-shiny)
* [Bend plastic sheets](https://community.preciousplastic.com/how-to/bend-plastic-sheets)

![How Sheetpress Works](assets/create/sheetpress_design.svg)

Comment fonctionne la presse à plaques : image svg

Il existe différentes façons de transformer les plaques de plastique pour créer des produits finis. Nous présentons ici deux de nos techniques les plus courantes :

### 1\. Découpe et techniques de menuiserie

Le découpage est le processus le plus courant et il existe un certain nombre de méthodes différentes - vous pouvez utiliser des outils de menuiserie ordinaires tels qu'une scie sauteuse ou une scie à table ou des méthodes plus avancées telles que le fraisage CNC.

* Le PEHD et le PP sont les plus faciles à couper et à utiliser.
* Pour obtenir une coupe nette, il faut trouver le bon outil et la bonne lame.
* Le refroidissement par air est utile si vous y avez accès.
* La découpe produira de nombreuses chutes, veillez à les collecter et à les réutiliser.
* Les joints à encliquetage fonctionnent très bien sur le plastique.

<p class="note">Remarque : si vous utilisez de grandes coupes de plastique, familiarisez-vous avec le matériau et la façon dont il s'affaisse et se plie sur de grandes surfaces. Il peut être très flexible et plus vous en savez sur ses propriétés, plus vous pouvez amplifier ses qualités.</p>

![Stool](assets/create/stool.jpg)

### 2\. Cintrer / Plier

C'est très amusant ! En appliquant la bonne température pendant un certain temps, vous pouvez mouler et plier une feuille et la transformer en une forme tridimensionnelle. L'élément le plus important est la température, qui détermine la vitesse et la régularité de la diffusion de la chaleur dans la feuille. Elle dépend du type de plastique, de la taille du pliage et de l'épaisseur de la feuille.

* Une chaleur rapide garantit une diffusion uniforme de la chaleur, ce qui permet un pliage facile et agréable. Si la chaleur est trop lente, le plastique peut brûler à l'extérieur avant que la chaleur n'atteigne les couches internes, ce qui se traduit par une surface irrégulière et des fissures/marques blanches le long du pli.
* Les feuilles plus fines, jusqu'à 6-8 mm, sont plus faciles à plier.  Au-dessus de 6-8 mm, nous recommandons d'utiliser la chaleur double face pour atteindre le milieu assez rapidement pour un pliage propre.
* Cette technique fonctionne bien avec le PS mais est plus difficile à mettre en œuvre avec le PP, le LDPE ou le HDPE.
* Vous pouvez utiliser des cintreuses de ligne, il existe de nombreuses options de bricolage en ligne utilisant du fil Nichrome pour les feuilles de moins de 6mm. Pour les feuilles supérieures, vous aurez besoin de cordes épaisses et la chaleur double face est fortement recommandée. Vous pouvez également utiliser des cintreuses à lame chauffée pour le PEHD et le PP.
* Une option moins efficace pour les petits plis est de chauffer la pièce entière dans un four et de la former avec un moule à deux faces - cela peut être rapide mais restrictif en raison de la taille du four.

![Bends](assets/create/bends.jpg)

<b>Exemples de produits:</b>

* [Make a chair with bent sheets](https://community.preciousplastic.com/how-to/make-a-chair-with-bent-sheets)
* [Make a mould to bend sheets](https://community.preciousplastic.com/how-to/make-a-mould-to-bend-sheets)
* [Make a stool with woodworking techniques](https://community.preciousplastic.com/how-to/make-a-stool-with-woodworking-techniques)

## L'injection

La machine à injecter est un concept très simple : elle chauffe le plastique et remplit un moule sous pression. Vous pouvez créer des objets vraiment beaux et bien finis, et si votre moule est bien fait, vous pouvez maintenir une qualité élevée et constante dans toute votre production. Il peut également être très rapide et facile à saisir pour vos clients ou si vous organisez un atelier - vous pouvez remplir le plastique dans la trémie, il est chauffé et remplit le moule, puis refroidi et voilà ! Le produit est terminé et les gens peuvent le voir et le manipuler.

Comment fonctionne la machine à injecter – image svg

![How Injection Machine Works](assets/create/injection_design.svg)

Cette machine vous permettra d'être polyvalent dans votre production car il vous suffira de visser un autre moule pour fabriquer un produit complètement différent. N'oubliez pas de consulter la section Moules à gauche pour en savoir plus sur les différentes méthodes.

> Astuce:  la machine à injecter est le compagnon idéal d'un atelier car elle est capable de transformer des déchets en un produit de valeur en quelques minutes et les gens/clients peuvent vraiment saisir la valeur de votre processus et de votre produit rapidement.

![Moulds](assets/create/moulds.jpg)

<b>Techniques:</b>

* [Work with the injection machine](https://community.preciousplastic.com/how-to/work-with-the-injection-machine)
* [Make a quick release opening system for injection moulds](https://community.preciousplastic.com/how-to/make-a-quick-release-opening-system-for-injection-moulds)

<b>Exemples de produits</b>

* [Make a carabiner CNC vs laser cut](https://community.preciousplastic.com/how-to/make-a-carabiner-cnc-vs-lasercut)
* [Make a lightswitch and socket](https://community.preciousplastic.com/how-to/make-a-lightswitch-and-socket)
* [Make an interlocking brick](https://community.preciousplastic.com/how-to/make-an-interlocking-brick)
* [Make a handplane - simple mould](https://community.preciousplastic.com/how-to/make-a-handplane-simple-mould)
* [Make a broom hanger](https://community.preciousplastic.com/how-to/make-a-broom-hanger)

<p class="note">Note: assurez-vous de connaître la quantité totale de plastique qui peut entrer dans le baril - cela limitera le volume du produit final. Elle est généralement d'environ 150 cm3, ce qui est assez faible. Ce n'est pas un problème, juste une contrainte de conception ! </p>

![Human with mould](assets/create/person-with-mould.jpg)

## how-tos

Lorsque vous concevez des produits destinés à être fabriqués avec les machines Precious Plastic, votre meilleure ressource sera les how-tos ! Vous y trouverez des conceptions de produits, des techniques et des hacks, certains venant de nous et beaucoup de la communauté. Vous pouvez également consulter le canal Discord ci-dessous, car lorsque (et non si 😉 ) vous êtes bloqué, il est très probable que quelqu'un d'autre ait rencontré le même problème ! Si vous trouvez un nouveau produit, une astuce ou une technique super cool, créez votre propre mode d'emploi pour que nous puissions tous profiter de vos connaissances géniales.

<b>Vous souhaitez faire part de vos commentaires, discuter des produits et de leur conception ou en apprendre davantage sur la communauté ? Rendez-vous sur le canal [**__\#Create__**](https://discordapp.com/invite/yhmfzTZ) sur Discord. Nous y parlons de conception de produits, de fabrication de moules, de mélanges de couleurs, de finition... tout ce qui permet de créer des objets précieux !</b>
