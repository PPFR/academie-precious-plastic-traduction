---
id: injection-moulds
title: Astuces pour ses moules d'injection
sidebar_label: Moules d'injection
---
<style>
:root {
  --highlight: #ffe084;
  --links: rgb(131, 206, 235);
  --hover: rgb(131, 206, 235);
}
</style>
<img style="margin-left:0px;" src="../../assets/create/socket-cnc.jpg" />

# Fabrication de moules d'injection

En principe, toutes les machines ont besoin de moules pour fabriquer quelque chose, qu'il s'agisse d'une plaque, d'une barre ou de tout autre objet. La machine la plus polyvalente pour utiliser des moules est la presse à injection, mais elle a ses limites. Il ne faut pas en avoir peur! Il est simplement important de les connaître pour mieux cadrer votre conception. Commençons par les avantages et les inconvénients :

| Avantages   |     Inconvénients   |
|----------|-------------|
| Idéal pour les pièces de petit volume <150cm³ |  Les moules peuvent être coûteux |
| Temps de cycles courts (<2-5min) |   Processus compliqué avec de nombreuses incertitudes   |
| Les empreintes multiples permettent une production plus rapide |     Volume insuffisant pour des plus grandes pièces  |
| Production fiable de pièces   |  Les pièces à parois fines nécessitent une pression supérieure à celle de la machine actuelle  |
| Des pièces très détaillées peuvent être réalisées | Principalement pour le HDPE et le PP	|
| Des pièces complexes peuvent être conçues grâce à l'ajout d'inserts ou de tiroirs|	|


## Matériaux pour faire un moule

Vous êtes encore là ? Génial ! 
Le matériau le plus courant pour les petits moules est l'aluminium, car il est plus facile à usiner que l'acier et a une durée de vie relativement élevée par rapport aux autres matériaux. Il existe également différents procédés de fabrication qui peuvent être choisis pour la fabrication d'un moule, chacun présentant certains avantages et inconvénients. En voici un aperçu:


| Matériau    |     Application   | Précision | Accessibilité / Disponibilité | Coûts | Durée de vie (injections) |
|----------|-------------|-----|-----|-----|-----|
| Bois / MDF| Non recommandée, le plastique colle au moule |Basse-Moyenne|Haute|Faibles (<100€)|-|
| Acrylique (CNC/ Lasercut) |   Démonstrations/prototypes   |Basse-Moyenne|Haute|Faibles (<100€)|5 à 10|
| Empreinte Silicone |     Prototypage  |Basse|Haute|Faibles (<100€)|jusqu'à 20|
| Impression 3D SLA (résine haute température)   |   Prototypage, petites séries   |Moyenne| Moyenne |Moyenne (>200€)|Jusqu'à 100|
| 🎥 [Aluminium (coulé - moulé)](https://youtu.be/5LhHUBz9uL0)  |	moules pour des formes organiques  |Basse-Moyenne|Haute|Faibles (<100€)|de 2000 à 10 000|
| 🎥 [Aluminium (usiné - fraisé)]( https://youtu.be/ZYFoWP-3MYE)   | Petites et Moyennes séries	|Basse-Moyenne|Basse|Moyenne  (>200€)|de 2000 à 10 000|
| 🎥 [Acier (découpé au laser - 2D)](https://youtu.be/P_zCIXsHkVI )  | Prototypage - grandes séries	|Moyenne (découpe laser)|Moyenne|Faibles (<100€)|Jusqu'à 100 000|
| 🎥 [Acier (soudé)](https://youtu.be/dYG7qcGp5mc )    | Prototypage - grandes séries	|Basse-Moyenne|Haute|Faibles (<100€)|Jusqu'à 100 000|
| Acier (usiné)    |Grandes séries|Très haute|Basse|Hauts (>1.000€)|Jusqu'à 100 000|


# Dimensions de la pièce et du moule

<b>La taille maximale possible de la pièce est définie par plusieurs variables :</b>

1. <b>Volume maximal </b> -> le volume de la pièce (y compris la carotte, les ponts et les canaux)  est-il inférieur à 150 g (pour la machine à injecter standard v3) ?  Si l'on se rapproche de ce volume max (>=130g), une compression supplémentaire du plastique sera nécessaire et le temps de cycle sera  augmenté .
Les flocons ont un volume plus important que le volume fondu du plastique :
<br><br>
<img style="margin-left:0px;" src="../../assets/create/volume.jpg" width="500"/><br>

2. <b>Taille totale du moule </b> peut-il être utilisé avec ma machine à injecter ? 
(v3 Injection : Diamètre : 380mm x 170mm) 
Si vous utilisez une buse à vis et un moule rectangulaire, tenez compte de la distance diagonale pour la largeur/longueur maximale !
<br><br>
<img style="margin-left:0px;" src="../../assets/create/sizes.jpg" width="500"/><br>

3. <b>Profondeur de la pièce</b> -> C'est plus facile si elle est inférieure à ~40mm, car des fraises standard pourront être utilisées pour l'usinage du moule.La plupart des fraises sont limitées à une longueur d'environ 100 mm. Cela peut s'avérer utile si l'on utilise des machines CNC de type hobby avec une faible course en Z. Le moule peut généralement être usiné à partir d'un seul bloc si vous restez en dessous de 80 mm. Cela peut représenter une économie importante dans le processus.  <br><br>
<img style="margin-left:0px;" src="../../assets/create/milling.jpg" width="500"/><br>

4.<b>Surface projetée de toutes les cavités</b>, canaux et  ponts dans le sens de l'ouverture du moule. Cela détermine la force de fermeture nécessaire. Ce n'est pas un problème pour les conceptions de pièces injectées avec des machines d'injection manuelles. Si votre pièce est fabriquée sur une machine d'injection à pression plus élevée, vous devrez peut-être considérer plus précisément ce facteur. <br><br>
<img style="margin-left:0px;" src="../../assets/create/mould.jpg" width="500"/><br>



> _Conseil d'expert_: En réfléchissant aux contraintes de fabrication de votre produit dès le début, vous économiserez du temps et de l'argent. Pour réduire les coûts de fabrication des moules, vous devez concevoir tous les détails aussi grands et aussi peu profonds que possible - cela permet d'utiliser des outils plus grands et d'accélérer la fabrication du moule.

## Rétractation et tolérances

Tous les polymères thermoplastiques se rétractent lorsqu'ils refroidissent à partir de l'état fondu (voir le tableau ci-dessous).


Le taux de rétraction est très important pour la planification du matériau et du moule : le matériau se rétracte généralement vers le centre de la pièce, ce qui signifie que toute découpe ou détail sensible au centre peut entraîner le blocage de la pièce dans le moule.

La prise en compte du retrait est très importante, en particulier pour des éléments d’assemblage (type boîtier – couvercle) où les tolérances pour correspondre aux autres pièces sont serrées. 
Le retrait peut également varier dans une fourchette donnée en fonction des additifs (inconnus) contenus dans le matériau. 
<br>
| Material    |    Taux de rétractation  |
|----------|-------------|
| PEHD |  3,3% |
| LDPE |   3,8%   |
| PP |     2,6%   |
| ABS, PC, PMMA   |   0,6%   |
| PS | 0,5%	|
<br>
<img style="margin-left:0px;" src="../../assets/create/shrinkage.jpg" width="500"/><br>


## Angle de dépouille

Pour éjecter facilement les pièces des moules, toutes les surfaces parallèles au sens de l'ouverture doivent avoir un angle de dépouille. Cela permet de démouler facilement la pièce.

La valeur commune pour commencer est de 2° mais certaines géométries peuvent nécessiter un angle de dépouille plus important, jusqu'à 5°. Il est recommandé d'ajouter un angle de dépouille de 1° pour chaque 25 mm de hauteur de l'élément. 
Plus l'angle de dépouille est important, plus l'éjection de la pièce du moule est facilitée. L'ajout d'angles de dépouille à une pièce n'est pas une considération "naturelle" et entre souvent en conflit avec les objectifs de conception de l'ingénierie mécanique ou de la conception industrielle ; l'objectif est donc d'en inclure autant que possible tout en atteignant vos objectifs de conception et d'ingénierie. Même un angle de dépouille de 0,5° peut faire une grande différence dans la production.

> Conseil d'expert: si vous ajoutez de la texture à vos surfaces, veillez à augmenter l'angle de dépouille à 3-5°.

De nombreux logiciels sur le marché proposent une fonction d'analyse de la dépouille qui peut s'avérer très utile, notamment si vous devez vérifier l'angle de dépouille de nombreuses surfaces sur une seule pièce. Vous pouvez définir une plage d’angles de dépouille autorisés. Voir l'image ci-dessous.

<img style="margin-left:0px;" src="../../assets/create/draft-angle-1.jpg" width="500"/>
<br>
<p class="note">Note: s'il n'est pas possible d'ajouter un angle de dépouille à votre conception, envisagez de travailler avec des  éjecteurs ou des inserts pour réaliser les surfaces droites. N'oubliez pas que cela augmentera considérablement les coûts de fabrication. 
</p>

## Épaisseur de paroi
La bonne épaisseur de paroi est très importante pour la réussite de l'injection de votre pièce. L'épaisseur de la paroi correspond à la matière plastique utilisée, ou plus précisément à son indice de fluidité à chaud (MFI). En fonction de votre matériau, il existe donc une gamme d'épaisseurs de paroi possibles (voir le tableau ci-dessous).

Sachez que les machines d'injection manuelles sont principalement limitées par la pression qu'elles peuvent accumuler - choisissez une valeur dans le haut de la fourchette pour vous assurer que la pièce peut être injectée avec succès. Une épaisseur de paroi éprouvée est supérieure à 2,5 mm, ce qui a bien fonctionné dans la plupart des applications. Dans l'image, vous voyez le boîtier de l'iPhone 7 du dernier kit de démarrage, il montre une simulation de plusieurs épaisseurs de paroi et montre la raison pour laquelle il a atteint une épaisseur de 2,5 mm.

<img style="margin-left:0px;" src="../../assets/create/wall-thickness.jpg" width="500"/>

Les valeurs maximales sont des valeurs recommandées par l'industrie. 
Gardez à l'esprit qu'il est possible de les dépasser autant que vous le souhaitez. Cela peut être une option pour injecter des pièces solides comme un manche de couteau, de petites assiettes ou d'autres objets. L'inconvénient : vous serez confronté à des marques d'enfoncement plus importantes, à un retrait plus élevé et à une durée de cycle plus longue car les pièces doivent refroidir plus longtemps avant de pouvoir être démoulées.

<img style="margin-left:0px;" src="../../assets/create/wall-thickness-1.jpg" width="500"/>

L'épaisseur de la paroi combinée à la distance d'écoulement (de l'entrée à la cavité la plus éloignée) est le facteur le plus important dans la conception de la pièce pour déterminer la force d'injection nécessaire.

En comparaison avec l'industrie, la plupart des espaces de travail équipés de machines d'injection manuelles n'ont pas la possibilité d'augmenter la pression d'injection en changeant de machine d'injection. Soyez donc très prudent lors de la conception de pièces à parois fines (inf. à 1,5mm) et si vous le pouvez, demandez à votre fournisseur de moules la force d'injection nécessaire pour remplir votre moule.

L'épaisseur de la paroi doit être la même sur toute la pièce pour plusieurs raisons :

   - Flux non perturbé à l'intérieur de la pièce.
   - Prévention des marques d'enfoncement

Raisons d'augmenter l'épaisseur de la paroi :

   -     Réduction de la pression nécessaire
   -     Augmentation de la résistance structurelle
   -     (La géométrie de la pièce doit être solide)

Raisons de réduire l'épaisseur de la paroi :

   - Éviter les marques d'enfoncement
   - Réduire l'utilisation de matériaux (volume d'injection nécessaire)
   - Réduire le temps de cycle
    

<br>
| Matière    |   plage d'épaisseurs - industrielle (mm)   | recommandations - machines manuelles (mm) |
|----------|-------------|------------|
| PP |        0,8 - 3,8 | 2,5 - 5 |
| HDPE/LDPE |   0,8 - 4  | 2,5 - 5 |
| PS |     1 - 4   | 2,5 - 5 |
| ABS/PC   |   1,2 - 3,5  | 2,5 - 4,5  |
| POM | 0,8 - 3	| 2,5 - 4 |
| PMMA | 0,6 - 3,8|  2,5 - 5 |


## Rayons ou Congés
Toutes les pièces en plastique qui vous entourent ont des congés qui arrondissent leurs bords. La seule exception se situe sur les bords formés par l'intersection de chaque moitié de moule, ou ceux formés par les éjecteurs et les inserts en combinaison avec d'autres pièces. 
Cela permet d'une part de libérer facilement la pièce et d'autre part d'assurer un flux de matière uniforme dans le moule. En outre, cela réduit les coûts de fabrication du moule dans le processus de fabrication.

> Conseil d'expert: tessayez de garder les rayons intérieurs minimums supérieurs à 0,5 mm pour vous assurer que des outils standard peuvent être utilisés pour la fabrication.

<br>
<img style="margin-left:0px;" src="../../assets/create/fillets.jpg" width="500"/>

N'oubliez pas que l'épaisseur de la paroi doit également être maintenue sur le rayon.

<img style="margin-left:0px;" src="../../assets/create/fillets-1.jpg" width="500"/>

Évitez les  angles marqués dans la conception de la pièce pour faciliter l'écoulement du flux de matière.

<img style="margin-left:0px;" src="../../assets/create/fillets-2.jpg" width="500"/>


## Textes et graphismes

La gravure vous permet d'ajouter du texte et des graphismes à votre produit de manière très économique. Cela permet d'éviter les processus de post-traitement comme l'estampage et l'impression, et rend inutile l'utilisation d'étiquettes supplémentaires. Dans ce processus, le type de plastique peut être ajouté facilement et avec une grande finesse de détails.

Le texte et le graphisme peuvent être intégrés à votre moule de deux manières, soit en gaufrage (donc en relief), soit en défonce (donc en creux) :

- Le texte en creux est l'option la plus économique car il n'est pas nécessaire d'enlever le matériau autour du texte avec une petite fraise..
- le gaufrage est souvent plus facile à lire, car la zone s'assombrit  avec les ombres projetées à l'intérieur.

<img style="margin-left:0px;" src="../../assets/create/text-engrave.jpg" width="500"/>

La valeur la plus pertinente est le rapport entre l’épaisseur minimale du trait et la profondeur. Vous voyez dans l'image ci-dessous comment se présente la forme d'une fraise de gravure. En raison de la forme géométrique, la ligne s'élargit au fur et à mesure que la coupe devient plus profonde. Il existe des mèches de gravure dans une gamme de 10° à 90° et l'avantage est que vous obtenez déjà un angle de dépouille par défaut. Sur les petites hauteurs de police, l'angle de dépouille est à peine visible.

La profondeur du texte et des graphiques gravés doit être supérieure à >0,2 et jusqu'à 0,5 mm. Les éléments plus profonds  pourront être usinés avec de petites fraises. Les machines de base ne peuvent utiliser que des fraises d'une taille supérieure ~>0,5mm et la largeur du trait doit être adaptée en conséquence.


Tous les embouts de gravure ont une pointe plate ou ronde et la plus petite taille de pointe commence à 0,1mm.

<img style="margin-left:0px;" src="../../assets/create/text-graphics.jpg" width="500"/>

## Trous et découpes

Les trous ou les découpes intégrés dans les pièces injectées vous font gagner du temps lors du post-traitement de vos pièces et des opérations secondaires.
Mais ils comportent le risque de créer des lignes de soudure lorsque le plastique s'écoule autour d'eux et se refroidit en cours de route. Ces lignes de soudure peuvent ajouter une faiblesse structurelle à leur emplacement. Il est donc important de savoir si une charge est appliquée à la zone située derrière elles, là où le plastique fusionne.

<img style="margin-left:0px;" src="../../assets/create/socket-holes.jpg" width="500"/>

Le même effet se produira si vous travaillez avec plusieurs points d'injection.

Si les forces appliquées à cette zone sont critiques, vous pouvez également envisager des méthodes de post-traitement telles que le perçage, la découpe laser, le fraisage CNC ou la découpe à l'emporte-pièce. Cependant, cela ajoute une étape supplémentaire au processus et augmente les coûts de fabrication.


## Nervures

Pour augmenter la résistance structurelle tout en conservant la même épaisseur de paroi, des nervures peuvent être ajoutées à une pièce dont on veut assurer la rigidité. 
Ce processus de conception peut prendre beaucoup de temps (contrairement à l'augmentation de l'épaisseur de la paroi), mais il permet de mieux prévenir les marques d'enfoncement et de réduire l'utilisation de matière.

<img style="margin-left:0px;" src="../../assets/create/socket-rib.jpg" width="500"/>

L'épaisseur des nervures doit être d'environ 0,4 à 0,6 fois l'épaisseur de la paroi de la pièce - restez sur l'extrémité inférieure pour éviter les marques d'enfoncement. 
La hauteur des nervures de surface doit être inférieure au facteur 3 de l'épaisseur de la paroi.

<img style="margin-left:0px;" src="../../assets/create/socket-rib-1.jpg" width="500"/>

Veillez à tenir compte de la direction des forces appliquées quand vous concevez les nervures - surtout pour les pièces longues.

<img style="margin-left:0px;" src="../../assets/create/ribbon.jpg" width="500"/>

Pour éviter les marques d'enfoncement sur les surfaces esthétiques, la structure des nervures peut être décalée aux points d'intersection, où il y a plus de matière. Veillez à ce que l'angle de dépouille et les congés soient également appliqués à toutes les nervures pour garantir une éjection facile de la pièce. 
Le congé de fond pour les nervures est un choix difficile. Essayez de réduire les contraintes dans la pièce tout en évitant l'accumulation de matière et les marques d'enfoncement.


<img style="margin-left:0px;" src="../../assets/create/material-flow.jpg" width="500"/>

## Texture des surfaces

Vous pouvez ajouter une texture de surface aux pièces. Les détails les plus importants peuvent être implémentés dans votre modèle CAO et être gravés pendant la production. Pour une structure de surface plus fine, des méthodes de post-traitement telles que le sablage peuvent être utilisées pour obtenir une texture de surface rugueuse.

Si vous ajoutez une texture à vos surfaces, veillez à augmenter l'angle de dépouille à 3-5°. Vous trouverez ci-dessous un exemple de moule en aluminium avec une finition sablée à l'intérieur.


<img style="margin-left:0px;" src="../../assets/create/texture-sandblast.jpg" width="500"/>


## Réduire les coûts de fabrication des moules

| Suggestion    |     Effet   |
|----------|-------------|
| Réduire le nombre d'empreintes                    | Le temps d'usinage est multiplié par le nombre d'empreintes dans un moule. Le temps de préparation reste le même. |
| Éviter les contre-dépouilles                                 |  Rend inutile l'utilisation d’éjecteurs ou d'inserts.  |
| Éviter les détails inférieurs à 1 mm.              |   Permet d'usiner le moule sans utiliser l'électroérosion.  |
| Optimiser la géométrie de la pièce et du moule pour l’usinage  | Économise la communication et accélère le processus de fabrication du moule.   |
| Utiliser des finitions de qualité inférieure                   |	Permet d'économiser de la main-d'œuvre dans le post-traitement du moule.     |
| Anticipez les opérations secondaires après l'injection   |	Cela peut éviter des glissières coûteuses ou des difficultés dans la fabrication du moule. |
| Réduisez le nombre de pièces du moule        | Réduit d’autant la complexité de sa fabrication. |


## Préparez-vous à partager vos plans

Enfin, ce qui est cool avec les moules à injection, c'est qu'ils proviennent souvent de fichiers CAO et peuvent être partagés sur le web. Un fichier numérique permet à d'autres de reproduire le moule 🎉 Vous devriez partager vos fichiers CAO ou en tant que fichiers .step.

Dans le cas de géométries simples, les fichiers 2D comme .svg ou .dxf peuvent être utilisés pour faciliter le transfert de fichiers vers le fabricant de moules.  Si vous utilisez des fichiers .svg, assurez-vous d'ajouter un dessin technique afin que le mouliste connaisse la profondeur de toutes les caractéristiques. 
Considérez l'effort supplémentaire pour la génération du fichier CAO 3D.

S'assurer que les fichiers 3D sont des géométries fermées solides permet d'accélérer le processus. Des programmes comme Rhino peuvent provoquer des erreurs dans la génération du modèle. Vous pouvez ouvrir vos fichiers .step dans divers visualiseurs de fichiers et programmes de CAO faciles d'accès comme Fusion 360, Solidworks eDrawings, A360 ou onShape pour vérifier si le fichier ne contient qu'une seule partie solide.

Les logiciels polygonaux comme 3ds max, Cinema 4D, (Blender) et openSCAD sont insuffisants pour construire une géométrie fermée. Ils peuvent être utilisés dans de rares cas, comme des modèles organiques complexes, mais le processus complet de fabrication devient alors plus compliqué.

<b>Vous souhaitez faire part de vos expériences, discuter des produits et de leur conception ou en apprendre davantage sur la communauté ? Rendez-vous sur le canal [#Create](https://discordapp.com/invite/yhmfzTZ) channel on Discord.sur Discord. Nous y parlons de la conception des produits, de la fabrication des moules, de la finition des objets... Bref de tout pour créer de nouveaux produits!</b>
