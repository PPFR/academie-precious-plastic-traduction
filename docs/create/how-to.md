---
id: howto
title: Apprendre plus
sidebar_label: How to's
---

<style>
:root {
  --highlight: #ffe084;
  --links: rgb(131, 206, 235);
  --hover: rgb(131, 206, 235);
}
</style>


![How to](../../assets/create/howto2.png)

# Apprendre plus avec les tutos

La rubrique "comment faire" de la communauté est un endroit puissant du Web. Ici, des personnes du monde entier (comme vous) peuvent partager les choses qu'elles ont apprises de manière structurée. Cet outil est le cœur de notre communauté collaborative à sources ouvertes.
Nous comptons sur vous tous pour trouver des connaissances auprès de nous, puis pour partager en retour vos expériences et vos apprentissages. De cette façon, nous résolvons tous ensemble le problème des déchets plastiques ! [Pour en savoir plus, lisez ici pourquoi il est crucial de contribuer](https://community.preciousplastic.com/academy/universe/contribute)  

> Conseil d'expert: jetez un coup d'oeil à nos [tutoriels](https://community.preciousplastic.com/how-to) c'est amusant de les parcourir :)

# How to make... a How-to
Bien, vous avez jeté un coup d'œil aux how-tos de la communauté, peut-être utilisé certains d'entre eux, et maintenant vous êtes prêt à poster le vôtre. Nous et tous les membres de la communauté Precious Plastic apprécions VRAIMENT BEAUCOUP que vous preniez le temps de documenter vos expériences.

Les procédés et leurs mises en  œuvre peuvent être complexes. 
Il est tentant de se perdre dans la quête ultime pour créer le parfait mode d'emploi en 45 étapes. Visez cet objectif. Nous adorons ça. Mais de façon réaliste, la plupart des autres membres peuvent obtenir beaucoup d'informations à partir d'un bon modèle 3D ou de quelques photos. Si vous les diffusez avec quelques étapes qui expliquent grossièrement ce qui se passe et pourquoi vous le faites, cela aide déjà beaucoup. 

Voici un guide à suivre pour structurer vos contributions: 



## Intro
<b>Titre de votre tuto:</b> Transmet-il bien l'objectif ? Est-il facile à comprendre ? N'oubliez pas qu'il doit être court.

<b>Tags</b> Séléctionnez des mots-clés pour classer votre guide parmi les autres (4 au maximum). Choisissez des termes précis pour que votre tutoriel soit trouvé.

<b>Combien de temsp cela prend-il?</b> Aidez les autres à plannifier leurs temps de travail.

<b>Niveau de difficulté ?</b> Jamais évident à établir ! Essayez de le définir pour le type d'utilisateur qui utilisera votre guide. La création d'un moule de base pour une personne sans expérience sera probablement assez difficile. Mais pour un fabricant de moules, c'est facile.

<b>Brève description de votre tutoriel:</b> ne dépassez pas 300 caractères!

<b>Fichiers joints</b> Avez-vous des dessins CAO, des rapports d'essais ou d'autres ressources qui pourraient aider? Zippez-les dans un dossier et téléchargez-les ici. Essayez de ne pas dépasser 1Mb.

<b>Photo de couverture:</b> Une image "en paysage" qui montre clairement votre produit ou votre processus. Nous sommes des créatures visuelles, et on dit qu’une illustration vaut 1000 mots. Si possible dimensionnez ce visuel à 1280x960px.

![How to](../../assets/create/how-to-title.jpg)

## Étapes

Ok, passons aux choses sérieuses. Un tutoriel c'est comme une recette de cuisine, à suivre pas à pas (step en anglais). Chaque étape nécessite une intro, une description et des photos. **Vous devez avoir au moins trois étapes**, et si vous voulez en ajouter d'autres, vous devez cliquer sur "Ajouter une étape" en bas de la page.

<b>Intro:</b> là encore, soyez bref. 

<b>Description:</b>  réduisez-la à l'essentiel et ne la transformez pas en un flot de texte. Si vous avez trop de texte pour une étape, cela signifie probablement que votre découpage est trop ambitieux. Préférez diviser cette partie en deux étapes qui seront plus facile à digérer pour les autres.

<b>Photos:</b> assurez-vous qu'elles sont claires et utiles pour réaliser l'étape décrite.

![How to](../../assets/create/how-to-step.jpg)


> __Conseil d'expert :__ Le mieux est de toujours conserver des traces de ce que vous faites, (avec beaucoup de notes et de photos) pendant tout le processus, afin d'avoir facilement un contenu prêt à être partagé.

# Recommandations

### ✏️ Text
- max. 700 caractères par étape
- Upload Minimum 3 étapes
- Restez simple ! Imaginez votre tutoriel comme un guide pour les débutants.
- Nous vous recommandons d'utiliser un langage simple et un ton conversationnel.
- Il est préférable d'avoir plusieurs étapes faciles que quelques étapes très complexes.
- Si vous avez trop d'informations à mettre dans une seule étape, essayez de la diviser en plusieurs étapes.

### 📸 Images
- Chaque étape doit comporter 1 à 3 images.
- outes les images doivent être au format paysage (4:3), de préférence à (1920x1440 px).
- Image de couverture agréable et propre montrant ce qu'on va apprendre dans le tutoriel.
- Il peut être utile d'inclure des dessins techniques ou des modèles, si les formats ne passent pas en "images" ajoutez-les comme pièces jointes disponibles au téléchargement.
- Essayez de faire des photos claires et nettes montrant ce qui doit être vu (nettoyer votre espace peut aider ;)).
- Si possible, chaque étape doit inclure une photo montrant le résultat de cette étape.
- Une bonne photo du résultat final est très précieuse.


### 📦 Fichiers à télécharger

- Ajoutez des documents utiles en complément de votre documentation : dessins détaillés, fichiers CAO, liste des matériaux, etc.
- Regroupez tous vos fichiers dans un seul fichier .zip. plus facile à partager :)


## Conseils et astuces :

1. <b>les titres sont puissants</b> Choisissez-les judicieusement et soyez bref
2. <b>Utilisez des tags,</b> les mots-clés à la base de l’organisation : c'est grâce à eux que les gens trouveront votre guide pratique, alors assurez-vous qu'ils sont précis et pertinents.
3. <b>Rédigez des textes courts mais informatifs:</b>  croyez-nous, nous avons tous un temps d'attention limité.
4. <b>Utilisez des images à chaque étape</b> cela aidera l'utilisateur à visualiser vos instructions. Veillez à ce que vos images soient claires et prises avec un bon appareil photo (les smartphones conviennent aussi !).

## Terminé ?

Mega génial. Vous faites partie de cette aventure avec nous et nous vous sommes très reconnaissants de partager vos connaissances avec le reste de la communauté Precious Plastic ! Asseyez-vous, détendez-vous et sentez [vos points de karma ](../../universe/contribute/#5-raisons-pour-lesquelles-vous-devriez-partager-en-retour)  augmenter.

<b>Vous voulez partager vos commentaires, discuter des produits et de leur conception ou en apprendre davantage sur la communauté? Rendez-vous sur le canal [#Create](https://discordapp.com/invite/yhmfzTZ)sur Discord. Nous y parlons de conception de produits, de fabrication de moules, de mélanges de couleurs, de finition... tout ce qui permet de créer des objets précieux !</b>
