---
id: Setup_Payment
title: Options de paiement
sidebar_label: Options de paiement
---

<style>
:root {
  --highlight: #f7b77b;
  --hover: #f7b77b;
}
</style>


# Configurez votre option de paiement 💸

Pour pouvoir vendre sur le Bazar, vous devrez vous assurer que votre paiement est correctement configuré. Nous fournissons deux processeurs de paiement sur le Bazar, [Stripe](https://stripe.com/en-gb-de) et [Paypal](https://paypal.com/). Vous devrez avoir au moins un de ces comptes pour gérer vos paiements, remboursements et factures.


![Méthodes de paiment](../assets/Business/Stripelogo.png)

# Connectez votre compte Stripe

Commençons par le début: Pour connecter votre compte Bazar à Stripe, vous avez besoin d'un compte Stripe. Vous n'en avez pas encore? [Créez-en un](https://dashboard.stripe.com/register).

- La création d'un compte est gratuite
- Vous devez avoir une organisation enregistrée dans un [pays supporté](https://stripe.com/global) par Stripe.
- Si votre pays n'est pas pris en charge, vous pouvez essayer de créer un compte via [Stripe Atlas](https://stripe.com/atlas), ou trouver un autre moyen créatif de contourner le problème.

Une fois que vous êtes prêt, vous pouvez connecter votre compte Bazar à votre compte Stripe.


## Étape 1 : Ajouter une méthode de paiement

- Connectez-vous au [tableau de bord](https://newbazar.preciousplastic.com/my_admin_panel.php) Vendeur
- Allez dans Administration/ Méthodes de paiement.
- Cliquez sur '+' pour en ajouter un.

![Méthodes de paiment](../assets/Business/Set%20up%20payment-1-payment%20methods.png)


## Étape 2: Sélectionner processeur (Stripe)

- Sélectionner le processeur Stripe, et remplir les informations requises
- Cliquer ensuite sur ‘Configurer’


![Sélectionner processeur](../assets/Business/Set%20up%20payment-2-add%20method.png)


## Étape 3: Connecter Stripe

- Remplir les informations pour ‘Configurer’
- Trouver votre clé publiable + clé secrète sur votre compte Stripe/Developers/API keys


![Select processor](../assets/Business/Set%20up%20payment-3-Config.png)


## Étape 4: Fini!

- Cliquer sur ‘Créer’ - vous devriez voir le nouveau mode de paiement listé


![Select processor](../assets/Business/Set%20up%20payment-4-create.png)

![Payment methods](../assets/Business/Paypallogo.png)

# Connectez votre compte Paypal

[Paypal](https://paypal.com/) est un outil de paiement très largement utilisé et très accessible dans de nombreux pays. Il peut être bon de le proposer en plus de votre méthode de paiement Stripe pour éviter les conflits avec les paiements.

## Condition requise: Compte Paypal Business

Pour utiliser Paypal pour le Bazar, vous devez avoir un compte Paypal Business actif. Si vous n'en avez pas, [créez-en un](https://www.paypal.com/bizsignup/#/checkAccount) et assurez-vous de le vérifier avec votre adresse e-mail.

## Étape 1: Ajouter une méthode de paiement

- Connectez-vous au [tableau de bord](https://newbazar.preciousplastic.com/my_admin_panel.php) Vendeur
- Allez dans Administration/ Méthodes de paiement.
- Cliquez sur '+' pour en ajouter un.

![Payment methods](../assets/Business/Set%20up%20payment-1-payment%20methods.png)


## Étape 2: Sélectionner processeur (Paypal)

- Sélectionner le processeur ”Paypal Express Checkout”
- Cliquer ensuite sur ‘Configurer’


![Payment methods](../assets/Business/Set_up_paypal_1.png)

## Étape 3: Remplir les détails techniques (1)
- Faites défiler la page jusqu'au champ **Détails Techniques**, c'est là que vous devrez insérer les informations de votre compte Paypal.
- Trouvez votre **Merchant ID** dans votre compte Paypal, sous [Business Information](https://www.paypal.com/businessmanage/account/aboutBusiness).
- Copiez-le dans le champ Détails techniques

![Payment methods](../assets/Business/Set_up_paypal_2.png)

## Étape 4: Remplir les détails techniques (2)
- Demandez votre nom d'utilisateur, votre mot de passe et votre signature dans la section [Gérer l'accès à l'API](https://www.paypal.com/businessprofile/mytools/apiaccess/firstparty).
- Copiez-les dans vos détails techniques

**Lorsque vous copiez des noms et des chiffres, veillez à ne copier que les chiffres et les lettres, et NON les espaces vides.**

![Payment methods](../assets/Business/Set_up_paypal_3.png)

## Étape 5: Remplir les détails techniques (3)
- Sélectionner le mode “Live”
- Cochez l’option Paypal pour la montrer dans le panier


![Payment methods](../assets/Business/Set_up_paypal_5.png)

## Étape 6: Fini!
- Cliquer sur ‘Créer’
- Vous devriez voir le nouveau mode de paiement listé


![Payment methods](../assets/Business/Set_up_paypal_4.png)


## Vous êtes maintenant ouvert aux affaires 🎉
Vous pouvez maintenant commencer à créer des annonces sur le Bazar et commencer à vendre. Bonne chance 🙌🏼 Avant d'aller ajouter des annonces, prenez un moment pour ce dernier point important!



# Les frais de Bazar - 5% pour la communauté
Nous aimerions offrir les services du Bazar sans frais, mais la réalité est la suivante : Le fonctionnement, la maintenance et l'amélioration d'une plateforme comme le Bazar nécessitent beaucoup de ressources financières et humaines.

C'est pourquoi nous avons mis en place la redevance Bazar, qui consiste à prélever 5 % de chaque vente. Ces frais sont utilisés pour payer l'hébergement de la plateforme, la correction des bugs, l'assistance technique aux vendeurs et aux clients, et dans le meilleur des cas, l'amélioration et le développement de la plateforme.

Ainsi, en vendant vos objets précieux sur le Bazar, vous contribuez également à rendre cette place de marché du recyclage du plastique aussi accessible et fluide que possible pour les recycleurs de plastique, afin de les aider à réussir leur projet de recyclage. :)

> **Comment les frais du Bazar sont-ils perçus?** Chaque mois, nous envoyons une facture qui représente 5 % du montant de vos ventes du mois. Gardez cela à l'esprit et calculez-les dans le prix de vos articles!


# Il est temps de créer des annonces 🎁

Le profil et les méthodes de paiement sont configurés?

Alors il est enfin temps d’ajouter les articles que vous souhaitez vendre. Pour tirer le meilleur parti de vos annonces, augmenter vos chances d'obtenir de bonnes ventes et être promu sur la page d'accueil, les bulletins d'information et les médias sociaux, suivez nos [**Directives pour créer de bonnes annonces Bazar**](https://community.preciousplastic.com/academy/business/Image_Size_Guidelines).

## Des questions? Un retour?

N'hésitez pas à nous joindre directement pour nous aider ou si vous pensez que nous avons oublié des conseils ou des informations importantes.

Vous pouvez nous trouver tous les jours sur Discord sur le canal [#🙌bazar-seller](https://discord.gg/2E93VxB3CD) ou nous envoyer un e-mail à **[bazar@preciousplastic.com](mailto:bazar@preciousplastic.com)**.
