---
id: listing_guidelines
title: Créer une Annonce
sidebar_label: Créer une Annonce
---

<style>
:root {
  --highlight: #f7b77b;
  --hover: #f7b77b;
}
</style>


# Créez de précieuses annonces sur le Bazar


En mettant un peu d'amour dans votre annonce, vous attirerez des clients et leur ferez croire en la qualité de votre travail. Un contenu de haute qualité sur le Bazar rendra également l'expérience globale du Bazar plus réussie et rendra donc les utilisateurs plus heureux, ce qui se traduira par **plus de trafic et de ventes pour tout le monde**!

> Les bons listings ont également de meilleures chances d'être **promus sur la page d'accueil, les newsletters et les médias sociaux**, ce qui contribuera à augmenter votre visibilité et vos ventes.

**Voici quelques lignes directrices sur la façon de créer une annonce attrayante qui a plus de chances d'être vendue**. [Cette vidéo](https://www.loom.com/share/b1df010a31b04a79b034f723b579cc4e) passe en revue les éléments essentiels à prendre en compte, qui sont également expliqués ci-dessous.

## 1. Type d'article
Tenez-vous-en à ce pour quoi le Bazar est conçu, car c'est ce que les clients recherchent: machines de recyclage plastique, moules, matières premières et produits fabriqués à partir de plastique recyclé. Les offres de services ne sont pas acceptées.

## 2. Titre
Le titre de votre article doit être aussi simple que possible et inclure les éléments essentiels de votre article. Il ne doit pas contenir trop d'informations, mais juste assez pour que le client puisse immédiatement savoir s'il convient par rapport à ce qu'il recherche.


![Profile Logo](../assets/Business/bazar-listingguide-2.png)


## 3. Images

Le contenu et la qualité de vos images est probablement l'**un des aspects les plus importants et le plus transformateur** pour rendre votre annonce informative et convaincante.  Nous allons donc partager ici quelques bonnes pratiques.

Les images horizontales (paysage) sont fortement encouragées pour une visibilité maximale. La taille minimale recommandée pour les images d'annonces est de **794 x 1000 pixels** avec une résolution de 72 ppi.

![Product Image](../assets/Business/product.png)

### 3.1 Angles multiples
Incluez des photos sous différents angles, afin de garantir une **vue complète** de l'article, de ses composants et de ses proportions.

Il est recommandé d'avoir à la fois des **photos nettes** montrant uniquement l'article, et au moins une photo montrant l'article **dans son contexte** pour avoir une idée de sa taille et de ses proportions.

![Profile Logo](../assets/Business/bazar-listingguide-3.1.png)


### 3.2 Plans rapprochés & grands angles
Les plans rapprochés ainsi que les grands angles aideront à se faire une idée des détails et de la qualité de l’article.

￼
![Profile Logo](../assets/Business/bazar-listingguide-3.2.png)


### 3.3 Format d'expédition (avec les dimensions)
En particulier lors de l'achat d'une machine, il est très utile pour le client d’avoir une photo de la boîte d'expédition avec ses dimensions.

Le client **saura ainsi à quoi s'attendre** lorsqu'il recevra l'article et s'il a besoin d'un équipement spécial pour le décharger (par exemple, une palette de livraison et la nécessité d'un chariot élévateur).

Cela montre également que vous êtes déjà passé par cette étape et vous rend plus **digne de confiance**.

![Profile Logo](../assets/Business/bazar-listingguide-3.3.png)


### 3.4 Dessins CAO
S'ils sont disponibles, il peut être utile de partager les dessins CAO ou techniques de la machine, du moule ou de l'article.

Cela permet de mieux comprendre les **mesures exactes et l'assemblage**, ce qui peut aider le client à évaluer si son équipement existant sera compatible avec votre article.

![Profile Logo](../assets/Business/bazar-listingguide-3.4.png)


# 4. Description

Comme pour les photos, une **description claire et détaillée** de l'article permettra au client de n'avoir aucun doute sur ce qu'il achète, ce qui contribuera également à réduire votre taux de contact avec les clients.

**L'acheteur doit être en mesure de comprendre clairement toutes les caractéristiques principales**, les dimensions, les conditions, etc. - tout ce qu'il doit savoir sur ce qu'il regarde, afin qu'il puisse évaluer s'il va faire un achat ou non.

**Veuillez ne pas inclure de liens vers d'autres plateformes** dans la description de votre article. Nous acceptons les liens vers des contenus qui expliquent davantage l'article et qui ne sont pas pris en charge par le Bazar, par exemple un document PDF.

Le cas échéant, vous pouvez également inclure vos termes et conditions.

![Profile Logo](../assets/Business/bazar-listingguide-4.png)


## 5. Caractéristiques

Remplir la section des caractéristiques fournie par Precious Plastic Bazar aide le client à mieux comprendre l'article qu'il achète et à établir une relation de confiance avec votre magasin.

Veillez à en remplir le plus possible, **idéalement toutes**.

￼![Profile Logo](../assets/Business/bazar-listingguide-5.png)


## 6. Options

Il est possible de **personnaliser votre article en fonction des besoins** du client (les gens adorent ça!). De plus, cela aide les clients à cliquer et à acheter, ce qui réduit les frictions et augmente le taux de conversion.

Quelques exemples :
- Un moule peut devoir s'adapter soit à une **vis**, soit à une **buse conique**.
- Un broyeur peut être livré avec un tamis dont les trous ont des **tailles différentes**.
- Un article peut être disponible dans **différentes gammes de couleurs**.

Les annonces proposant plusieurs options sont généralement plus performantes, car les clients aiment choisir ce qui **convient exactement à leur besoin**.

￼![Profile Logo](../assets/Business/bazar-listingguide-6.png)


## 7. Options d'expédition

Plus vous avez d'options d'expédition, plus vous pouvez toucher un **large éventail de clients**.

L'option la plus simple pour couvrir les options d'expédition dans le monde entier est de définir un prix d'expédition pour **chacune des plus grandes régions** (États-Unis, Amérique du Sud, Europe, Afrique, Russie, Asie, Océanie).

Si vous n'établissez pas de prix par région, essayez d'offrir des frais d'expédition pour **le plus grand nombre de pays possible**. Nous recommandons d'avoir au moins une option de livraison pour l’**Europe et les États-Unis** (où se trouvent les groupes d'utilisateurs les plus actifs sur le Bazar).

> Le Bazar ne pourra **promouvoir que les articles dont l'expédition en Europe et aux États-Unis est activée**, car il s'agit du groupe d'utilisateurs le plus important du Bazar.

Veillez également à bien définir le **prix de chaque article supplémentaire** (en particulier pour les machines, il est très probable que le double de la quantité entraîne le double du prix de l'expédition).

Pour en savoir plus sur la livraison et les régions, consultez notre **[Guide de configuration des options de livraison](https://community.preciousplastic.com/academy/business/International_Shipping)**.

![Profile Logo](../assets/Business/bazar-listingguide-7.png)


# Vous souhaitez être mis en avant ? ✨

Ok, voici sont les directives qui, selon nous, **aideront vos annonces à bien fonctionner sur le Bazar**. Bonne chance!

Nous aimons mettre en valeur le bon travail, et soutenons les vendeurs qui mettent de l'amour dans leur travail et leur apparition sur le Bazar. Si vous souhaitez que vos annonces soient mises en avant (par exemple sur la page d'accueil, dans les campagnes d'emailing ou sur les médias sociaux), assurez-vous de suivre notre **[Guide pour être mis en avant](https://community.preciousplastic.com/academy/business/regions)**.


## Des questions? Un retour?

N'hésitez pas à nous joindre directement pour nous aider ou si vous pensez que nous avons oublié des conseils ou des informations importantes.

Vous pouvez nous trouver tous les jours sur Discord sur le canal [#🙌bazar-seller](https://discord.gg/2E93VxB3CD) ou nous envoyer un e-mail à **[bazar@preciousplastic.com](mailto:bazar@preciousplastic.com)**.
