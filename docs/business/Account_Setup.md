---
id: Account_Setup
title: Création Compte
sidebar_label: Créer votre compte
---

<style>
:root {
  --highlight: #f7b77b;
  --hover: #f7b77b;
}
</style>


# Mettre en place votre compte Vendeur

C’est cool que vous vouliez commencer à vendre sur le Bazar du Precious Plastic! Vous avez déjà **[créé un compte](https://bazar.preciousplastic.com/index.php?dispatch=companies.apply_for_vendor)** vendeur et reçu un e-mail de confirmation pour la création de votre compte? Alors passons en revue les étapes pour bien vous lancer!

Pour construire et renforcer votre activité sur le Bazar, il est important d'avoir un profil unique et digne de confiance.

**Voici quelques conseils sur la façon de créer un profil attrayant.**

> Info: Seules les annonces venant d’un profil de vendeur adéquat seront **mises en avant sur la page d'accueil, les bulletins d'information et les médias sociaux**, ce qui contribuera à augmenter votre visibilité et vos ventes.

## 1. Allez sur votre profil Vendeur

Connectez-vous à votre [tableau de bord](https://bazar.preciousplastic.com/vendor.php?dispatch=auth.login_form&return_url=vendor.php) de vendeur et ouvrez votre profil.

![Profil Vendeur](../assets/gif/seller_profile.gif)


## 2. Complétez les informations de votre profil

- Vérifiez l'exactitude des informations de votre profil
- Il est nécessaire d’**ajouter sa localisation** afin de pouvoir créer une épingle sur la [carte](https://bazar.preciousplastic.com/index.php?dispatch=companies.catalog) des vendeurs. Cela aidera votre communauté locale à vous trouver.

![Vérifier Profil](../assets/gif/seller_map.gif)


## 3. Ajoutez une description
![Ajoutez Description](../assets/gif/Description.gif)

Une description détaillée aide les acheteurs potentiels à vous connaître, vous et votre organisation. Racontez-leur votre histoire, comment vous avez commencé et quel est votre objectif.

Une description complète de votre entreprise doit indiquer:

* Qui vous êtes
* Ce qui vous représente (mission/motivation)
* Les activités principales de votre entité
* Références passées, etc.

Vous pouvez également ajouter une photo de votre espace de travail ou de votre magasin pour donner de la personnalité et une meilleure impression de votre projet.

> Toute information pertinente qui fournira des détails au client sur votre entreprise lui permettra de **faire confiance à votre profil** et de se sentir à l'aise pour acheter vos articles.


![Logo du Profil](../assets/Business/bazar-profileguide-5.png)



## 4. Ajoutez vos logos

![Ajouter Logo](../assets/gif/add_logo.gif)

Veillez à ce que votre profil soit reconnaissable en téléchargeant un logo sur votre compte. Nous vous recommandons d'utiliser le logo de votre entreprise (si vous n'en avez pas, vous pouvez en créer un avec notre [générateur de logo](https://community.preciousplastic.com/academy/universe/yourlogo)).

![Profile Logo](../assets/Business/bazar-profileguide-1.png)

- Le premier logo est celui qui apparaît sur votre photo de profil
- Le deuxième apparaîtra sur la facture de vos clients

La taille recommandée pour votre logo est **250x250 pixels**, et sa taille **ne doit pas dépasser les 5 MB**.

## 5. Indiquer les options de paiement

Afin de réaliser des ventes sur le Bazar, il est nécessaire de mettre en place **au moins une option de paiement** (évidemment!).

Afin d’accroître le nombre de clients potentiels, essayez de configurer les deux modes de paiement - **Carte de crédit et Paypal** - pour répondre au maximum de besoins potentiels.

Si vous voulez plus d’informations sur la manière d’installer une option de paiement, merci de vous référer au guide pour [Mettre en place une méthode de paiement](https://community.preciousplastic.com/academy/business/Setup_Payment).

![Logo Profil](../assets/Business/bazar-profileguide-3.png)



# Tout est prêt? Il est temps de créer des annonces!
Si vous avez complété toutes ces étapes, votre profil devrait être correctement configuré pour vendre et **obtenir de bons résultats sur le Bazar**.

Il est maintenant temps d’ajouter les articles que vous souhaitez vendre. Pour **tirer le meilleur parti de vos annonces**, augmenter vos chances de réaliser de bonnes ventes et être promu sur la page d'accueil, suivez notre [**guide pour créer de bonnes annonces sur le Bazar**](https://community.preciousplastic.com/academy/business/Image_Size_Guidelines).

## Obtenez des avis

Les avis des clients reflètent l'expérience d'achat et peuvent porter sur: la qualité de votre communication, la qualité de vos articles, la livraison, etc. Ils sont visibles publiquement et peuvent constituer votre portfolio, servant d'**indicateur aux clients potentiels** pour prendre la décision d'acheter chez vous, ou pas.

Nous vous recommandons donc d'assurer une **grande qualité de service** à travers vos échanges avec le client.

Si les clients ne laissent pas d'avis de leur propre chef, vous pouvez **définir le statut de leur commande sur "En attente d'avis"**, ce qui leur permettra de recevoir un rappel pour écrire un avis.

![Logo Profil](../assets/Business/bazar-profileguide-4.png)

## Des questions? Un retour?

N'hésitez pas à nous joindre directement pour nous aider ou si vous pensez que nous avons oublié des conseils ou des informations importantes.

Vous pouvez nous trouver tous les jours sur Discord sur le canal [#🙌bazar-seller](https://discord.gg/2E93VxB3CD) ou nous envoyer un e-mail à **[bazar@preciousplastic.com](mailto:bazar@preciousplastic.com)**.
