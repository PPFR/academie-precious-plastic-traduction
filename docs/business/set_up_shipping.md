---
id: set_up_shipping
title: Configurer l'expédition
sidebar_label: Configurer l'expédition
---

<style>
:root {
  --highlight: #f7b77b;
  --hover: #f7b77b;
}
</style>

![Bazar Images](../assets/Business/world_map.jpeg)

# Configurez vos options d'expédition

Des utilisateurs du monde entier visitent le Bazar tous les jours. Il est important pour vous, en tant que vendeur, de configurer correctement vos options d'expédition, afin que vos annonces soient prêtes à être achetées sans aucun obstacle.

> **Que se passe-t-il si je ne propose pas d'options d'expédition?** Les clients de différents pays verront que votre produit n'est pas configuré pour être expédié dans leur pays. Ils peuvent vous envoyer un message pour vous demander d'ajouter leur pays, mais de nombreux clients ne font pas cette démarche supplémentaire, ce qui **peut vous faire perdre une vente**.

La mise en place de cette procédure dès le départ permettra de faciliter les choses pour les deux parties, et pourra vous aider à augmenter vos ventes sur le Bazar.


## 1. Expédition nationale
Au minimum, votre produit doit avoir un prix d'expédition national (votre pays).

Il existe généralement deux options pour définir un prix d'expédition :

* **Expédition gratuite**: Vous pouvez calculer le prix de l'expédition dans le prix standard de votre produit, de sorte qu'aucun coût supplémentaire ne s'applique. Facile.
* **Frais forfaitaires**: Vous définissez ici un prix d'expédition pour **un article** et pour des **articles supplémentaires**. Pour les petits articles tels que les produits, les moules ou le plastique broyé, le prix d'expédition peut ne pas changer beaucoup avec une quantité plus élevée (il peut donc être gratuit ou inférieur au prix "un article"), mais pour une machine supplémentaire, vous devrez probablement ajouter le même prix d'expédition pour chaque article ajouté.

![Bazar Images](../assets/Business/IS_1.png)


## 2. Régions

Vous pouvez également configurer un taux d'expédition pour une région du monde, qui s'appliquera à tous les pays situés dans cette région. De cette façon, vous pouvez rapidement définir les frais d'expédition pour de nombreux pays.

> La mise en place de l'expédition pour de nombreuses régions élargira votre public et vos clients, mais assurez-vous de connaître les prix et les conditions auxquels vous avez affaire avant, afin de ne pas rencontrer de complications lors d'une transaction avec un client.

Les régions s'affichent par défaut, mais si vous ne souhaitez pas offrir des services d'expédition à toutes les régions, vous pouvez supprimer manuellement les zones individuelles (et les ajouter à nouveau ultérieurement si vous le souhaitez).

Comme les frais d'expédition peuvent varier au sein d'une même région, vous pouvez également ajouter un taux d'expédition différent pour des pays spécifiques *(nous y reviendrons au point suivant)*.

![Bazar Images](../assets/Business/IS_6.png)


## 3. Pays spécifiques

Même si les prix d'expédition vers les pays d'une même région sont souvent relativement similaires les uns aux autres, les prix peuvent varier d'un pays à l'autre.

Pour vous assurer que vous ne facturez pas trop au client ou que vous ne perdez pas d'argent sur l'expédition, vous pouvez également définir des prix individuels pour des pays spécifiques.

> Plus vous avez configuré de pays, plus vos tarifs d'expédition sont précis et équitables (pour les deux parties). D'un autre côté, la mise en place et la mise à jour de ces tarifs demandent plus d'effort. C'est pourquoi il peut être utile de définir des tarifs par région.


![Login](../assets/Business/IS_2.png)

### Est-il possible de configurer un taux d'envoi différent pour une région et un pays spécifique dans cette région?$

Oui ! Vous pouvez configurer un taux qui convient à la plupart des pays dans une région, et ensuite ajouter des taux pour des pays spécifiques, pour ceux où le taux est franchement différent.

![Login](../assets/Business/IS_4.png)


### Assurez-vous d'ajouter les lieux les plus importants du Bazar
Tous les lieux ne sont pas égaux en termes d'impact. Il y a certains pays où se trouvent de nombreux utilisateurs de Bazar. Assurez-vous d'ajouter des options de livraison pour ces pays afin d'augmenter la probabilité d'une vente.


![Bazar Images](../assets/Business/IS_5.png)


## Quels pays se trouvent dans chaque région?
🌍 Region | 🏳 Countries |  <span style="color:white">Countries </span>.
--- | --- | ---
**Europe** | - Albania <br> - Andorra <br> - Armenia <br> - Austria <br> - Azerbaijan <br> - Belarus <br> - Belgium <br> - Bosnia and Herzegovina <br> - Bulgaria <br> - Croatia <br> - Cyprus <br> - Czechia <br> - Denmark <br> - Estonia <br> - Finland <br> - France <br> - Georgia <br> - Germany <br> - Greece <br> - Hungary <br> - Iceland <br> - Ireland <br> - Italy <br> - Kazakhstan <br> - Kosovo <br> - Latvia <br>| - Liechtenstein <br> - Lithuania <br> - Luxembourg<br> - Malta <br> - Moldova <br> - Monaco <br> - Montenegro <br> - Netherlands <br> - North Macedonia <br> - Norway <br> - Poland <br> - Portugal <br> - Romania <br> - Russia <br> - San Marino <br> - Serbia <br> - Slovakia <br> - Slovenia <br> - Spain <br> - Sweden <br> - Switzerland <br> - Turkey <br> - Ukraine <br> - United Kingdom <br> - Vatican City (Holy See)
**North America** | - Antigua and Barbuda <br> - Bahamas <br> - Barbados <br> - Belize <br> - Canada <br> - Costa Rica <br> - Cuba <br> - Dominica <br> - Dominican Republic <br> - El Salvador <br> - Grenada <br> - Guatemala <br> - Haiti <br> - Honduras <br> - Jamaica <br> - Mexico <br> - Nicaragua <br> - Panama <br> - Saint Kitts and Nevis <br> - Saint Lucia <br> - Saint Vincent and the Grenadines <br> - Trinidad and Tobago <br> - United States of America <br> | - Anguilla (UK) <br> - Aruba (Netherlands) <br> - Bermuda (UK) <br> - Bonaire (Netherlands) <br> - British Virgin Islands (UK) <br> - Cayman Islands (UK) <br> - Clipperton Island (France) <br> - Curacao (Netherlands) <br> - Greenlands (Denmark) <br> - Guadeloupe (France) <br> - Martinique (France) <br> - Montserrat (UK) <br> - Navassa Island (USA) <br> - Puerto Rico (USA) <br> - Saba (Netherlands) <br> - Saint Barthelemy (France) <br> - Saint Martin (France) <br> - Saint Pierre and Miquelon (France) <br> - Sint Eustatius (Netherlands) <br> - Sint Maarten (Netherlands) <br> - Turks and Caicos (UK) <br> - US Virgin Islands (USA)
**Oceania** | - Australia <br> - Fiji <br> - Kiribati<br> - Marshall Islands <br> - Micronesia <br> - Nauru <br> - New Zealand <br> - Palau <br>  - Papua New Guinea <br> - Samoa <br> - Solomon Islands <br> - Tonga <br> - Tuvalu <br> - Vanuatu <br>| - American Samoa (USA) <br> - Cook Islands (New Zealand) <br> - French Polynesia (France) <br> - Guam (USA) <br> - New Caledonia (France) <br> - Niue (New Zealand) <br> - Norfolk Island (Australia) <br> - Northern Mariana Islands (USA) <br> - Pitcairn Islands (UK) <br> - Tokelau (New Zealand) <br> - Wake Island (USA) <br> - Wallis and Futuna (France)
**Africa** | - Algeria <br> - Angola <br> - Benin <br> - Botswana <br> - Burkina Faso <br> - Burundi <br> - Cabo Verde <br> - Cameroon <br> - Chad <br> - Comoros <br> - Democratic Republic of the Congo <br> - Republic of the Congo <br> - Djibouti <br> - Egypt <br> - Equatorial Guinea <br> - Ethiopia <br> - Gabon <br> - Gambia <br> - Ghana <br> - Guinea <br> - Guinea-Bissau <br> - Ivory Coast <br> - Kenya <br> - Lesotho <br> - Liberia <br> - Libya <br> | - Madagascar <br> - Malawi <br> - Mali <br> - Mauritania <br> - Mauritius <br> - Morocco <br> - Mozambique <br> - Namibia <br> - Niger <br> - Nigeria <br> - Rwanda <br> - Sao Tome and Principe <br> - Senegal <br> - Seychelles <br> - Sierra Leone <br> - Somalia <br> - South Africa <br> - South Sudan <br> - Sudan <br> - Tanzania <br> - Togo <br> - Tunisia <br> - Uganda <br> - Zambia <br> - Zimbabwe
**Asia** | - Afghanistan <br> - Armenia <br> - Azerbaijan <br> - Bahrain <br> - Bangladesh <br> - Bhutan <br> - Brunei <br> - Cambodia <br> - China <br> - Cyprus <br> - Georgia <br> - India <br> - Indonesia <br> - Iran <br> - Iraq <br> - Israel <br> - Japan <br> - Jordan <br> - Kazakhstan <br> - Kuwait <br> - Kyrgyzstan <br> - Laos <br> - Lebanon <br>  - Malaysia <br> | - Maldives <br> - Mongolia <br>  - Myanmar (Burma) <br> - Nepal <br> - North Korea <br> - Oman <br> - Pakistan <br> - Palestine <br> - Philippines <br> - Qatar <br> - Russia <br> - Saudi Arabia <br> - Singapore <br> - South Korea <br> - Sri Lanka <br> - Syria <br> - Taiwan <br> - Tajikistan <br> - Thailand <br> - Timor-Leste <br> - Turkey <br> - Turkmenistan <br> - United Arab Emirates <br> - Uzbekistan <br> - Vietnam <br> - Yemen
**South America** | - Argentina <br> - Bolivia <br> - Brazil <br> - Chile <br> - Colombia <br> - Ecuador <br> - Guyana <br> - Paraguay <br> | - Peru <br> - Suriname <br> - Uruguay <br> - Venezuela <br> - Falkland Islands (UK) <br> - French Guinea (France) <br> - South Georgia and the South Sandwich Islands (UK)


## Des questions? Un retour?

N'hésitez pas à nous joindre directement pour nous aider ou si vous pensez que nous avons oublié des conseils ou des informations importantes.

Vous pouvez nous trouver tous les jours sur Discord sur le canal [#🙌bazar-seller](https://discord.gg/2E93VxB3CD) ou nous envoyer un e-mail à **[bazar@preciousplastic.com](mailto:bazar@preciousplastic.com)**.
