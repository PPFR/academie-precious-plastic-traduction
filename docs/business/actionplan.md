---
id: actionplan
title: Plan d'Action
sidebar_label: Plan d'Action
---

<div class="videocontainer">
  <iframe width="800" height="400" src="https://www.youtube.com/embed/w6Hyd9c5QMw" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
</div>

<style>
:root {
  --highlight: #f7b77b;
  --hover: #f7b77b;
}
</style>

# Le Plan d'Action

<div class="videoChapters">
<div class="videoChaptersMain">

### Un élément clé de la réussite d'une entreprise de recyclage.

Le plan d'action consiste à réduire votre idée d'entreprise aux éléments les plus importants, afin que vous puissiez voir toutes les pièces mobiles interagir ensemble. Les personnes qui essaient de faire trop de choses à la fois sont un problème courant au sein de notre communauté - cet outil vous aidera à établir des priorités !

> Conseil d'expert: cet outil est un document vivant qui peut grandir et évoluer avec votre entreprise : prenez le temps de le réviser et de le tester sur des clients potentiels (alias des amis) et vous gagnerez beaucoup de temps par la suite.

</div>
<div class="videoChaptersSidebar">

### Video Chapters

- 00.08 Intro
- 00:58 The PP Business Tools and Resources
- 01.47 Action Plan Overview
- 02.39 Action Plan Explained

</div>
</div>

# Le plan d'action expliqué

Le plan d'action est un outil que vous imprimez et sur lequel vous écrivez, vous pouvez le coller sur votre mur pour rester concentré et vous aider à bien fonctionner une fois que vous avez commencé. Joseph vous explique tout ce que vous devez savoir dans cette vidéo, et vous pouvez télécharger le plan d'action dans le kit de téléchargement, ou ci-dessous !

Le plan d'action est divisé en 4 domaines principaux : Mission, Analyse de marché, Opérations et Mesures d'impact.

![Action Plan](assets/Business/actionplan_FR.jpg)


### Mission

Il s'agit du but de l'entreprise, de la direction que vous voulez suivre et du cœur du projet. La mission résume la raison pour laquelle vous créez votre entreprise et définit votre objectif principal. Elle aura très probablement à voir avec le problème des déchets plastiques, mais devrait être spécifique à l'aspect du problème auquel vous vous attaquez.

### Analyse du marché

Elle porte sur toutes les façons dont vous interagissez avec vos clients :

Les **produits et services** sont ce que vous allez fournir à vos clients - vous devez savoir ce que vous souhaitez fournir avant de lancer votre entreprise. Allez-vous vous concentrer sur les ateliers, la construction de machines, la collecte de plastique ou la fabrication de produits? 
S'il s'agit de produits, quels produits allez-vous fabriquer? Essayez de réfléchir aux produits et services que vous pouvez offrir et qui vous différencieront des autres.

Les **groupes cibles** sont les personnes ou organisations que vous allez cibler avec vos produits et services.


Identifiez ceux qui sont les plus susceptibles d'être intéressés par ce que vous avez à offrir: 

1. Quelles sont leurs principales caractéristiques ?
2. Comment pouvez-vous regrouper ces clients ?
3. Quels sont les problèmes que vous résolvez pour eux ?
4. Qu'est-ce qui les motive ?
5. Ce groupe est-il suffisamment important pour vous permettre de gérer une entreprise rentable ?

Chaque groupe aura des attributs et des caractéristiques spécifiques. N'oubliez pas de mentionner tous les groupes concernés, même s'il s'agit d'un groupe futur que vous souhaitez atteindre. La dernière question est particulièrement importante.

L'**engagement**, ce sont les canaux que vous utiliserez pour communiquer avec vos groupes cibles. 
Comment allez-vous faire en sorte qu'ils soient au courant de vos produits ou services? Comment allez-vous maintenir leur engagement après qu'ils aient effectué un achat? Un site web, des médias sociaux, des plates-formes en ligne, des actions sur le marché physique et d'autres points communautaires sont des exemples d'outils d'engagement.

Les **canaux de vente** sont la manière dont vos produits et services sont distribués à vos groupes cibles. 
Ils peuvent être physiques (magasins, marchés, festivals, etc.) ou numériques (Precious Plastic Bazar, boutiques en ligne, etc.) 
- où vos groupes cibles achètent-ils normalement leurs produits et services? Chaque groupe peut avoir besoin d'un canal de vente spécifique, mais peut aussi partager avec d'autres groupes. Il est important de sélectionner le bon canal de vente afin de pouvoir fournir vos produits et services de manière efficace et effective.

### Opérations

Il s'agit de tous les processus et besoins à l'intérieur de votre entreprise.

Les **ressources clés** sont les outils, les machines, l'espace, les autorisations ou les personnes qui sont absolument nécessaires pour mener à bien votre mission et gérer votre entreprise - il peut s'agir d'éléments physiques ou de ressources immatérielles. Il peut s'agir d'objets physiques ou de ressources immatérielles. Par exemple, une machine à plastique précieux, un certain employé de vente ou l'endroit où vous allez installer votre espace de travail.

Les **tâches ou activités clés** sont les actions qui sont indispensables au fonctionnement et à la réussite de votre entreprise et il est important de définir ce qui vous concerne. Par exemple, si vous fabriquez des produits, une production efficace et l'entretien de vos machines peuvent être essentiels. Si vous vous concentrez sur les ateliers, le transport vers les sites peut être essentiel. Sachez que cela changera en fonction du type d'espace de travail que vous gérez. L'énumération de ces éléments vous aidera à vous souvenir de ce sur quoi vous devez vous concentrer, et à vous assurer que vous disposez des bonnes personnes impliquées dans votre espace de travail pour effectuer ces tâches.

Les **frais courants** sont les principaux coûts que vous prévoyez de payer pour gérer votre entreprise. Ils sont mensuels ou hebdomadaires et sont liés au cœur de votre activité. Le loyer, les salaires, l'électricité, etc. en sont des exemples. Il est important de noter que vous pouvez avoir des coûts fixes et variables. Les coûts fixes sont ceux qui ne varient pas si vous produisez des produits et services ou non - le loyer, par exemple. Les coûts variables sont ceux qui dépendent de la quantité de produits et de services que vous fournissez. Par exemple, si vous fabriquez des produits, l'achat de plastique recyclé broyé sera très probablement l'un de vos principaux frais courants.

Les **collaborateurs** sont toutes les personnes, entreprises et organisations en dehors de votre espace de travail qui feront la clé de votre succès. Il est toujours nécessaire (et plus amusant) pour une entreprise de se connecter et de collaborer avec la communauté et les organisations qui entourent votre projet. Parmi les exemples de collaborateurs, citons les fournisseurs, les pépinières d'entreprises, les universités, les organisations gouvernementales locales, un leader communautaire qui aide à commercialiser votre entreprise, les partenariats avec d'autres entreprises, etc. 
N'oubliez pas que les avantages de chaque collaboration dépendront de votre projet - il n'y a pas de recette parfaite. Une question utile à poser est la suivante : avec quels collaborateurs pouvons-nous établir un partenariat pour nous aider avec certaines de nos ressources ou activités clés?

### Mesures d'impact

C'est ainsi que vous pourrez mesurer votre succès.

**Communauté** - quel est l'impact de votre entreprise sur votre communauté locale? Créera-t-elle des emplois verts, de l'éducation, de la sensibilisation, une nouvelle attraction pour le tourisme? Vous pouvez également penser à l'impact que vous aurez sur la communauté des plastiques précieux. Allez-vous mettre vos idées, vos informations et vos produits en libre accès? Qui en bénéficiera?

**Planète** - comment votre entreprise aura-t-elle un impact positif sur l'environnement? Cela concernera très probablement les déchets plastiques, mais essayez d'être précis. Nettoyez-vous les déchets plastiques dans votre ville grâce à des services de collecte? Vous empêchez le plastique d'entrer dans l'océan en nettoyant les plages? Construisez des machines pour que d'autres lieux de travail puissent recycler le plastique? Pensez à ce que vous pourriez mettre en chiffres si vous avez besoin de quantifier votre succès.

Les **flux de revenus** sont toutes les façons dont vous pouvez créer des revenus à partir de votre entreprise. Il ne s'agit pas de l'investissement initial dont vous avez besoin pour démarrer votre entreprise, mais de tout l'argent que vous recevrez mensuellement de vos activités. Vendez-vous des produits? Vous organisez des ateliers? Vous facturez des frais d'entretien des machines que vous construisez? Soyez réaliste et regardez dans la communauté Precious Plastic comment les autres espaces de travail gagnent de l'argent.

![Action Plan Fill](assets/Business/actionplan-fill.jpg)

# 👌 Conseils

- Révisez l'outil Plan d'action plusieurs fois avant de commencer - les choses se révèleront probablement à différents moments et il faudra donc le modifier et l'affiner. Cet outil est un document vivant qui pourra grandir et évoluer avec votre entreprise.
- Définissez un code couleur pour parler à vos groupes cibles
- Vous devez passer le plus de temps possible à définir vos groupes cibles et à vous assurer que vos produits et services répondent à un besoin ou à une envie.
- Pour vos sources de revenus, assurez-vous de vous en tenir à celles qui sont les plus réalisables dans un premier temps.
- Il est beaucoup plus facile de réviser le plan d'action qu'un plan d'affaires complet. Présentez votre plan d'action à des personnes en qui vous avez confiance et que vous respectez pour obtenir leur avis et mettez votre plan à jour en fonction de leurs conseils. Demandez surtout à des amis ou à des membres de votre famille qui font partie de vos groupes cibles - leur avis est le plus important.
- Consultez le kit de démarrage !

<b>Vous voulez partager vos commentaires, discuter affaires ou en apprendre davantage de la communauté ? Rendez-vous sur le canal [#business](https://discordapp.com/invite/n5d8Vrr) sur Discord. Ici, nous parlons argent. Nous aimons les chiffres. Modèles d'affaires, revenus, bazar etc 🤑</b>
