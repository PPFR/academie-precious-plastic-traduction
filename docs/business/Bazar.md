---
id: bazar
title: Vue d'ensemble du Bazar
sidebar_label: Vue d'ensemble
---
<style>
:root {
  --highlight: #f7b77b;
  --hover: #f7b77b;
}
</style>



![Article du Bazar](../assets/Business/bazar-header.png)

# Acheter & Vendre sur le Bazar Precious Plastic


Le **[Bazar Precious Plastic](http://bazar.preciousplastic.com/)** est un marché pair à pair qui met en relation les acheteurs et les vendeurs de machines, de moules, de matières premières et de produits Precious Plastic. Tout ce qui est listé est créé par des espaces de travail Precious Plastic dans le monde entier. Un outil formidable pour se connecter avec d'autres espaces de travail et trouver les articles dont vous avez besoin pour démarrer ou améliorer votre propre espace 👊

## Vous souhaitez acheter sur le Bazar?

![Article du Bazar](../assets/Business/Bazar-Item.png)

Le Bazar fonctionne comme les autres plateformes de e-commerce que l'on trouve sur le web, qui vous permet les choses suivantes:

1. Chercher des annonces ou naviguer par catégorie.
2. Trouver des offres de divers vendeurs à travers le monde (achetez local si vous le pouvez 🌍).
3. Ajouter des articles à votre panier.
4. Régler vos achats avec une carte de crédit. **Vous devrez passer à la caisse pour chaque vendeur, le paiement allant directement dans sa poche.**
5. Recvoir vos articles.
6. Laisser un avis sur le vendeur pour aider la communauté.

> Precious Plastic perçoit **5% de frais de transaction** lorsque des articles sont achetés sur le Bazar, ce qui nous aide à payer les coûts de fonctionnement de la plateforme et à l'améliorer au fil du temps 💪 💪

## Vous souhaitez vendre sur le Bazar?
Si votre espace de travail est opérationnel et que vous souhaitez **[vendre](https://bazar.preciousplastic.com/index.php?dispatch=companies.apply_for_vendor)** sur le Bazar, il y a quelques exigences à connaître avant de commencer.

* Vous devez être **enregistré en tant qu'entreprise/organisation** (c'est requis par le processeur de paiement)
* Vous acceptez de **finaliser les transactions ayant été initiées sur le Bazar**. Il n'est pas permis de demander aux clients de vous envoyer un e-mail en dehors de la plateforme pour utiliser vos propres méthodes de paiement. Contourner la plateforme pour éviter les frais de transaction nuit à la plateforme et à la communauté Precious Plastic.
* Tous les problèmes de transaction doivent être résolus **entre le client et le vendeur**. Precious Plastic n'est pas responsable des litiges!
* La configuration d'une option de paiement (Stripe) est requise avant de publier vos annonces.
* Lire et accepter les [Termes et Conditions](https://bazar.preciousplastic.com/terms-and-conditions/).


> Ici sur l'Academy, vous pouvez trouver des guides pour vous aider à  mettre en place votre profil - le mieux étant de commencer par [configurer son compte](https://community.preciousplastic.com/academy/business/Account_Setup).


## Amusez-vous bien :)
Amusez-vous à naviguer, acheter, vendre et soutenir les recycleurs engagés!
