---
id: workspacecalculator
title: Calculateur d'Atelier
sidebar_label: Calculateur d'Atelier
---
<div class="videocontainer">
  <iframe width="800" height="400" src="https://www.youtube.com/embed/mcoN76xG9Is" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
</div>

<style>
:root {
  --highlight: #f7b77b;
  --hover: #f7b77b;
}
</style>

# Calculateur d'Atelier

<div class="videoChapters">
<div class="videoChaptersMain">

### Une aide pour comprendre la faisabilité du lancement d'un nouvel espace de travail.

Cet outil de prévision financière n'est pas une science exacte, mais vous donne un point de départ pour savoir combien d'argent vous avez besoin pour commencer, combien de produits et services vous devez vendre par mois afin d'être rentable, et combien de temps cela va prendre pour rembourser votre investissement initial.

Cet outil est applicable à tout type d'entreprise que vous espérez lancer dans le domaine du plastique précieux, que vous soyez constructeur de machines, que vous travailliez dans la production, que vous animiez des ateliers - peu importe ! L'idée de cet outil est que vous allez lister vos sources de revenus, soustraire les coûts et ensuite regarder les résultats récapitulatifs.

</div>
<div class="videoChaptersSidebar">

### Video Chapters

- 00.08 What is it?
- 02.00 Start
- 03.13 Sales
- 04.48 Costs
- 07.53 Dashboard
- 13.03 Extras
</div>
</div

~

### 👇Le lien vers le calculateur en ligne

Cliquez sur le lien ci-dessous pour ouvrir le document des feuilles, puis cliquez sur Fichier > Faire une copie et vous êtes prêt à démarrer !

<b>[Ouvrir le calculateur (en anglais)](https://bit.ly/2YG16Xt)</b>

### 👌  Pour tirer le meilleur parti de cet outil:

- Commencez par la page "Read-me", puis allez de gauche à droite. Vous obtiendrez ainsi le meilleur flux de travail et les meilleurs résultats.
- Ne vous souciez pas d'avoir les chiffres exacts - même une approximation vous donnera une bonne estimation.
- Ne remplissez que les cases entourées d'une bordure bleue.
- Les chiffres que vous saisissez peuvent être inférieurs à 1 - utilisez simplement une décimale (exemple : 0,57).
- Contactez les espaces de travail existants qui font quelque chose de similaire (en utilisant la carte ou Discord), ils pourraient être en mesure de partager certains des chiffres dont vous avez besoin.

<p class="note">Note: cette calculatrice ne prend pas en compte les taxes. Veillez donc à tenir compte des taxes applicables dans votre pays lorsque vous utilisez cette calculatrice.</p>

### 🗝️ Termes Clés

**Recettes**: L'argent que vous rapporte la vente d'un produit ou d'un service.<br>

**Bénéfice**: Argent qu'il vous restera après avoir payé tous les coûts (loyer, matériaux, électricité, etc.).<br>

**Marge bénéficiaire**: Le pourcentage de profit que vous réalisez par rapport au coût total.<br>

**Investissement**: Coût unique que vous payez pour un actif : une machine, un outil, etc.<br>

**Coûts fixes**: Coûts que vous payez mensuellement, trimestriellement ou annuellement et qui ne dépendent pas de la quantité de produits et de services que vous produisez - un loyer par exemple.
<br>

**Coûts variables**: Coûts qui dépendent du nombre de produits et de services que vous produisez - les matériaux par exemple.<br>

**Tableau des flux de trésorerie**: Une vue d'ensemble de la façon dont vous avez dépensé votre argent tout au long de l'année.<br>

**Compte de résultat**: Une vue d'ensemble de l'argent que vous avez gagné ou perdu.

<img style="margin-left: 0; margin-top: 0px" src="../../assets/gif/wscalc.gif" width="600"/>

# 📚 Sections

## 0. READ ME

La fiche __Lisez-moi__ vous indique tout ce que vous devez savoir sur l'utilisation de la calculatrice. Les deux choses les plus importantes à savoir sont que vous ne remplissez que les cellules qui sont en bleu, et que si vous survolez les cellules avec une flèche dans le coin supérieur droit, vous pouvez trouver plus d'informations sur ce sur quoi nous vous interrogeons.

## 1. VENTES

Il s'agit de savoir quels produits vous allez fabriquer et combien de temps et de ressources il vous faudra pour les fabriquer. Vous les avez indiqués dans votre plan d'action - il est maintenant temps de prévoir les coûts. Chaque produit ou service coûte quelque chose à produire, qu'il s'agisse du plastique qui entre dans une poutre ou du temps que vous passez à préparer un atelier.

<img style="margin-left: 0;" src="../../assets/Business/workspace_1.jpg" width="500"/>

**Produits ou services**: vous pouvez énumérer ici tous les produits, machines, ateliers ou services que vous prévoyez de vendre avec votre espace de travail Precious Plastic. Vous pouvez être aussi spécifique que vous le souhaitez - si vous prévoyez de vendre des bols différents à des prix différents, vous pouvez lister chaque type ou couleur séparément. 

**Coût des matériaux pour chaque produit**: estimez combien chaque produit ou service vous coûterait en matériaux. Cela peut être difficile, car vous n'avez pas de chiffres exacts, mais essayez de faire une estimation. Par exemple, si vous savez combien coûte le plastique au kilo, estimez le poids du produit que vous envisagez de fabriquer et multipliez-le par le prix au kilo.

**Heures de production**: estimez le temps qu'il faudra pour créer chaque produit ou service. Par exemple, vous pouvez utiliser les taux de rendement de production indiqués pour chaque machine.

## 2. COÛTS

La feuille de coûts est l'endroit où vous entrez vos activités hebdomadaires, vos investissements et vos coûts mensuels. Ceux-ci comprennent tous les coûts que vous avez en plus de l'investissement direct dans vos produits et services.

**Heures de travail**: comprend toutes les activités que vous ferez sur une base hebdomadaire pour gérer votre entreprise et qui ne sont pas directement liées à la production de vos produits. Des choses comme le marketing, le nettoyage de votre espace, les discussions avec le comptable - vous savez, les choses amusantes 🙂 .

> Conseil d'expert: vous ne savez pas quoi mettre ? Essayez d'entrer en contact avec un espace de travail existant et demandez-leur comment ils passent leur temps pendant la semaine.

**Coûts d'investissement**: vous pouvez saisir ici les coûts uniques dont vous avez besoin pour démarrer votre atelier. Il s'agit par exemple des machines Precious Plastic, de certains outils dont vous aurez besoin, ou du coût de rénovation d'un espace que vous avez en tête. Essayez de vous limiter aux investissements minimums dont vous avez besoin pour démarrer - il est préférable de commencer petit et concentré, puis de faire des investissements supplémentaires une fois que vous commencez à être rentable ou à faire des revenus importants.

**Coûts fixes**: vous saisissez ici tous les coûts qui ne varient pas avec la quantité de produits et de services que vous produisez, mais que vous devez payer régulièrement. Il s'agit notamment du loyer et des services publics de votre espace de travail, de l'hébergement de votre site web ou de l'abonnement à un logiciel de comptabilité. Certains coûts doivent être payés trimestriellement ou annuellement, alors faites quelques calculs rapides pour les estimer sur une base mensuelle pour cette partie. Une autre bonne chose à demander à un espace de travail existant est de savoir quels sont ses principaux coûts mensuels récurrents.


## 3. DASHBOARD

Le __tableau de bord__ est l'endroit où tout commence à s'assembler et où vous pouvez voir l'ensemble du tableau financier du démarrage de votre espace de travail. C'est passionnant ! C'est également là que vous pouvez jouer avec quelques-unes des principales variables pour voir comment elles affectent le résultat final - comme le nombre d'employés, le temps de remboursement de votre investissement initial et le bénéfice total gagné par mois.

Points à considérer :

- Vérifiez le prix de vente de produits et services similaires dans différents canaux de vente (en ligne, en magasin, etc.).
- Faites une estimation prudente du nombre de ventes que vous pensez pouvoir réaliser sur une base mensuelle. Cela changera au fur et à mesure que votre entreprise se développera, mais choisissez des chiffres réalisables pour l'année prochaine.
- Lorsque vous saisissez le montant du salaire horaire, tenez compte de toutes les activités menées dans votre entreprise par vous ou vos copropriétaires et employés: production, marketing, comptabilité.

### Tableau de bord - Marge bénéficiaire

<img style="margin-left: 0;" src="../../assets/Business/workspace_2.jpg" width="700"/>

Et maintenant, quelques chiffres pour s'enthousiasmer ! En remplissant les variables, vous pouvez commencer à analyser les résultats. La première chose à prendre en compte est la marge bénéficiaire par produit - ces chiffres doivent tous être verts, ce qui signifie que le prix que vous demandez couvre les coûts de production de ces produits et services, avec un supplément.

Si la marge bénéficiaire est rouge, vous devrez modifier certaines des variables. Si vous avez l'impression que vous ne pouvez plus augmenter le prix et que les gens continuent d'acheter, essayez de diminuer votre salaire horaire et analysez l'effet sur la marge bénéficiaire. Vous avez peut-être surestimé le coût des matériaux ou le nombre d'heures nécessaires à la production de ce produit ou service. Si vous ne parvenez pas à trouver un prix ou un volume de ventes qui vous donne une marge bénéficiaire positive (dans le vert), vous devriez envisager de ne pas produire ce produit ou service. Même si vous pensez qu'il s'agit d'un bon produit ou service, il est parfois tout simplement impossible pour les clients de payer ce qu'il faut pour couvrir les coûts de production.

De nombreuses personnes s'interrogent sur la marge bénéficiaire à viser pour chaque article, mais il n'existe malheureusement pas de réponse simple à cette question. Il s'agit en fait de trouver un équilibre entre une bonne marge bénéficiaire et la possibilité de vendre une quantité raisonnable de ce produit ou service. Certains produits peuvent avoir une marge bénéficiaire de plusieurs centaines de points de pourcentage.

### Tableau de bord - Résumé


<img style="margin-left: 0;" src="../../assets/Business/workspace_3.jpg" width="700"/>

Une fois que vous avez calculé une bonne marge bénéficiaire pour chacun de vos produits et services, commencez maintenant à analyser les résultats de cette boîte récapitulative :

**L'argent nécessaire pour démarrer**: il s'agit de l'addition de tous vos coûts d'investissement, plus un mois de coûts opérationnels (coûts des matériaux, coûts de la main-d'œuvre et coûts fixes). Un mois de coûts opérationnels est inclus afin que vous ayez suffisamment d'argent pour payer les coûts mensuels au début de vos coûts opérationnels. Si votre "argent nécessaire pour démarrer" vous semble élevé, vous pouvez peut-être essayer de limiter encore plus les coûts d'investissement dont vous avez réellement besoin pour démarrer. Cela pourrait même vous aider à vous concentrer uniquement sur les éléments vraiment cruciaux pour commencer à livrer les clients.

**Mois nécessaires pour rembourser l'investissement**: vous donne une idée du temps qu'il vous faudra pour récupérer l'investissement de départ, qu'il s'agisse d'un prêt, de votre propre argent ou de l'argent que vous avez emprunté à une banque. Il part du principe que les bénéfices réalisés chaque mois sont utilisés pour rembourser l'investissement initial que vous avez fait pour démarrer. Cela ne tient pas compte des conditions de paiement, comme un calendrier de paiement ou un taux d'intérêt que vous pourriez négocier avec une banque ou une autre entité financière, mais vous aide simplement à réfléchir à la rentabilité de cette entreprise par rapport à l'investissement initial dont vous avez besoin pour la lancer. Par exemple, vous pourriez avoir à investir ce qui semble être BEAUCOUP d'argent, mais il pourrait être remboursé dans une période de temps relativement courte. En revanche, s'il vous faut plus de 36 mois (3 ans) pour rembourser l'investissement initial, la cellule affichera "Trop long !" (__too long__), pour vous indiquer que ce n'est pas un délai raisonnable pour rembourser un investissement initial et que vous devez revoir vos variables pour être plus rentable.


**Employés à plein temps**: vous indique le nombre de personnes qui doivent travailler dans cette entreprise pour couvrir tout le travail à effectuer. Par exemple, si la cellule indique 2,5, cela signifie que vous avez besoin de deux employés à temps plein travaillant 40 heures par semaine, et d'un employé travaillant 20 heures par semaine.

**Revenu gagné par mois**: il s'agit du montant gagné par la vente de tous les produits et services que vous produisez.

Les **coûts fixes par mois** sont la somme de tous les coûts fixes mensuels que vous avez saisis sur la page des coûts.

**Coûts matériels par mois**: il s'agit de tous les coûts matériels liés à la production de tous les produits et services chaque mois.

Les **salaires totaux par mois** sont tous les coûts de main-d'œuvre que vous devrez payer en un mois à tous vos employés pour la production des produits et services, ainsi que pour l'exécution des activités hebdomadaires.

Le **bénéfice total gagné par mois** est le montant total d'argent que vous avez gagné après avoir soustrait les coûts des matériaux, les coûts fixes et les coûts mensuels.

### Tableau de bord - Graphiques

Le tableau d'analyse de la rentabilité vous indique combien de temps il vous faudra pour récupérer votre investissement initial. Il montre visuellement ce que la cellule "nombre de mois nécessaires pour amortir l'investissement initial" indique dans le tableau récapitulatif. Le mois où vous avez entièrement remboursé votre investissement initial est celui où la ligne passe du rouge au vert 🎉.

Le graphique des recettes par produit par rapport aux coûts vous permet de voir comment chaque produit se positionne en termes d'argent qu'il rapporte par rapport à l'argent qu'il coûte à produire. Vous pouvez également voir comment ces coûts sont répartis entre le coût des matériaux, le coût de la main-d'œuvre, le coût indirect de la main-d'œuvre et les frais généraux. Le coût indirect de la main-d'œuvre provient des activités hebdomadaires nécessaires au fonctionnement de l'entreprise, qui sont attribuées à chaque produit en fonction du nombre d'unités produites.

Le bénéfice mensuel total par mois vous montre comment chaque produit et service contribue à votre bénéfice total. Ce graphique peut vous aider à décider sur quels produits et services vous devriez vous concentrer - ceux qui rapportent une plus grande part des bénéfices.

Les recettes totales par rapport aux coûts donnent un dernier aperçu de la manière dont vos recettes se comparent aux coûts totaux de l'entreprise.


## 4. Extras

La page des extras vous fournit quelques états financiers de base (comme les flux de trésorerie et les pertes et profits) et vous montre les calculs des pages précédentes. Ces informations peuvent être particulièrement utiles pour les personnes qui sollicitent un prêt bancaire ou une subvention, afin de présenter un dernier aperçu de leur santé financière.

Le **tableau de financement** est une projection pour la première année d'exploitation basée sur les estimations de revenus et de coûts que vous fournissez. Un tableau de financement prévisionnel montre que vous disposez de suffisamment de liquidités pour payer tous les coûts nécessaires d'un mois à l'autre. À la fin de la première année, vous pouvez générer un état des flux de trésorerie sur la base de vos coûts et revenus réels.

Le **compte de résultat** est un aperçu de la santé financière d'une organisation. Le compte de résultat prévisionnel indique si vous aurez réalisé un bénéfice net positif à la fin de l'année.

Le **chiffre d'affaires mensuel** indique la ventilation complète des coûts et des entrées de vos produits et services.

Le **tableau de remboursements** montre comment l'argent dont vous avez besoin pour démarrer est "remboursé" au fil du temps, lorsque vous commencez à réaliser des bénéfices.

# 🎪 Grandes hypothèses

Le problème de la conception d'un outil professionnel applicable dans le monde entier est que vous devez faire des hypothèses pour simplifier les choses. Quelques éléments à prendre en compte :

- Il est possible que si votre entreprise de plastique précieux est très axée sur la production de produits et utilise l'une des plus grosses machines de la version 4, l'électricité aura un effet important sur le coût de chaque produit. Dans l'exemple du calculateur d'espace de travail, l'électricité est supposée être un coût fixe, mais dans ce cas, il s'agit plutôt d'un coût variable qui pourrait être inclus dans la section des coûts des matériaux.
- Le salaire horaire que vous saisissez est supposé être le même pour toutes les personnes travaillant dans votre entreprise. Peut-être que le propriétaire de l'espace de travail ne se verserait pas de salaire, mais garderait simplement tous les bénéfices de l'entreprise. Les personnes également employées par l'entreprise dans la réalité pourraient également recevoir des montants différents.
- Dans le calcul des "mois nécessaires pour rembourser l'investissement", on suppose que tous les bénéfices de chaque mois sont utilisés pour rembourser l'argent nécessaire au démarrage. En réalité, vous pouvez conserver une plus grande partie de ce bénéfice supplémentaire pour réaliser des investissements supplémentaires, comme de nouvelles machines, de nouveaux outils, etc.
- **Ce calculateur n'inclut pas les taxes**, il faut donc en tenir compte lors de l'analyse des résultats, notamment du bénéfice total réalisé par mois et par an. Vous devrez peut-être aussi payer des taxes sur les salaires, ce qui peut augmenter considérablement le coût du "salaire horaire" que vous versez à vos employés.
- L'"argent nécessaire pour démarrer" comprend également un mois de coûts opérationnels, mais si vous voulez être prudent, vous pourriez vouloir quelque chose de plus comme 3-6 mois de coûts opérationnels disponibles lorsque vous démarrez.
- Les "employés à temps plein nécessaires" supposent une semaine de travail de 40 heures, mais cela peut être plus ou moins selon la culture du travail de votre région.

<b>Vous souhaitez partager vos commentaires, discuter affaires ou en apprendre davantage sur la communauté ? Rendez-vous sur le canal [#business](https://discordapp.com/invite/n5d8Vrr) sur Discord. Ici, nous parlons argent. Nous aimons les chiffres. Modèles d'affaires, revenus, bazar etc 🤑</b>
