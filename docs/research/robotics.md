---
id: robotics
title: Tri Robotisé
sidebar_label: tri robotisé
---
<style>
:root {
  --highlight: #798bc5;
  --hover: #798bc5;
}
</style>

# Tri Robotisé

### Pourquoi trier?

68% du plastique mondial ne peut pas être trié par des centres de recyclage automatisés. Bien que le recyclage à l’échelle locale fonctionne différemment selon le pays, les installations de tri classiques peuvent facilement identifier et séparer les plastiques de type PET et PEHD (HDPE). Le reste des plastiques non-identifiés sera probablement laissé non trié en raison du coût élevé de l’opération.  
Ce mélange de plastiques est souvent envoyé dans d'autres installations de traitement des déchets pour être incinéré, ou, en d’autres termes, “revalorisé énergétiquement”. Mais ces résidus de chaîne de tri peuvent aussi finir dans des décharges, ou encore être expédiés à l'étranger où dans le pire des cas, ils sont empilés et brûlés à l'air libre.

Le recyclage local fonctionne différemment selon les pays, mais il existe de nombreuses options pour reconnaître le plastique. Il peut s'agir de machines industrielles haut de gamme coûtant jusqu’à des millions d’euros, ou l’emploi de personnes pour manuellement trier les déchets. <br>
**La mise en place de processus intermédiaires prendra du temps et de l'énergie, et c'est ce à quoi nous nous employons.**

## Accueillons maintenant One Arm ! 

<img style="margin-left: 0; margin-top: 40px; margin-bottom: 40px" src="../../assets/gif/robot.gif" width="500"/>

Pour qu’un système robotisé comme celui-ci ramasse et trie du plastique, il lui faut trois choses:

    - Un bras, pour attraper les objets en plastique
    - Des capteurs (ou yeux) pour voir où se trouvent ces objets
    - Un cerveau, programmé pour prendre des décisions basées sur des paramètres prédéterminés

Simple comme bonjour n’est ce pas ? Chaque élément vient avec son lot de défis particuliers, en plus de celui de faire fonctionner l'ensemble conjointement. 
De nombreux systèmes robotisés de l’industrie sont accompagnés de leur propre système d'exploitation, le plus souvent propriétaire (et nous on préfère les logiciels libres !). 
On a donc opté pour un système appelé Robotic Operating System (ou ROS), c’est un framework qui permet aux différents composants - bras, capteurs, cerveau - de communiquer pour fonctionner correctement. ROS est entièrement modulaire (🎉), ce qui permet d'interchanger différents composants et algorithmes.

![Robotic Belt](assets/robotic_ros.png)

## Les Yeux

Le robot a besoin d'un capteur pour comprendre ce qui se passe devant lui. Dans le cas du robot monobras Precious Plastic, on utilise une caméra 3D qui capture les données de profondeur en infrarouge et les combine avec une image en 2D, afin de créer une reconstitution en 3D. Une Kinect a été utilisée pour nos tests. Grâce à ces données 3D, il est plus facile de comprendre la forme globale d'un objet et ainsi déterminer la meilleure manière de le saisir.

Selon le préhenseur utilisé pour attraper le plastique, tu n'auras peut-être pas besoin d'une caméra 3D sophistiquée pour que le capteur reconnaisse les objets. Il existe des systèmes qui utilisent des caméras 2D ordinaires associées à une technologie d'apprentissage automatique pour cibler et reconnaître les objets à ramasser.

## Le Cerveau

La caméra 3D nous fournit un volume important de données mais le bras ne sait pas encore quoi en faire. En visionnant une représentation graphique des nuages de points, un humain peut facilement identifier chaque groupe de pixels en tant qu’objet différent. Un algorithme nécessite quelques étapes supplémentaires pour arriver au même résultat, à savoir, reconnaître des objets individuels à partir de données brutes des caméras.

Ici à gauche, on remarque la haute résolution de points provenant de la caméra. Avec autant de données, il sera difficile pour le processeur de tout traiter rapidement. À droite, on peut voir comment la résolution a été réduite pour accélérer le traitement.

![Robotic Belt](assets/Research/roboticbelt1.png)

Vu du dessus, l’absence de tapis déroulant facilite l’identification des objets spécifiques (à gauche). Le processeur utilise un algorithme divisant les nuages de points dans l'espace pour les regrouper en tant qu’objets uniques (à droite).

![Robotic Belt](assets/Research/roboticbelt2.png)

Les objets individuels étant désormais identifiés comme des ensembles de points dans l’espace, l’algorithme trouve le centre de l'objet et le point le plus haut pour créer un "point de contact". Une fois ce point identifié dans l'espace, une fonction ROS est capable de créer le chemin, évitant tout obstacle, qu'empruntera l’effecteur du bras robotisé. Une fois arrivé à ce point, l’effecteur saisit l’objet, et le déplace vers un emplacement cible déterminé à l’avance.

## Le Bras

Nous avons acheté un robot industriel d'occasion, qui n'était pas vraiment bon marché (mais ils deviennent moins chers et plus accessibles d’année en année). Le nôtre a une portée de 1,5 mètre et une capacité de charge de 10 kg. Il fonctionne sur 6 axes, pouvant ainsi réaliser une grande variété de poses. Ce n’est pas une flèche, mais nous l'avons choisi parce qu'il était facile à programmer. D'autres géométries de robot (comme un delta ou un SCARA) seraient plus adaptées pour atteindre des vitesses élevées, car plus optimisées pour des opérations de prise et dépose (pick&place).

Pour saisir des objets en plastique, nous utilisons un préhenseur pneumatique. C'est comme un mini tuyau d'aspirateur qui peut aspirer et retenir des objets par effet de succion. Nous utilisons une pompe à vide utilisant de l'air comprimé, qui sert aussi à activer un jet d'air rapide pour expulser l'objet saisi afin d'accélérer le processus de largage.

<img style="margin-left: 0; margin-top: 40px; margin-bottom: 40px" src="../../assets/gif/suck_sort.gif" width="500"/>

Un autre élément clé du processus est la manière dont les objets sont amenés au robot. La mise en place d'un tapis roulant en ferait une boucle de recyclage du plastique entièrement autonome 🎉🎉🎉🎉.

One Arm sait désormais ramasser et déposer des objets en plastique, et sait se diriger. Jusque là tout va bien! On va maintenant améliorer l’effecteur pneumatique et envisager l'installation d'un tapis roulant. Si tu veux en savoir plus ou discuter tri automatisé de plastiques, <a href="https://davehakkens.nl/community/forums/topic/sorting-plastic-with-robotics-v4/">visite les anciens forums</a>. 
Nous avons également publié notre code source pour l’identification des objets individuels sur Github pour le rendre disponible à qui voudrait l'améliorer. <a href="https://github.com/HbirdJ/plastic_picker">Tu peux le retrouver ici! </a>

# Ok, mais Comment différencie-t-il le PEHD du PS?

C'est une excellente question ! Pour que le tout puisse fonctionner, il est très important d'identifier le type de plastique auquel on a affaire. Un robot est intelligent, mais malheureusement, il n'est pas assez autonome pour identifier les différents matériaux (ce qui est probablement une bonne chose). C'est là que nos recherches sur les techniques d'identification des plastiques entrent en jeu.

Comme mentionné précédemment, les chiffres 1 à 7 sont utilisés pour étiqueter les différents types de plastique. Mais la quantité de plastique non marqué, dégradé ou déchiqueté, est non négligeable et nous ne pouvons donc pas compter sur la présence de ce numéro dans toutes les situations.

## Machine Learning ( apprentissage automatisé) 

Les machines ne sont pas assez intelligentes pour reconnaître différents objets par elles-mêmes, mais on peut les entraîner à l'aide de techniques dites d’apprentissage automatique (machine learning). Pour faire simple, on entraîne un algorithme à repérer certains motifs dans des ensembles de données brutes pour déterminer quel est l’objet et donc quel type de plastique le compose. On donne au programme des données labellisées dans un premier temps, pour “l’accompagner”, pour qu’il puisse ensuite reconnaître les motifs dans des ensembles de données inconnus.

Nous avons implémenté une version basique de ce procédé en combinaison avec One Arm, et elle a parfaitement fonctionné sur notre petit ensemble de données inconnues ! Nous avons montré à la caméra 3D des objets connus et lui avons demandé d'enregistrer leur forme, leur taille et leur couleur dans différentes orientations, en lui ayant indiqué à l'avance de quel type de plastique il s'agissait. Ces informations brutes ont été fournies à un algorithme d'apprentissage qui a produit un modèle de <a href="https://en.wikipedia.org/wiki/Support-vector_machine">machine à vecteur de support (SVM)</a> model. Ce modèle SVM a été capable de reconnaître les quinze objets connus sur lesquels nous l'avons entraîné dans 100% des cas !

C'est déjà une formidable avancée, mais il existe malheureusement plus de quinze objets en plastique différents dans le monde. Il est tout à fait possible d'utiliser des ensembles de données plus volumineux et des modèles améliorés pour trier toutes sortes de plastique à l'avenir. Certains centres de tri utilisent cette méthode, mais il reste très difficile de reconnaître le type de plastique d’un objet qui n'a pas été “vu” auparavant.

## Spectroscopie

Une approche alternative consiste à utiliser la spectroscopie, en observant l'interaction entre la matière et le rayonnement électromagnétique. La spectroscopie classique est complexe et coûteuse, mais nous pouvons avoir recours à sa cousine plus abordable: la spectroscopie optique.

![Spectroscopy](assets/Research/spectroscopy.png)


C’est bien beau tout ça, mais **comment ça fonctionne?** C’est plutôt simple en réalité: on envoie de la lumière sur un objet, et en fonction du faisceau reflété, on peut catégoriser l’objet, chaque type de plastique ayant ses caractéristiques propres. Il existe plusieures options pour faire de la spectroscopie optique:

La première est la __spectroscopie dans l’infrarouge proche__, qui utilise des longueurs d'onde proches du spectre infrarouge. C'est une méthode largement utilisée dans l'industrie, et des recherches sont en cours dans la communauté Precious Plastic à ce sujet. L'inconvénient est qu’elle est moins efficace avec les microplastiques ainsi que les plastiques épais ou de couleur sombre, qui se trouvent être en grande quantité.

L'autre option dont nous disposons est la __spectroscopie Raman__, qui est plus complexe mais présente moins d'inconvénients. Elle est plus précise pour identifier le plastique, apporte plus d'informations sur les couleurs, et permet de détecter la contamination par d'autres matériaux ainsi que d'informer sur le niveau de dégradation d’un plastique. C'est incroyable ! Mais accroche toi, ce n'est pas tout : cette méthode peut même être utilisée pour reconnaître les microplastiques dans l'eau!

Les perspectives avec la spectroscopie Raman sont vraiment intéressantes, et c'est la raison  pour laquelle nous avons décidé de nous y casser les dents. Et bien sûr, toutes nos recherches sont disponibles gratuitement en open source. Alors c’est parti!

La relation entre matière et lumière est étudiée depuis longtemps. En 1930, le physicien indien C. V. Raman remporte le prix Nobel pour sa contribution au domaine. Les personnes de ce milieu scientifique ont beaucoup appris de lui depuis. Par exemple, on sait qu’en envoyant de la lumière sur un objet, certaines molécules gagnent en potentiel énergétique, alors que d’autres en perdent. C’est un moyen très pratique pour identifier différentes sortes de plastique.

En d’autres termes, cela signifie que si on envoie de la lumière, disons verte, sur un objet, nous obtiendrons majoritairement de la lumière verte, mais également d’autres couleurs, telles que du violet ou du rouge. La différence de couleurs retournées par l’échantillon nous donne une indication sur la composition de celui-ci, et donc le type de plastique utilisé pour sa fabrication.

<img style="margin-left: 0; margin-top: 40px; margin-bottom: 40px" src="../../assets/Research/raman.png" width="500"/>

Avant de continuer, il convient de présenter quelques hommages. Chez Precious Plastic, nous sommes plutôt débrouillards, nous aimons fabriquer et réparer des choses, développer des machines. Mais cette recherche expérimentale sur l'utilisation de la spectroscopie Raman ne serait pas possible sans le travail de C.V. Raman (évidemment) et de tous les scientifiques et chercheurs qui publient leurs résultats en ligne en open source. Nous ne pourrions pas le faire sans vous (continuez !).

Avec toutes ces informations, nous avons travaillé sur un prototype de capteur, qui peut être utilisé partout dans le monde, pour s'attaquer ensemble à ce problème. 💪

Commençons par un aspect critique. Les équipements optiques sont très sensibles à la poussière. Pour travailler avec les composants, il faut donc un environnement très propre. Tu as besoin d'aide pour construire un espace propre ? Rends toi sur les forums pour apprendre à en construire un avec une boîte, des pièces imprimées en 3D et des morceaux d'aspirateur.

Regardons le fonctionnement du scanner de plus près. Le système est composé de deux niveaux: l'étage d'excitation et l'étage du spectrographe. 

![Scanner](assets/Research/scanner.png)

L'étape d'excitation est conçue pour diriger une source lumineuse vers le matériau que nous essayons d'identifier et de réfléchir le faisceau dans le spectrographe. Afin de mesurer le changement d'énergie des molécules, nous devons disposer d'une source lumineuse de longueur d'onde connue, c'est le premier composant du spectrographe. Un laser vert est utilisé dans notre cas!

Nous utilisons un miroir semi-réfléchissant pour diriger la lumière du laser vers la lentille du microscope qui concentre les photons sur un point précis du plastique inconnu. La même lentille sert aussi à capturer le faisceau réfléchi par le matériau. Étant donné que les molécules présentant un effet Raman qui composent un matériau sont peu nombreuses (par rapport à celles qui restent au même niveau d'énergie), nous devons filtrer le signal obtenu avant de l'envoyer au spectrographe.

Nous utilisons la configuration de Czerny Turner qui permet de séparer les différentes composantes d’un faisceau lumineux afin qu’elles soient collectées et numérisées par le capteur photosensible.

La lumière entre par la fente d'entrée qui limite l'accès à la lumière de sorte à ce que nous n'obtenons que la réponse du matériau et rien d'autre. Un premier miroir est utilisé pour collimater la lumière sur un réseau de diffraction (similaire à un prisme) afin de pouvoir enregistrer toutes les longueurs d'onde de la réponse. Ensuite, un second miroir est utilisé pour focaliser les spectres lumineux sur une matrice CCD où nous pouvons capturer le signal et le transférer à un ordinateur pour une analyse plus approfondie. 

Une matrice CCD peut sembler sophistiquée, mais c'est la même technologie que celle utilisée dans les appareils photo de téléphone portable. Les signaux de structure moléculaire que nous recherchons doivent être extrêmement précis. C'est pourquoi le dernier composant du spectroscope est un refroidisseur thermoélectrique qui contrôle la température de la matrice CCD.

Avec ce dispositif, il est possible d’isoler des molécules et de mesurer leur réponse, puis de transférer les informations à un ordinateur pour une analyse plus approfondie. Merci à Alejandro, membre de notre équipe, pour son travail jusqu'à présent. Le scanner Precious Plastic est encore en cours de développement, mais les avancées montrent de bonnes perspectives.

<b>Tu veux en apprendre plus ou partager tes connaissances? Fonce sur les canaux #robotics et #plastic scanner sur Discord. Nous y discutons efficacité, tri, scanners, et de robotique bien sûr.  🤖</b>
