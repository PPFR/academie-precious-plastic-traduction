---
id: shredderprorun
title: Utiliser le Broyeur Pro
sidebar_label: - l'utiliser
---
<style>
:root {
  --highlight: #f29094;
  --hover: #f29094;
}
</style>

![Shredder](assets/build/shredderpro-run1.jpg)

# Fonctionnement et Maintenance du Broyeur Pro
Félicitations, vous avez construit un Broyeur Pro ! Il est temps de hacher du plastique. Ci-dessous, vous trouverez des informations sur la façon de procéder, les paramètres à utiliser ainsi qu'un processus de travail idéal. N'oubliez pas que l'entretien est également important !

## 🏃‍♀️  Il est temps de broyer du plastique !

### Mise en place

1. Une fois l'interrupteur principal éteint, les arrêts d’urgence désenclenchés et l'interrupteur de direction en position d'arrêt, brancher le broyeur (s'il ne l’était pas) sur une prise triphasée 16A.
2. Vérifiez que la trémie et le tamis (si présent) sont montés correctement.
3. Vérifiez que la trémie est vide de pièces qui pourraient endommager la machine.
4. Après avoir activé l'interrupteur principal, l'écran s'allume.
5. Si l'écran affiche «PRET», activez l'arrêt d'urgence.
6. Exécutez le broyeur en mode inverse pendant quelques secondes pour vérifier que tout fonctionne correctement. L'écran devrait afficher «INVERSION».
7. Enclencher le broyeur en marche avant. L'écran devrait afficher «SHREDDING».
8. La machine est prête à l'emploi.

> Si vous entendez ou voyez quelque chose d'inattendu, arrêtez immédiatement la machine et vérifiez quel est le problème. La machine est suffisamment puissante pour endommager les composants s'ils ne sont pas correctement assemblés.

### Verser du Plastique

Le rendement du broyeur dépend, entre autres, de la forme du plastique. Les boîtiers de CD, les feuilles, les bouchons de bouteilles, les restes d'extrusion / injection… sont faciles à déchiqueter. Les formes rondes ou les gros morceaux sont difficiles à saisir par les lames. Pour augmenter la productivité du broyeur, essayez de verser le plastique de manière à ce que le broyeur puisse l'aggriper. Par exemple, pour la plupart des bouteilles, un bon moyen est de les insérer à l'envers, afin qu'elles puissent être saisies par le goulot.

Pour un deuxième broyage , remplissez simplement la trémie avec le plastique préalablement broyé.

### Fonctionnement

Selon la puissance du moteur, vous devrez peut-être vérifier la charge du moteur (indiquée sur l'écran pendant que la machine est en fonctionnement). La charge du moteur dépend également du matériau, de la forme et de l'épaisseur des pièces à déchiqueter.

Par exemple, les boîtiers de CD en PS (plastique fragile) ne nécessitent pas beaucoup d'énergie. Les grandes bouteilles de HDPE (plastique élastique) nécessitent beaucoup plus de puissance.

Selon le moteur sélectionné, la puissance / intensité nominale est différente. Il convient de s’assurer que le moteur fonctionne sous son ampérage nominal en moyenne. Si le moteur est sous-dimensionné pour le travail et que l'intensité de broyage est supérieure à sa puissance nominale, assurez-vous d’attendre suffisamment de temps pour qu’il puisse refroidir.

Pendant le broyage, nous vous recommandons de surveiller la température des lames, des roulements, de la boîte et du moteur avec un pistolet à température. La température dépend de la charge du moteur et du temps de fonctionnement. Arrêter la machine si la température des lames dépasse 90°C, sinon le plastique commencera à fondre. Les autres composants ne devraient pas trop chauffer sauf si vous effectuez un broyage très intense ou si le temps est chaud. Vérifier la température recommandée par le fabricant, mais de manière générale, ne pas laisser les autres composants dépasser 55°C.


### Nettoyage en profondeur

Il est recommandé d'effectuer les étapes de nettoyage suivantes si vous prévoyez de changer de matériau. Il n'est pas nécessaire de nettoyer si vous changez uniquement la couleur, en gardant le même type de plastique (comme toujours, cela dépend de la sortie souhaitée).

1. Avant de nettoyer la machine, appuyer sur l'arrêt d'urgence et désactiver l'interrupteur principal.
2. Retirer le tamis (s'il est monté).
3. Retirer la trémie.
4. Retirer la plaque supérieure de la boîte.
5. Retirer les lames fixes.
6. Nettoyer entre les lames. Le bas de la machine doit également être nettoyé.
7. Nettoyer la trémie, les lames fixes, le tamis et l'intérieur de la boîte.
8. Monter les lames fixes, la plaque supérieure, la trémie et le tamis (si nécessaire).

### Changement de matériau ou de couleur 

Il est recommandé de broyer du plastique du même type et de la même couleur. Si vous souhaitez changer de matériau ou de couleur, vous devrez nettoyer la machine. Suivez les étapes de «Nettoyage en profondeur». Si vous souhaitez changer la couleur seulement, et que cela ne vous dérange pas qu'il reste des flocons de couleur différente, vous pouvez nettoyer le broyeur plus légèrement, avec une brosse et un tournevis plat pour enlever le plastique entre les lames fixes.

# 🔓 Dépannage
L’Arduino ou l'écran ne fonctionne pas correctement

1. Vérifiez le câblage.
2. Connectez la carte Arduino à un ordinateur et téléversez le code, puis le configurer.


# ⏳ Maintenance

### Avant de commencer

Vérifiez le niveau d'huile de la boîte de vitesses du moteur. Habituellement, la quantité d'huile dépend de la position de montage du moteur. Vérifier les instructions du fabricant.

Assurez-vous que les roulements et les engrenages sont correctement graissés.

![Shredder](assets/build/shredderpro-maintain.jpg)

### Chaque semaine

- Nettoyer en profondeur le broyeur
- Une fois que vous avez retiré toutes les lames, inspectez les lames fixes et mobiles : elles ne devraient pas avoir de rayures. Si vous en voyez, démonter le broyeur et poncer / affûter les lames endommagées.
- Vérifier s'il y a du plastique coincé entre les lames. Le cas échéant, démonter l'axe, nettoyer les lames et remonter le tout.

### Chaque mois

- Démonter les engrenages, les nettoyer et les remonter avec de la graisse neuve.
- Inspecter les roulements à la recherche de fissures et ajouter de la graisse supplémentaire si nécessaire.
- Démonter la boîte, poncer ou affûter la partie endommagée et huiler toutes les plaques. Si les dents sont trop endommagées, les remplacer. Nous vous recommandons de commander des lames supplémentaires pour permettre un entretien plus facile.
- Selon le couplage utilisé, certains composants en caoutchouc seront peut-être à remplacer. Consulter les instructions du fabricant.
- Vérifier la peinture et repeindre si besoin afin d’éviter les problèmes de corrosion sur le long terme.

**Si vous avez besoin d'aide, avez des questions ou cherchez quelqu'un à qui parler dans votre atelier empreint de solitude... Allez sur le canal[#build](https://discordapp.com/invite/XQDmQVT) sur Discord. Nous y discutons de la technique entourant la construction des machines.**
