---
id: extrusionpro
title: Informations sur l'Extrudeuse Pro
sidebar_label: - Vue d'ensemble
---



<style>
:root {
  --highlight: #f29094;
  --hover: #f29094;
}
</style>

![Extruder Pro](assets/build/extruderpro.jpg)

# 📓 Informations sur l’Extrudeuse Pro
| Specification    |     |
|----------|-------------|
| 📓 Type   |     Single Screw   |
| 💎 Version   |     1.0   |
| 💰 Price new material in NL |  +/- €2000 |
| ⚖️ Weight (inc frame) |   110 kg   |
| 📦 Dimension   | 1500 x 600 x 1550 mm|
| ⚙️ Power (W) | 5 kW|
| 🔌 Voltage | 400V|
| ⚡️ AMP | 16A|
| ♻️ Input Flake Size  | Small  |
| 🔩 Screw diameter | 30mm|
| 🔩 Length of screw (mm) | 790 mm |
| 🔩 Effective screw length | 600 mm |
| 🔩 Rated Motor Power | 3 kW |
| ⚙️ Motor Type   |    (check the build section for more details)   |
| - Rated Motor output Torque |  109 Nm |
| - Rated Motor output speed |   263 RPM   |
| - Max. Motor and Inverter power   | 3 kW|
| - Recommended motor shaft   | 30 mm|
| - Heating zones   | 3 |
| - Heating power: max.   | 2 kW|


# 🌐 Modèle 3D
<iframe width="500" height="600" src="https://b2b.partcommunity.com/community/partcloud/embedded.html?route=embedded-viewer&name=Extruder+Pro+v1&model_id=96617&portal=b2b&noAutoload=true&autoRotate=false&hideMenu=true&topColor=%23FFFFFF&bottomColor=%23ffffff&cameraParams=false&varsettransfer=" frameborder="0" id="EmbeddedView-Iframe-96617" allowfullscreen></iframe>



# 🌦 Avantages et inconvénients

### Avantages

L’extrudeuse est l’une des machines les plus rapides et efficaces parmi le panel de machines Precious Plastic. C’est un élément sécurisé et fiable au sein de la famille de machines !

La vis de l’Extrudeuse Pro est dessinée pour fonctionner avec de nombreux types de plastique. Elle vous permettra de travailler par petites séries de différents plastiques. Cette machine est également conçue pour être beaucoup plus polyvalente que son équivalent industriel, lui permettant d’extruder dans divers moules, formes, buses...

### Inconvénients

Cette machine requiert un niveau de compétences en fabrication de machines plus élevé comparé aux autres machines Precious Plastic. Elle nécessite également un moteur puissant et une vis spécifique la rendant plus chère à fabriquer que les autres machines Precious Plastic.

### Normal VS Pro

Comparée à la version précédente de l’Extrudeuse, cette machine :
  - Possède une plus grande vis permettant de traiter un volume plus important
  - L’optimisation du profil de la vis permet une fonte homogène du plastique pour une qualité améliorée
  -  Une amélioration du dessin des pas pour supporter les forces d’extrusion exercées de toutes parts
  - Est conçue pour être utilisée pendant des journées entières
  - Assemblage et maintenance facilités
  - Composants globalement plus solides et testés
  - Boîtier électronique plus sécurisé et facile à fabriquer
  - Trémie plus grande et isolée permettant une meilleur fluidité



# ♻️ Entrée et sortie

Une multitude de types de plastique peuvent être efficacement traités avec cette extrudeuse. Chaque type de plastique a ses propres propriétés et comportements (flexibilité, rigidité, liquidité etc..). Les matériaux ayant une large fourchette de température de fonte sont les plus faciles à travailler. On vous conseille de commencer avec du PP, PEHD, PEBD ou PS puisque les températures requises peuvent être plus imprécises. On vous déconseille d’essayer des polymères dont les fourchettes de températures de fonte sont petites (comme le PET), mais si vous souhaitez tout de même le faire, pensez à bien prendre les précautions de sécurité nécessaires !

Vous pouvez définir la température de l’extrudeuse grâce aux contrôleurs sur le boîtier électronique. Les éléments chauffants sont identifiés et câblés en trois groupes distincts (buse, vis et trémie). On vous recommande d’utiliser les températures indiquées dans le tableau ci-dessous comme règle d’or. Celles-ci sont définies pour s’assurer que le plastique est complètement fondu juste avant qu’il sorte de la buse.


### ⚠️ Faites bien attention à ce que l’extrudeuse soit à la bonne température avant d’allumer le moteur !!!!!!
<br>
| Plastic Type | Temp. de Trémie (C°) | Temp. de Vis (C°) | Temp. de Buse (C°) |
|--------------|-------------------|------------------|------------------|
| PP           | 190               | 200              | 200              |
| PS           | 200               | 210              | 210              |
| HDPE         | 190               | 200              | 200              |
| LDPE         | 190               | 200              | 200              |


Quelques exemples de temps d’extrusion pour différents moules :
    
  - Moule de poutrelle (200mm x 40 mm x 40 mm) : 8min
  - Petit moule de brique lego : 4 min
  - Grand moule de brique lego : 6 min
  - Moule de skateboard : 20min


![Image](assets/build/extruderpro-output.jpg)


# 🙌 Liens Utiles (en anglais)

* [ Acheter ou vendre des pièces et machines sur notre Bazar](https://bazar.preciousplastic.com)
* [ Trouver un fabricant de machine local sur notre carte](https://community.preciousplastic.com/map)
* [ Consulter nos How-Tos pour des améliorations et hacks](https://community.preciousplastic.com/how-to)
* [ How-To : faire un poutrelle lumineuse](https://community.preciousplastic.com/how-to/make-a-lamp-with-beams)
* [ How-To : faire un profilé en forme de “T”](https://community.preciousplastic.com/how-to/make-a-tshape-beam-)
* [ Hack : buse de libération rapide](https://community.preciousplastic.com/how-to/make-a-quick-release-for-the-extrusion-machine)
* [ Hack : buse plate](https://community.preciousplastic.com/how-to/make-a-flat-nozzle-for-the-extrusion-machine)
* [ Documentation relative à la vis d’extrusion](http://www.mie.uth.gr/ekp_yliko/_Chapter_5a.pdf)
* [ Documentation : rhéologie](http://www.polydynamics.com/Rheology.pdf)
* [ Documentation relative à la trémie en forme conique](http://craig-russell.co.uk/demos/cone_calculator/)
* [ Documentation](http://www.mvt.ovgu.de/mvt_media/Vorlesungen/Lecture_SFPS/Folien_SFPS_4-p-2002.pdf)

**Si vous avez besoin d'aide, avez des questions ou cherchez quelqu'un à qui parler dans votre atelier empreint de solitude... Allez sur le canal  [#build](https://discordapp.com/invite/XQDmQVT) sur Discord. Nous y discutons de la technique entourant la construction des machines.**
