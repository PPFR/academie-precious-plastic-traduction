---
id: extrusion 
title: Construire une extrudeuse 
sidebar_label: Extrudeuse
---
<div class="videocontainer"> <iframe width="800" height="400" src="https://www.youtube.com/embed/p4NoY33-Tfo" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe> </div>

<style> :root { --highlight: #f29094; --hover: #f29094; } </style>

<div class="videoChapters"> <div class="videoChaptersMain">

# Construire une extrudeuse

### Quelle est cette machine?

L'extrusion est un processus continu où le plastique broyé pénètre dans la trémie, est chauffé puis pressé à l’aide d’une vis sans fin à travers un long tube. On obtient un filament continu de plastique en sortie, et comme cette machine peut fonctionner sans interruption, il suffit d’avoir assez de plastique et un processus bien rôdé pour pouvoir recycler 24/7 🎉

> Conseil d'expert: pour augmenter l'efficacité et la diversité d’utilisation, nous recommandons de [__mettre à niveau l'extrudeuse__](https://www.youtube.com/watch?v=zNGuuSKE1pY) avec la vis de compression.

</div> <div class="videoChaptersSidebar">

### Video Chapters

* 00:00 Introduction
* 00:46 Hopper
* 02:22 Barrel
* 04:48 Nozzle
* 07:05 Barrel holder
* 08:44 Framework
* 10:43 Electronics
* 15:03 How it works

</div> </div>

Avec cette machine, vous pouvez créer un fin filament (difficile mais faisable), des granulés ou faire preuve de créativité et faire tourner ce filament continu autour d'une forme, ce qui est idéal pour des fins éducatives car le processus est très simple et facile à comprendre. Lorsque le plastique est extrudé, il mélange bien différentes couleurs et produit une couleur homogène et propre.

# 📓 Technical information

| 📓 Type | Extrusion Machine |
|---------|-------------------|
| 💎 Version | 2\.0 |
| 💰 Prix matériaux neufs aux Pays Bas | \+/- €500 + moteur |
| 💰 Prix matériaux d'occase aux Pays Bas | \+/- €200 |
| ⚖️ Poids | 35 kg |
| 📦 Dimensions | 500 x 1020 x 1120 mm |
| ⚙️ Taille des vis | 26 x 600 mm vis à bois |
| ⏱ Temps de fonctionnement maxi | 4H/Day |
| 🔌 Voltage | 380V |
| ⚡️ AMP | 5\.8A |
| ♻️ Taille des flocons en entrée | Petits et moyens |

* Puissance nominale | 1.5 kW minimum.
* Couple | 109 Nm
* Vitesse de sortie | 40-140 r/min

![Extrusion v3](assets/Build/extrusionv3.jpg)

# 🌐 Modèle 3D

<iframe width="500" height="500" src="https://b2b.partcommunity.com/community/partcloud/embedded.html?route=embedded-viewer&name=Extrusion+Basic+V2.0&model_id=96651&portal=b2b&noAutoload=true&autoRotate=false&hideMenu=true&topColor=%23FFFFFF&bottomColor=%23ffffff&cameraParams=false&varsettransfer=" frameborder="0" id="EmbeddedView-Iframe-96651" allowfullscreen></iframe>

# 🛠 Machines et compétences requises

| Extrudeuse | Outils nécéssaires  | Compétences requises |
|------------|---------------------|----------------------|
| <img style="margin-left: 0;" src="../../assets/build/thumb-extrusion.jpg" width="100"/> | Perceuse à colonne<br> - Poste à souder (non spécifique) <br> - meuleuse d'angles | \-Soudure (intermédiaire) <br> - Assemblage (intermédiaire) <br> - Electroniques (intermédiaire) |

# ⚡️ Boîtier électronique

Explication des composants électriques à l'intérieur de cette machine. Plus d'informations et de schémas sont à retrouver dans le kit de téléchargement.

* **Contrôleur PID :** le cerveau de la machine qui sert à régler les températures souhaitées. Il enverra de l'énergie aux éléments de chauffe jusqu'à ce que le PV (point variable) corresponde à la SV (valeur définie). Pour ce faire, il utilise les lectures du thermocouple et du SSR.
* **SSR:** le *Solid State Relay* est un interrupteur électronique qui s'ouvre ou se ferme en fonction du signal qu'il reçoit (du PID).
* **Thermocouple** \: fondamentalement une sonde de température.
* **Élément chauffant** \: élément chauffant qui s'adapte autour d'un tuyau.
* **Interrupteur d'alimentation** : interrupteur mécanique.


* **Indicateur lumineux**\: LED s'allumant lorsque la machine est sous tension (souvent située sur l'interrupteur d'alimentation).


* **Câble d'alimentation :** câble d'alimentation domestique courant.

> *Conseil d’expert: Voici un bon* [*__sujet de forum sur l'électronique__*](https://davehakkens.nl/community/forums/topic/the-big-electronics-topic/)*.*

# 🛠 Trucs et astuces pendant la fabrication

* L'alignement du moteur et des couplages est essentiel, tout désalignement pouvant entraîner une usure future. Pour ce faire, nous vous recommandons de percer des trous dans les plaques et de les fixer au moteur. Alignez puis soudez la plaque en position sur le cadre.
* Tous les boulons utilisés doivent être utilisés en combinaison avec des écrous de blocage pour éviter qu'ils ne se desserrent avec le temps.
* Lors du choix d'un système d'engrenage, gardez à l'esprit que la vis d'extrusion applique une pression latérale à l’intérieur de la boîte de vitesses. Évitez d'utiliser des systèmes linéaires dépourvus de plaque arrière, comme les moteurs des broyeurs de jardin.
* Choisissez des roulements qui peuvent supporter une charge directionnelle (comme un roulement conique).
* L'installation d'un VFD (Variateur électronique de vitesse) avec cette machine rend son utilisation idyllique.

# ♻️ Entrées et Sorties

<b>Type de plastiques utilisables:</b> HDPE, LDPE, PP, PS

**Taille des flocons:** 5 mm Sortie: dépend de la buse, ± 5 kg / h

# ⚙️ Fonctionnement et maintenance

Vous pouvez régler la température à partir des contrôleurs sur le boîtier électronique. 

Les éléments chauffants sont câblés en deux groupes - “buse” et “tube” sont marqués au-dessus des contrôleurs. 

Les trois premiers éléments du tube doivent être réglés à une température légèrement inférieure à celle du dernier près de la buse. Il s'agit de s'assurer que le plastique reçoit un dernier coup de chauffe juste avant sa sortie. 
Ci-dessous, vous pouvez voir un diagramme avec les meilleurs paramètres pour cette machine d’après nos expériences (astuce : vous pouvez vous-même voir ce qui fonctionne le mieux pour vous). 

Les températures optimales ci-dessous sont optimisées pour un moteur tournant à 70 tr / min. Si votre moteur tourne plus vite, vous pourriez avoir besoin d'une température plus élevée car le plastique s'écoule plus rapidement à travers le tube, en ayant du coup moins de temps pour fondre, et vice-versa si votre moteur est plus lent.

> Conseil d'expert: Faites un support pour votre sortie / moule pour le stabiliser et vous faciliter la vie*.*

### Comment faire fonctionner l'extrudeuse

1. Faire chauffer la machine à la température souhaitée.
2. Attendre 20 minutes.
3. Ajouter le plastique choisi dans la trémie.
4. Allumer le moteur.
5. Pendant les 2 premières minutes, le matériau qui sort de la machine sert à nettoyer le tube de vieux plastiques restant des sessions précédentes.
6. La machine est maintenant prête pour la production !

# 🔓 Dépannage

* Paramètres PID pour K-Type - Changer pour J-Type (Demander à Vincent)
* Bourrages machine? Changez les polarités du câblage moteur pour inverser le sens de rotation.
* Le problème le plus courant avec l'extrusion est d'avoir une sortie non-uniforme. Il y a plusieurs raisons pour lesquelles cela pourrait se produire :

1. Le plastique est peut être sale, obstruant la buse et rendant difficile sa sortie.
2. Deux types différents de plastique sont mélangés ensemble, les causes courantes pouvant être la contamination lors du tri ou du plastique de la session précédente laissé dans le tube. Dans ce cas, un type de plastique fond tandis que l'autre obstrue le plastique qui coule à travers la buse.
3. La température n'est pas assez élevée et le plastique dans le tube ne parvient pas à fondre complètement.
4. Vous pouvez résoudre ces problèmes en vérifiant l'intégrité et la pureté de la matière première ou en augmentant la température. Lors du réglage de votre processus, assurez-vous d’entièrement vider le tube avant d'essayer de fabriquer un nouveau produit.

# 🌦 Avantages & inconvénients

| Avantages | inconvénients |
|-----------|---------------|
| Sortie continue | complexe à construire |
| Relativement bon marché | durée de vie limitée |
| Nombreuses possibilités de produits | débit de sortie limité |
| Convivial |  |

# 🌎 Construit par la communauté

<div class="j-slideshow">

![Community Extrusion](assets/Build/community/community_extrusion.jpg)

![Community Extrusion](assets/Build/community/deskfactory.jpg)

![Community Extrusion](assets/Build/community/machinehack-ppukraine.jpg)

![Community Extrusion](assets/Build/community/machines-inajason.jpg)

![Community Extrusion](assets/Build/community/tableextrusion.jpg)

</div>

# 🙌 Liens Utiles (en anglais)

* [__Broyeur et extrudeuse - Électronique industrielle__](https://davehakkens.nl/community/forums/topic/shredder-and-extrusion-industrial-electronics-2/)
* [__Tests d'extrusion de poutres__](https://davehakkens.nl/community/forums/topic/bean-extrusion-optimization/#post-131338)
* [__V4 Production de poutres__](https://davehakkens.nl/community/forums/topic/beam-production-v4/)
* [__V4 Produits d'extrusion__](https://davehakkens.nl/community/forums/topic/extrusion-machine-products-v4/)
* [__V4 Tubes et profils__](https://davehakkens.nl/community/forums/topic/v4-extrusion-tubes-and-profiles/)
* [__V4 Moules d'extrusion__](https://davehakkens.nl/community/forums/topic/extrusion-moulds-v4/)
* [__How-to: moule à poutre__](https://community.preciousplastic.com/how-to/make-a-mould-to-extrude-beams)
* [__How-to: buse plate__](https://community.preciousplastic.com/how-to/make-a-flat-nozzle-for-the-extrusion-machine)
* [__How-to: différentes textures__](https://community.preciousplastic.com/how-to/extrude-different-textures)
* [__How-to: banc__](https://community.preciousplastic.com/how-to/make-a-bench-with-beams)
* [__How-to: étagère__](https://community.preciousplastic.com/how-to/build-a-shelving-system)
* [__How-to: Poutres en verre__](https://community.preciousplastic.com/how-to/make-glasslike-beams)

**Si vous avez besoin d'aide, avez des questions ou cherchez quelqu'un à qui parler dans votre atelier empreint de solitude... Allez sur le canal** [**__\#build__**](https://discordapp.com/invite/XQDmQVT) **sur Discord. Nous y discutons de la technique entourant la construction des machines.**
