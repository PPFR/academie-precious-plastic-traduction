---
id: shredderpro
title: Informations sur le Broyeur Pro
sidebar_label: - Vue d'ensemble
---


![Shredder Pro](assets/build/shredderpro.jpg)

# 📓 Informations sur le Broyeur Pro

| Nom  |  Shredder Pro     |
|----------|-------------|
| 📓 Type   |     Double Shaft Shredder   |
| 💎 Version   |     1.0   |
| 💰 Price new material in NL |  +/- €2200 + motor |
| ⚖️ Weight |   340 kg   |
| 📦 Dimension   | 1205 x 550 x 1512 mm|
| ⚙️ Blade width | 6 mm|
| 🔌 Voltage | 400V|
| ⚡️ AMP | 16A|
| ⏱ Monitoring interval time | 1h|
| ⚙️ Geared Motor   |       |
|  - Nominal Power |  2.2 kW minimum. 3-4 kW recommended |
|  - Nominal Torque |   1100 Nm minimum - 3000 Nm max   |
|  - Output Speed   | 15-25 r/min|
|  - Minimum Service Factor   | 1,5 (2,2kW) / 1,2 (3kW)|


# 🌐 Modèle 3D
<iframe width="500" height="600" src="https://b2b.partcommunity.com/community/partcloud/embedded.html?route=embedded-viewer&name=Shredder+Pro+v1&model_id=96615&portal=b2b&noAutoload=true&autoRotate=false&hideMenu=true&topColor=%23FFFFFF&bottomColor=%23ffffff&cameraParams=false&varsettransfer=" frameborder="0" id="EmbeddedView-Iframe-96615" allowfullscreen></iframe>




# 🌦 Avantages et inconvénients

### Avantages   

Comparé aux machines Precious Plastic précédentes, ce broyeur est très robuste et fiable. Tout a été calculé et dimensionné pour gérer les contraintes élevées requises par le broyage du plastique.

La plupart des systèmes de broyage industriels nécessitent une phase de broyage et une phase de granulation. Grâce aux mailles réglables, cette machine est capable d'effectuer les deux tâches simultanément !

L’assemblage de cette machine est conçu pour être relativement facile. Aucune soudure n'est requise pour la boîte de broyage centrale, et toutes les autres soudures sont simples.

Ce broyeur a également été conçu pour être facile à nettoyer. Il peut être nettoyé en moins de 30 minutes, prêt pour un autre type de plastique, permettant aux membres de la communauté de rapidement produire de petits lots de plastique.

### Inconvénients

La granulation n'est possible qu'avec ce broyeur, mais cette étape est légèrement plus lente qu'avec un machine à granulation industrielle. Étant principalement destinée au broyage, sa polyvalence vient avec un coût.

Par rapport aux machines V3 Precious Plastic, cette machine est plus difficile à construire même si nous avons tâché de garder les choses simples. Vous aurez besoin de machines et de compétences appropriées.

Cette machine nécessite également un équipement spécifique et un moteur puissant, ce qui la rend un peu plus difficile à construire à partir de pièces de récupération. Assurez-vous de lire attentivement la liste des composants (BOM) pour acheter vos pièces car la fiabilité de votre machine en dépend.

# ♻️ Entrée et sortie

La machine peut déchiqueter tout plastique de n'importe quelle forme qui s'insère entre les lames. Nous l'avons testé avec du HDPE, PP et PS. Il n'y a pas de limite d'épaisseur que le broyeur ne puisse pas gérer. Si le morceau de plastique est trop épais, il ne sera pas broyé car les lames ont été conçues de sorte à ce que le plastique ne puisse pas bloquer la machine (dépendamment de la puissance du moteur). Le PP, HDPE et PS de 0,7 mm à 12 mm ont été testés avec succès. Les films sont trop fins et glissent entre les lames. Le caoutchouc, les pneus et les plastiques thermodurcissables n'ont pas été testés.

![Shredder Pro](assets/build/shredder_output.jpg)
### Définition de nos tailles de flocons
Le plastique broyé peut être utilisé avec trois machines Precious Plastic différentes: l’extrudeuse, la machine à injection et la presse à plaques. Nous avons donc défini trois tailles de plastique broyé qui fonctionnent avec chaque machine.

Nom: | Large | Moyenne | Petite |
--- | ---| ---| ---|
Visuel: | <img style="margin-left: 0;" src="../../assets/build/shredder_output_02.jpg" /> | <img style="margin-left: 0;" src="../../assets/build/shredder_output_03.jpg"  />| <img style="margin-left: 0;" src="../../assets/build/shredder_output_04.jpg" />
Taille: | 0-30 MM| 0-10 MM | 0-7 MM|
Fonctionne avec: | Presse à Plaques| Presse à Plaques <br> Presse d'injection <br> Four à Compression | Presse à Plaques <br> Presse d'injection <br> Four à Compression <br> Extrudeuse|


Pour obtenir ces tailles, il convient d’utiliser le bon maillage. Installer un tamis de 7 mm sur le broyeur pour obtenir de petits flocons ou un tamis de 10 mm pour obtenir une taille moyenne. La plus grande taille de flocons peut être obtenue sans utiliser de tamis. Selon la taille des morceaux en entrée, vous devrez peut-être les broyer plusieurs fois pour obtenir la taille souhaitée.


### Sortie et vitesse
| Material | Description | Times shredded | Output (Kg/h) | Sieve | Size |
|---|---|---|---|---|---|
| HDPE | Shampoo bottles | 1st time  | 36,7 | No  | Too big  |
|   |   |  2nd time | 375  | No  |  Large |
|   |   |  **Total** |  **30.9** |   | **Large**  |
| PS | Transparent CD cases | 1st time  | 9.8 | No  | Medium  |
|   |   |  2nd time | 86.5  | 7 mm sieve  |  Small |
|   |   |  **Total** |  **8.8** |   | **Small**  |
| PP | Bottle caps | 1st time  | 76.7 | No  | Large  |
|   |   |  2nd time | 92  | 10 mm sieve  |  Medium |
|   |   |  **Total** |  **41.8** |   | **Medium**  |


![Shredder Pro](assets/build/shredderpro-output.jpg)


# 🙌 Liens utiles 
* [ Acheter ou vendre des pièces et machines sur notre bazar](https://bazar.preciousplastic.com) (en anglais)
* [ Trouver un atelier d'usinage local sur notre carte](https://community.preciousplastic.com/map) (en anglais)
* [ Visitez nos How-tos pour des mises à niveau et des hacks](https://community.preciousplastic.com/how-to) (en anglais)
* [⭐ Mettre en place un atelier de broyage complet](spaces/shredder.md) 

**Si vous avez besoin d'aide, avez des questions ou cherchez quelqu'un à qui parler dans votre atelier empreint de solitude... Allez sur le canal [#build](https://discordapp.com/invite/XQDmQVT) sur Discord. Nous y discutons de la technique entourant la construction des machines.**
