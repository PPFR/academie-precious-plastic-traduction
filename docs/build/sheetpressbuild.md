---
id: sheetpressbuild
title: Construire la Presse à Plaques
sidebar_label: - la Construire
---

<div class="videocontainer">
  <iframe width="800" height="400" src="https://www.youtube.com/embed/j3OctDe3xVk " frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
</div>

<style>
:root {
  --highlight: #f29094;
  --hover: #f29094;
}
</style>

<div class="videoChapters">
<div class="videoChaptersMain">

# Construire la Presse à Plaques

Salutations! Il semblerait que vous souhaitiez construire une Presse à Plaques n’est-ce pas? C’est génial! C'est une machine très intéressante qui peut recycler une grande quantité de plastique par jour. Et à chaque fois que vous ouvrez le couvercle, c'est une surprise de découvrir la plaque fraîchement créée. Mais pas si vite. Concentrons-nous. Il s’agirait d'abord de la construire. Vous pouvez regarder cette vidéo de 20 minutes pour voir comment la fabriquer, ou défiler ci-dessous pour en savoir plus sur les pièces et machines spécifiques nécessaires à sa construction.

> Conseil d’expert: Avant de commencer, assurez-vous de lire toute la documentation. C’est un gros projet donc soyez conscients de ce dans quoi vous vous embarquez.


</div>
<div class="videoChaptersSidebar">

### Video Chapters

- 00:06 Introduction
- 01:07 Pressing Plates
- 02:46 Frame
- 04:43 Heating Elements
- 09:32 Pressing Mechanism
- 12:46 Extraction Hood
- 13:54 Electronics
- 15:35 Assembly
- 16:08 How to Use

</div>
</div>


# 🛠 Required machinery & skills
la Presse à Plaques  | Machines nécessaires | Compétences requises
--- | ---| ---
<img style="margin-left: 0;" src="../../assets/build/thumb-sheetpress.jpg" width="100"/>  | - Perceuse à colonne <br> - Poste à souder <br>  - Engin de levage (ou quelques amis costauds) | - Soudure (expert) <br> - Usinage (intermédiaire) <br> - Assemblage (intermédiaire)<br> - Electronique (expert)



# 🔩 Matériaux et pièces

Vous aurez besoin de fabriquer certaines pièces et d’en acheter d’autres. Vous trouverez la BOM complète dans le kit en téléchargement. C’est une liste pour la totalité du système de la Presse à Plaques. Cela inclut évidemment la Presse à Plaques, mais aussi la Presse à refroidissement et la table de préparation. Ci-dessous vous trouverez les pièces du commerce que vous aurez à obtenir, ainsi que davantage de précision sur les endroits où les trouver.

### Options de vérins & explication

Le type de vérin utilisé par les deux presses est un cric hydraulique à longue course muni d’un raccord à œil en bas (rond, pas une plaque) lui permettant d’être monté à l’aide d’un boulon. Pour la Presse à Plaques, il est recommandé d'utiliser une pression minimale de 8 tonnes et 3 tonnes pour la Presse de Refroidissement. Nos modèles CAO et dessins techniques sont conçus pour s'adapter à un vérin dont le diamètre est de 28 mm, qui est le plus courant. Notez que la pointe du coulisseau est plus petite que le reste, il est typique d'indiquer le diamètre de la pointe.

### Diamètre du tube du coulisseau et du mécanisme à ressort

Les diamètres du tube pour le coulisseau et le mécanisme à ressort dépendent du diamètre du vérin du cric. La section intérieure du mécanisme à ressort et donc le rail pour les plaques de pression doivent avoir le même diamètre que le vérin. Comme la section extérieure du mécanisme à ressort et le rail doivent fonctionner en toute fluidité, leur dimension intérieure dépend également de la dimension du vérin du cric. Idéalement, la dimension extérieure doit être aussi épaisse que possible pour des raisons de solidité, à condition que votre ressort s'adapte en conséquence. Ces dimensions sont très similaires à celles du tube et du piston d'injection. Si vous avez des restes de la construction de l'injection, voyez s'ils s'adaptent à votre cric, ou essayez de trouver un cric qui s'y adapte. Vous pouvez également les modifier sur un tour pour les adapter au cric.

### Choix du ressort

Nous avons utilisé un ressort de compression mais il est également possible d'utiliser un ressort de moto de course ou tout autre ressort dont les dimensions sont proches de celles du ressort que nous avons utilisé et que vous trouverez dans les dessins techniques. Nous avons choisi le ressort le plus ferme possible, le plus ferme étant le mieux, tant qu'il est possible de le comprimer avec le cric que vous utilisez.

### Choix de l’élément chauffant & explication
Nous avons choisi des cartouches de 300W comme source de chauffe. Les cartouches chauffantes permettent de transférer la chaleur par conduction, par opposition au rayonnement, ce qui est beaucoup plus efficace sur le plan énergétique que la seconde solution. L'utilisation de nombreuses petites sources de chaleur réparties sur les plaques chauffantes permet de distribuer la chaleur de manière uniforme, par opposition à un nombre réduit de plus puissantes sources de chaleur. Malgré leur petite taille, les cartouches de 300 W que nous avons utilisées sont plus que suffisantes pour efficacement fondre les types courants de plastique que nous traitons.

Si vous souhaitez modifier la taille de la Presse à Plaques, nous avons constaté qu'un élément chauffant de 300W suffit pour une plaque d'aluminium de 240x240mm, de 10mm d'épaisseur. Il suffit d'ajuster le nombre d'éléments chauffants en conséquence. Notez également que nous avons décalé les éléments chauffants sur les plaques chauffantes supérieure et inférieure pour obtenir une chauffe plus uniforme.

###  Choix de la plaque d'aluminium & explication

Nous avons utilisé de l'aluminium usiné de précision plutôt que de l'aluminium laminé, car il présente plusieurs avantages. Il est moins susceptible de se déformer sous l'effet de la chaleur, est plus solide et donc moins susceptible de se déformer sous l'effet de la pression, et sa surface a une tolérance plus faible, ce qui le rend plus plat.

### Choix de l’acier pour les moules

Il est recommandé d'utiliser une plaque d'acier inoxydable ou d'acier galvanisé de 2 mm pour les moules. Les plaques plus fines risquent d'être endommagées par le pressage. Ces plaques sont également résistantes à la corrosion et ont souvent une finition de surface polie, ce qui est très avantageux pour notre cas d’usage. Les plaques d'acier doux fonctionnent également, mais leur durée de vie est plus courte en raison de la corrosion et de la tendance accrue du plastique à adhérer à une surface non polie. Pour garantir une durée de vie maximale de vos moules, gardez les points suivants à l'esprit ;

- Ne surchargez pas le cric en forçant (appuyez jusqu'à ce que le ressort soit complètement comprimé).
- Assurez-vous d'utiliser un matériau de moule approprié
- Veillez à ce que la répartition initiale du plastique broyé soit régulière (pas d'empilement au centre).

###  Découpe laser ou non?

Dans la mesure du possible, nous recommandons que vous découpiez au laser l’ensemble des pièces à partir de plaques d’acier, en particulier la structure de support soutenant les plaques de pressage. Les avantages de la découpe laser étant un haut degré de précision, et une réduction drastique du temps de construction. Si vous ne pouvez pas découper vos pièces au laser, vous pouvez découper la plupart manuellement à partir de bandes d’acier de 100x6mm, certaines nécessitant une plaque plus large comme le boîtier électrique et la structure de support inférieure.

### Choix de l’acier pour la découpe laser

Pour les pièces de 6mm, nous avons choisi de l’acier de construction laminé à chaud (HRS 75F70) pour sa grande résistance à la traction. Ce type d’acier est laminé ce qui le rend moins susceptible de se déformer lors de la soudure à la construction, ou pendant la chauffe du plastique en utilisation.

Pour les pièces de 2mm, nous avons utilisé de l’acier profilé à froid (CRS CR4), une grande résistance n’étant pas requise.

### Démoulage

L’huile de silicone est l’agent de démoulage le plus performant que nous ayons trouvé. Il existe de nombreuses alternatives, si vous trouvez une solution plus performante ou plus accessible, n’hésitez pas à la partager sur notre communauté en ligne!

### Choix du matériau d’isolation

Pour l’isolant nous avons choisi de la laine minérale. Cette matière est largement accessible, en plus d’être douce, lui permettant d’être facilement coupée et introduite dans des cavités aux formes irrégulières. Elle peut être substituée par d’autres types d’isolation légère, tant que la résistance thermique est suffisante. La laine de verre est l’alternative la plus proche, qui est couramment faite à partir de matériaux recyclés mais est plus chère, moins accessible, et plus susceptible de moisir, réduisant sa durée de vie.

# ⚡ Boîtier électrique

![Sheetpress electronics](assets/build/sheetpress-electronics.jpg)

### Explication du rôle des composants électriques

Le noyau du système repose sur la combinaison habituelle PID-SSR-Thermocouple-Dissipateur thermique, à savoir la même que l’on retrouve dans nos autres machines.

La différence principale étant l’addition d’un relais électromécanique entre le circuit d’alimentation principal et les éléments de chauffe, déclenchés par les SSRs. Ainsi, le courant utilisé par les éléments chauffants ne passe plus par les SSRs, car lors du prototypage, nous avons découvert qu’ils brûlaient bien avant d’atteindre la tension nominale.

Nous en avons déduit que la combinaison du courant et de la chaleur remontant le long des câbles est à l’origine de ce phénomène. Le relais électromécanique permet d’isoler physiquement les deux circuits, en complément d’empêcher la chaleur de s’accumuler dans les relais de par sa plus grande taille.

Il y a deux contrôleurs PID, un pour la plaque supérieure, et un autre pour la plaque inférieure. La longueur des thermocouples k-type est de 3M pour atteindre la bonne position sur les plaques de chauffe.

Entre le circuit d’alimentation principal et les PIDs il y a un interrupteur circulaire pour les allumer ou les éteindre, ainsi qu’un bouton d’arrêt d’urgence.

Les éléments chauffants sont distribués équitablement entre les trois phases. Chaque plaque comporte une phase entière, et une moitié de phase partagée entre les deux plaques. Par exemple, sur la plaque inférieure il y a ⅔ des éléments sur la phase 1 et ⅓ des éléments sur la phase 2, ce qui laisse sur la plaque supérieure ⅓ des éléments sur la phase 2, et ⅔ sur la phase 3. L’alimentation venant du boîtier de contrôle entre dans les plaques de pressage jusqu’à un terminal en céramique, capable de supporter de hautes températures.

Les éléments chauffants sont disposés dans une matrice de structure de support à 45°, placée en quinconce en bas comme en haut, pour que chaque élément chauffant se retrouve au milieu par rapport à ceux de la plaque opposée.

Ils sont installés au sein d’un bloc d’aluminium qui est accroché en dessous de la plaque d'aluminium pour faciliter la conduction thermique.

Pour le câblage à l'intérieur des plaques de pressage, des câbles avec isolation en céramique ont été utilisés.

Tant pour la jonction des câbles que pour leur fixation aux bornes, nous avons utilisé des gaines thermorétractables à base de PTFE, capables de résister à de hautes températures.

Pour toute soudure, il est conseillé d’utiliser de l’étain avec une température de fusion supérieure à 300°C.

Il est indispensable de connecter chaque composant métallique individuel à la terre, à savoir les deux plaques de pressage, les deux plaques de chauffe, le boîtier électrique ainsi que le panneau avant de cette dernière.

Un presse-étoupe est utilisé dès qu’un câble passe de l’intérieur à l’extérieur du corps de la machine, à la fois pour empêcher toute humidité de rentrer et protéger les câbles de tout dommage physique. Ils doivent être placés face vers le bas sur le boîtier pour éviter que du liquide ne s’accumule dans le presse-étoupe.

Lors du câblage du boîtier électrique, chaque fil vissé à une borne est serti pour assurer son maintien.


# 👌 Conseils et astuces lors de la fabrication

### Construction en parallèle

Si vous voulez construire la Presse à Plaques et la Presse de Refroidissement, le procédé est quasiment identique donc il vous sera plus simple de les construire en parallèle.

### Guides de perçage

L’objectif des guides de perçage est double: assurer le positionnement et l’alignement des deux trous utilisés pour le montage des blocs d’aluminium à la plaque, et garder la mèche de perçage droite lors de l’opération.

### Séquencement du soudage du cadre

1. Commencer par souder les deux cadres externes, puis le cadre inférieur. (Lors de l’assemblage de la plaque de fixation du cric avec le cadre inférieur, souder d’abord la plaque du milieu car elle ne sera plus accessible une fois les plaques externes en place. Utiliser le boulon pour l’alignement des trois plaques.)
2. Retourner l’ensemble de la structure pour boulonner les cadres externes avec la plaque de pressage supérieur. (Une fois soudée, retourner à nouveau la structure)
3. Placer des chutes de poutres en dessous du cadre inférieur pour le positionner entre les deux cadres externes. Il doit se trouver à 100mm du sol pour permettre à un transpalette de passer en dessous.

###Soudure et assemblage des plaques de pressage

1. À l’aide d’une soudure par points, assembler le cadre carré qui entoure la structure de support. Il est très important qu’avant de souder les pièces elles soient aux bonnes dimensions, que les trous aient été percés, et que les presse-étoupes soient installés.

2. Séparer les pièces pour la structure de support supérieure de celles pour la structure inférieure. Les premières disposent d’une petite encoche, comme vous pouvez le voir dans les fichiers de découpe.

3. Pour chaque partie:
    ◦ Emboîter les pièces ensemble.
    ◦ Les surélever à l’aide de blocs pour pouvoir les maintenir par dessous à l’aide d’un serre-joint.
    ◦ Maintenir chaque intersection au moment de la souder par points, afin de garantir une surface plate. Il est préférable de souder des points aux opposés de la structure de support pour éviter toute déformation provenant de tensions inégales. Souder par points toutes les intersections avant de les souder totalement.

4. Pour la partie inférieure, souder les attaches du support de fixation du cric en place.

5. Positionner les structures de support à l’intérieur des cadres carrés, en faisant attention que la plaque d’aluminium repose 3mm au-dessus de la surface du cadre.

6. Souder par points plusieurs endroits où la structure de support rencontre le cadre, puis les souder totalement, avant de souder les angles du cadre.

> Conseil: L’aspect le plus important de la structure de support est de s’assurer que les deux ensembles de pièces soient parfaitement plats. Heureusement, les pièces sont précisément découpées à la laser assurant une profondeur constante des rainures, il suffit donc de les maintenir ensemble avec un serre-joint pour que les structures soient le plus plat possible.

Si vous construisez à la fois la Presse à Plaques et de Refroidissement, prenez soin de souder les supports pour le refroidissement avant d’installer la plaque d’aluminium à l’intérieur des plaques de pressage de la Presse à Plaques.

# Options complémentaires

### Ventilation intégrée

Il est possible d’utiliser la machine sans ventilation, mais pour des raisons de sécurité pour vous et les personnes autour, nous recommandons de n’allumer la machine que lorsque la hotte d’extraction est attachée, et le système de ventilation activé. En plus d’utiliser la hotte, il est recommandé d’aérer la pièce dans laquelle se trouve la presse. L’environnement idéal pourrait être un local industriel pour faire du pistolet à peinture.

### Presse à Plaques uniquement, ou système complet

Vous pouvez fabriquer des plaques avec la Presse à Plaques toute seule, mais le procédé est très lent. Vous devriez attendre que la Presse chauffe, puis refroidisse pour chaque plaque créée, impliquant une production d’une à deux plaques par jour. Pour certaines personnes (bricoleurs par exemple) c’est un résultat acceptable, mais pour beaucoup d’autres une production accrue est désirable.

Afin d’améliorer la facilité d’utilisation et l’efficacité de la Presse à Plaques, nous avons conçu trois outils pour la transformer en système complet de production. En ajoutant une presse à refroidissement, une table de préparation et un mécanisme de tirage, vous pourrez préparer, chauffer et refroidir des plaques simultanément. Ces améliorations permettent de transporter les plaques en toute sécurité et facilité entre les différentes étapes, et augmentent drastiquement la cadence de production. Donc si vous cherchez un rythme de production efficace, construisez le système entier. Si vous n’avez pas beaucoup d’espace ou souhaitez simplement faire quelques plaques, contentez-vous de la Presse à Plaques.

![Sheetpress run](assets/build/sheetpress-system.jpg)

**Si vous avez besoin d'aide, avez des questions ou cherchez quelqu'un à qui parler dans votre atelier empreint de solitude... Allez sur le canal  [#build](https://discordapp.com/invite/XQDmQVT) sur Discord. Nous y discutons de la technique entourant la construction des machines.**
