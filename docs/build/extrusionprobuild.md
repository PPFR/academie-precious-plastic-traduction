---
id: extrusionprobuild
title: Construire l'Extrudeuse Pro
sidebar_label: - la Construire
---
<div class="videocontainer">
  <iframe width="800" height="400" src="https://www.youtube.com/embed/3-JFVo6BDA4" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
</div>

<style>
:root {
  --highlight: #f29094;
  --hover: #f29094;
}
</style>

<div class="videoChapters">
<div class="videoChaptersMain">

# Construction de l'extrudeuse Pro

Bienvenue à l’Extrudeuse Pro ! C’est une version plus solide, plus grande et plus robuste que l’extrudeuse V3 alors si vous cherchez une production sérieuse et avez des compétences avancées en fraisage et tournage, continuez la lecture ! Si vous cherchez à débuter, ou préférez une machine plus petite et plus facile à construire, on vous conseille de commencer avec  l’extrudeuse originelle (qui est quand même très cool !)

> Conseil d'expert: Construire l’extrudeuse requiert une très grande précision et un oeil de lynx pour les tolérances, donc si vous pensez ne pas être à la hauteur essayez de trouver les  pièces complexes sur le bazar. Elles sont de petites tailles et faciles à expédier.

</div>
<div class="videoChaptersSidebar">

### Video Chapters

- 00:07 Introduction
- 01:30 Bearing body and shaft
- 08:49 Barrel inlet
- 12:17 Barrel
- 15:13 Motor adapter
- 16:35 Hopper
- 17:39 Electronics
- 19:45 Frame
- 20:19 Final assembly
- 24:11 Maintenance and how to run

</div>
</div>


# 🛠 Machines et compétences requises
Extrudeuse Pro  | Outils nécessaires | Compétences requises
--- | ---| ---
<img style="margin-left: 0;" src="../../assets/build/thumb-extrusion-pro.jpg" width="150"/>  | - Tour <br> - Perceuse à colonne <br>- Fraiseuse <br> - Poste à souder (TIG or MIG/MAG recommandé) <br> - Clé Torq| - Soudure (avancé) <br> - Usinage (avancé) <br> - Assemblage (intermédiaire)<br> - Electronique (intermédiaire)


# 🔩 Matériaux et pièces

### Choix du moteur et explication

Nous vous recommandons d’utiliser un moteur dont le couple est compris entre 80 et 120 Nm. Nous vous recommandons également d’utiliser un moteur d’environ 3 kW qui correspondrait à une vitesse de rotation de 220 à 320 tours/minute. On vous déconseille d’utiliser un réducteur à vis sans fin à cause de sa faible efficacité.

Avant d’acheter un moteur, faites attention à la tension ainsi qu'à la fréquence électrique standard de votre pays. Ces chiffres varient à travers le monde et peuvent rendre l’importation d’un moteur compliquée:

![Voltage Map](assets/ex1_map.jpg)

Le couple de votre moteur asynchrone (triangle ou étoile) dépendra de la tension secteur de votre installation et la tension nominale du moteur. Si votre configuration est mauvaise votre moteur pourrait tourner mais avec un faible couple, ce qui pourrait enclencher votre disjoncteur ou brûler votre moteur.


<img style="margin-left: 0; margin-top: 0px margin-bottom: 0px;" src="../../assets/ex2.jpg" width="500"/>

La plupart des variateurs de fréquence (VFD) requièrent d’être connecté au moteur en configuration triangle, mais lisez bien le manuel d’utilisation de votre VFD pour plus d’informations.

![Star Delta Connectiion](assets/ex3.jpg)

### Options de couple nominal & explication

La méthode de “Serrage du cône de fixation d'arbre” a été choisie pour maintenir la vis d’extrusion car elle permet de supporter de fort couples sur de courtes durées. La vis a été conçue de sorte à permettre une maintenance facile des composants de l’extrudeuse. Il est facile d’installer et retirer le couplage du réducteur. Soyez attentif à bien utiliser un système de serrage capable de supporter un couple d’au moins 250 Nm.

### Forme de la trémie

Les trémies de forme carrée fonctionnent bien, sauf lorsqu’il y a des irrégularités dans les formes et tailles du plastique broyé. Le choix relatif à la forme de la partie basse de la trémie est fonction de nombreux facteurs. Les trémies de forme pyramidale sont habituellement utilisées avec des produits fluides qui sont stables dans le temps et pour lesquelles la taille des morceaux est homogène. Les trémies de forme conique sont assez simples à fabriquer et ont une forme bien adaptée pour faire face aux pressions internes.

# ⚡ Boîtier Électronique

### Explication du rôle des composants électriques

![PID](assets/ex4_PID.jpg)

### Contrôleurs PID:

Les trois PID contrôlent la température des bagues chauffantes. Chacun est lié à un thermocouple qui mesure la température à un point spécifique du processus. Ces sondes de température sont très robustes, auto-alimentées et peu coûteuses. Il faudra faire attention au fait qu’il y a différents types de sonde de température. Les modèles de thermocouple les plus communs sont de type J, T et K, accessibles pré-assemblés à l'achat.

### Assurez-vous que vos sondes thermocouples sont de même type que vos PID (J, T ou K).

<img style="margin-left: 0; margin-top: 0px margin-bottom: 0px;" src="../../assets/ex5.jpg" width="400"/>

La majorité des PID sont de type K par défaut, mais il est parfois possible d’utiliser d’autres types en changeant les paramètres initiaux via le mode d’initialisation :  

- PID REX C100 Page 7 Chapitre 7.1 :  https://www.mpja.com/download/rex-c100.pdf

### Variateur de fréquence :

<img style="margin-left: 0; margin-top: 0px margin-bottom: 0px;" src="../../assets/ex6.jpg" width="250"/>

Un VFD (variateur de fréquence) est un dispositif électronique précis qui permet de contrôler la vitesse de rotation de moteurs à courant alternatif sans altérer son couple nominal, sa consommation électrique, son impédance, etc.   

# 👌 Conseils et astuces lors de la fabrication

### Moules

Au niveau de la buse de l’extrudeuse, vous pouvez utiliser différents types de connecteur pour attacher un moule. Serrure à came et queue d’aronde sont adaptés à des besoins de libération rapide du moule. Une buse filetée est très bien pour des produits nécessitant une grande pression en sortie.

<img style="margin-left: 0; margin-top: 0px margin-bottom: 0px;" src="../../assets/ex7.jpg" width="500"/>

Vous obtiendrez différentes textures de surface selon la température de votre moule. Un moule préchauffé vous donnera une surface bien lisse alors qu’un moule froid vous donnera des produits avec une surface ridée.

<img style="margin-left: 0; margin-top: 0px margin-bottom: 0px;" src="../../assets/ex8.jpg" width="500"/>

Lorsque vous utilisez l’extrudeuse pour faire des profilés, il est facile de voir lorsque le moule est rempli, mais pour d’autres objets ça peut vite devenir un casse-tête. Voici quelques astuces que l’on a trouvé :
  - Mettre un minuteur si vous connaissez le débit de matière et le volume du moule
  - Ajouter de minuscules points de sortie (1mm)
  - Pour la brique lego,  nous avons utilisé des étau sur la connexion en queue d’aronde

Quand la pression est assez importante, l’étau commence à s’ouvrir, indiquant que le moule est rempli

Si vous voulez produire des objets plus grands, une technique qui fonctionne bien consiste à créer une contre-pression dans le moule pour que le plastique remplisse toute la section. Utiliser un piston avec un joint silicone haute température est la solution la plus économique et la plus simple que nous ayons trouvé. Si vous voulez encore augmenter la pression, vous pouvez ajouter du poids au piston.

<img style="margin-left: 0; margin-top: 0px margin-bottom: 0px;" src="../../assets/ex9.jpg" width="500"/>

Si vous voulez faire des profilés avec un moule complexe ou avec un plastique dont la rétractation est faible (comme le PS), un moule en deux parties peut être une bonne solution pour faciliter le démoulage. La photo ci-dessous montre une barre plate vissée qui va créer une rainure dans le profilé, et minimise le besoin d’usinage supplémentaire. Ce type de conception permet de réduire le temps de post-production et permet également de ne pas créer de microplastiques.

<img style="margin-left: 0; margin-top: 0px margin-bottom: 0px;" src="../../assets/ex10.jpg" width="500"/>

# Options complémentaires

### Support du tube

Si vous utilisez régulièrement l’extrudeuse avec moules lourds fixés dessus, vous aurez probablement besoin d’un support pour le tube afin d’éviter de tordre l’extrudeuse à cause des stress répétés.

Vous trouverez les dessins techniques associés dans le kit en téléchargement.

<img style="margin-left: 0; margin-top: 0px margin-bottom: 0px;" src="../../assets/ex11.jpg" width="500"/>

### Option de protection thermique

Nous recommandons de couvrir le tube de l’extrudeuse afin d’éviter de vous brûler et minimiser les pertes de chaleur. Afin de construire cette protection, coupez et pliez simplement une plaque d’acier (un objet cylindrique peut être utilisé pour former le pli)

Vous trouverez les dessins techniques dans le kit à télécharger.

Il ne reste plus qu’à insérer un matériau isolant entre le tube et la plaque d’acier. Différents types d’isolation peuvent être utilisés:
 - Laine céramique (meilleur option)
 - Laine minérale
 - Laine de roche
 - Laine de verre

<img style="margin-left: 0; margin-top: 0px margin-bottom: 0px;" src="../../assets/ex12.jpg" width="500"/>

**Si vous avez besoin d'aide, avez des questions ou cherchez quelqu'un à qui parler dans votre atelier empreint de solitude... Allez sur le canal [#build](https://discordapp.com/invite/XQDmQVT)sur Discord. Nous y discutons de la technique entourant la construction des machines.**
