---
id: sheetpress
title: Démarrer un atelier avec une Presse à Plaques
sidebar_label: Plaques
---
<div class="videocontainer">
  <iframe width="800" height="400" src="https://www.youtube.com/embed/TNG2f_hKc_A" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
</div>

<style>
:root {
  --highlight: #37b4a3;
  --hover: #37b4a3;
}
</style>

# Espace SheetPress ( presse à plaques)

<div class="videoChapters">
<div class="videoChaptersMain">

###  Cet Espace Precious Plastic transforme les paillettes de  plastiques en belles plaques de 1m x 1m.

Vous souhaitez configurer un espace de travail Sheetpress. Très excitant, non ?!!
Vos plaques peuvent être utilisées pour créer toutes sortes d'objets utiles et merveilleux - ou simplement avoir l'air arty accrochées au mur 🙂.

La mise en place d'un espace de travail Sheetpress peut se décomposer en trois étapes principales : 

1. <b>Rechercher:</b> à quoi penser lors du choix de votre espace
2. <b>Planifier:</b> comment organiser son espace efficacement pour y caler une ou des grosses machines.
3. <b>Aménager:</b> transformer une pièce vide en un espace de travail Sheetpress

Chacune de ces étapes est expliquée plus en détail ci-dessous. 

</div>
<div class="videoChaptersSidebar">

### Video Chapters

- 00:00 Introduction
- 00:50 Sheepress
- 01:10 Sourcing Plastic
- 01:40 Pressing Time
- 04:20 Products and Logistics

</div>
</div>

## 🔍 Rechercher

La première étape de la mise en place d'un espace de travail consiste à **trouver un local.** Lors de la recherche, il y a quelques considérations à garder à l'esprit. 

1. <b>Taille:</b> Il est important de s'assurer que vous disposez de suffisamment d'espace pour vous déplacer librement avec vos machines et vos outils. L'espace Sheetpress que nous avons conçu est d'environ 5 x 8 mètres minimum - vous pouvez chercher plus grand, mais pas plus petit.

2. <b>Électricité:</b> La machine Sheetpress fonctionne en triphasé et demande beaucoup de puissance, alors assurez-vous de choisir un espace triphasé et les spécifications suivantes 400 V/32 A/1,5 kW.

3. <b>Budget:</b> Tout en vous assurant que votre local répond à vos besoins fonctionnels, il est également important de garder à l'esprit comment il s'intégrera à votre budget (loyer, factures de consommation, assurance)

> Conseil d'expert: parfois, les villes offrent des loyers réduits aux entrepreneurs et associations ayant des missions ciblées (économie circulaire, sensibilisation, etc), il vaut donc la peine de contacter les accélérateurs d’activités locaux et les représentants de votre municipalité pour voir s'il existe des opportunités comme celle-ci dans votre région. 


## ✍️ Planifier

Vous avez trouvé votre local. Vous pouvez maintenant commencer à planifier la manière dont vous voulez organiser les éléments de votre espace de travail en fonction de votre plan de masse. 

Chaque espace est différent, il est donc important de personnaliser votre disposition pour qu'elle fonctionne au mieux pour vous. 

Pour vous aider, nous avons inclus des plans de démarrage, des modèles CAO et un simulateur de plan personnalisé dans le kit de téléchargement, que vous pouvez utiliser pour explorer différentes dispositions.

![Sheetpress Workspace](assets/spaces_sheetpress.jpg)

Une fois que vous avez dessiné votre plan qui semble prometteur, testez-le sur site en traçant au sol  avec de la craie les limites de chaque élément. Modifiez si nécessaire jusqu’à satisfaction :-). 

## 🛠 Aménager

Maintenant que votre plan d'étage est finalisé, il est temps de transformer votre pièce vide en un espace de travail Sheetpress. 

Commencez par placer vos éléments les plus essentiels : Sheetpress, établi, outils de base... Puis aménagez le reste du mobilier en commençant à voir ce qui convient le mieux à votre flux de travail.

> Conseil d’expert : il est préférable de garder les meubles de votre espace de travail sur roulettes afin de pouvoir les déplacer facilement au besoin.

Lorsque cela est possible, recherchez des pièces d'occasion. Si ce dont vous avez besoin n'est pas disponible d'occasion, achetez-le neuf ou construisez-le. Et privilégiez toujours la bonne qualité plutôt que le bon marché. 

Quelques éléments essentiels pour démarrer : 
    • Sheetpress - bien sûr 
    • Récipients de stockage pour les paillettes de plastique classés par types et couleurs. 
    • Moule simple – cadres carrés avec deux tôles d'acier, simples et efficaces. Consultez les tutoriels pour vous inspirer.
    • Outils de base - balance, seau, outil coulissant, huile de silicone, gants résistants à la chaleur, minuterie. 
    • Ventilation – masque filtrant, panier de ventilation, regardez la vidéo de sécurité pour en savoir plus. 
    • Conteneur de chutes. 


Une fois que vous avez acquis les bases, commencez à travailler avec la sheetpress pour expérimenter et quantifier votre flux de travail. Ainsi vous pourrez conserver ou modifier l’espace afin d'être plus efficace.

N’hésitez pas à décorer votre local avec des plantes, des affiches, un bon éclairage pour le rendre agréable à vivre. 

### Votre rôle dans l'univers Precious Plastic
| Votre badge  |  Votre mission |
|----------|----------------------|
| <img src="assets/universe/badge-workspace.png" width="150"/>        |  __Atelier__ <br> Un atelier Precious Plastic  est un endroit où le plastique est transformé à partir de déchets en matériaux ou produits de valeur. Il existe cinq espaces de travail différents : Broyeur, Extrusion, Sheetpress, Injection et Mix.  |

## 👋 Partager

Vous avez construit votre espace. Super ! Nous apprenons tous ensemble, alors assurez-vous de partager tous les hacks ou développements qui vous ont aidé à améliorer votre espace 🙂 

<b>Vous souhaitez partager vos commentaires, discuter de l'espace de travail Injection ou en savoir plus sur la communauté ? 
Rendez-vous sur le  [Discord](https://discordapp.com/invite/p92s237). Ici, nous répondons aux questions et donnons des conseils sur la configuration de votre espace de travail et sur son fonctionnement. </b>
