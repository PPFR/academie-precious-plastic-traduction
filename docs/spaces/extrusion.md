---
id: extrusion
title: Setup an Extrusion Workspace
sidebar_label: Extrusion
---

<div class="videocontainer">
  <iframe width="800" height="400" src="https://www.youtube.com/embed/NuYj4hDjKzU" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
</div>

<style>
:root {
  --highlight: #37b4a3;
  --hover: #37b4a3;
}
</style>

# Espace Extrusion

<div class="videoChapters">
<div class="videoChaptersMain">

###  Cet Espace Precious Plastic transforme les déchets plastiques en les extrudant en divers objets et matières premières comme des poutres ou des briques. 

Donc, vous voulez configurer un espace de travail d'extrusion. C'est génial ! 
L'espace de travail d'extrusion est un excellent choix pour recycler de nombreux déchets plastiques en poutres ou en briques dans un processus continu assez simple et facile à apprendre. 

**L'espace de travail de du broyeur est un élément crucial de l'univers de Precious Plastic. 
La mise en place d'un espace de travail peut se décomposer en trois étapes principales :**


1. <b>Rechercher:</b>  à quoi penser lors du choix de votre espace 
2. <b>Planifier:</b> comment planifier son espace efficacement pour travailler avec des moules (parfois) longs
3. <b>Aménager:</b> transformer une pièce vide en un espace de travail d'extrusion

Chacune de ces étapes est expliquée plus en détail ci-dessous. 

</div>
<div class="videoChaptersSidebar">

### Video Chapters

- 00:00 Introduction
- 00:49 Extrusion Machine
- 01:02 Sourcing Plastic
- 01:50 Extruding Time
- 04:20 Logistics

</div>
</div>

## 🔍 Rechercher

La première étape de la mise en place d'un espace de travail consiste à **trouver un local.** Lors de la recherche, il y a quelques considérations à garder à l'esprit. 

1. <b>Taille:</b> Il est important de s'assurer que vous disposez de suffisamment d'espace pour vous déplacer librement avec vos machines et vos outils. L'espace d’extrusion que nous avons conçu est d'environ 5 x 6 mètres minimum - vous pouvez chercher plus grand, mais pas plus petit.

2. <b>Électricité:</b> L’extrudeuse peut fonctionner en monophasé ou en triphasé selon la version que vous envisagez de construire (v2 ou v4), alors assurez-vous de vérifier les spécifications du moteur que vous prévoyez d'utiliser et les types de raccordements électriques lors du choix de votre local. 

3. <b>Budget:</b> Tout en vous assurant que votre local répond à vos besoins fonctionnels, il est également important de garder à l'esprit comment il s'intégrera à votre budget (loyer, factures de consommation, assurance)

4. <b>Ventilation:</b> Le recyclage du plastique peut parfois produire des émanations. Pour cette raison, il est indispensable de rechercher un espace bien ventilé et ouvert. Alternativement, vous pouvez toujours créer votre propre système de ventilation. 

> Conseil d'expert: parfois, les villes offrent des loyers réduits aux entrepreneurs et associations ayant des missions ciblées (économie circulaire, sensibilisation, etc), il vaut donc la peine de contacter les accélérateurs d’activités locaux et les représentants de votre municipalité pour voir s'il existe des opportunités comme celle-ci dans votre région. 


## ✍️  Planifier

Vous avez trouvé votre local. Vous pouvez maintenant commencer à planifier la manière dont vous voulez organiser les éléments de votre espace de travail en fonction de votre plan de masse. 

Chaque espace est différent, il est donc important de personnaliser votre disposition pour qu'elle fonctionne au mieux pour vous. 

Pour vous aider, nous avons inclus des plans de démarrage, des modèles CAO et un simulateur de plan personnalisé dans le kit de téléchargement, que vous pouvez utiliser pour explorer différentes dispositions.

![Extrusion Workspace](assets/spaces_extruder.jpg)

Une fois que vous avez dessiné votre plan d'étage qui semble prometteur, testez-le sur site en traçant au sol  avec de la craie les limites de chaque élément. Modifiez si nécessaire jusqu’à satisfaction :-). 


## 🛠 Aménager

Maintenant que votre plan d'étage est finalisé, il est temps de transformer votre pièce vide en un **espace de travail d’extrusion**. 

ommencez par placer vos éléments les plus essentiels : extrudeuse, établi, outils de base 

Puis aménager le reste du mobilier à partir de là en commençant à voir ce qui convient le mieux à votre flux de travail. 

> Conseil d’expert: il est préférable de garder les meubles de votre espace de travail sur roulettes afin de pouvoir les déplacer facilement au besoin.


Lorsque c'est possible, recherchez des pièces d'occasion. Si ce dont vous avez besoin n'est pas disponible d'occasion, achetez-le neuf ou construisez-le. Et privilégiez toujours la bonne qualité plutôt que le bon marché. 

Quelques éléments essentiels pour démarrer : 
  • Extrudeuse - bien sûr 

  • Récipients de stockage pour les paillettes de plastique classés par types et couleurs. 

  • Moule simple – peut être une poutre ou une brique. Il est préférable de commencer par quelque chose de simple pour vous familiariser avec le processus. Ensuite consultez les tutoriels pour vous inspirer.

  • Outils de base - clés, pinces multiprises, tournevis, lunettes de sécurité, gants résistants à la chaleur et pelle. 

  • Ventilation – masque filtrant, panier de ventilation, regardez la vidéo de sécurité pour en savoir plus. 

  • Table de travail. 
  
  • Conteneur de chutes. 

Une fois que vous avez acquis les bases, commencez à travailler avec l'extrudeuse pour expérimenter et quantifier votre flux de travail. Ainsi vous pourrez conserver ou modifier l’espace afin d'être plus efficace. 

N’hésitez pas à décorer votre local avec des plantes, des affiches, un bon éclairage pour le rendre agréable à vivre, pour vous et pour tous ceux qui passent le découvrir. 



### Votre rôle dans l'univers Precious Plastic
| Ton badge  | Ta mission |
|----------|----------------------|
| <img src="../../assets/universe/badge-workspace.png" width="150"/>        |  _Atelier__ <br> Un atelier Precious Plastic  est un endroit où le plastique est transformé à partir de déchets en matériaux ou produits de valeur. Il existe cinq espaces de travail différents : Broyeur, Extrusion, Sheetpress, Injection et Mix. 


## 👋 Partager

Vous avez construit votre espace. Super ! Nous apprenons tous ensemble, alors assurez-vous de partager tous les hacks ou développements qui vous ont aidé à améliorer votre espace 🙂 

<b>Vous souhaitez partager vos commentaires, discuter des aménagements pour l'extrusion ou en savoir plus sur la communauté ? 
Rendez-vous sur le [Discord](https://discordapp.com/invite/p92s237). Ici, nous répondons aux questions et donnons des conseils sur la configuration de votre espace de travail et sur son fonctionnement. </b>
