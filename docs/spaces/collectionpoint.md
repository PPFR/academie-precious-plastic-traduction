---
id: collectionpoint
title: Démarrer un point de collecte
sidebar_label: Point de collecte
---

<div class="videocontainer">
  <iframe width="800" height="400" src="https://www.youtube.com/embed/i2h3DWEJl84" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
</div>

<style>
:root {
  --highlight: #37b4a3;
  --hover: #37b4a3;
}
</style>

# Espace Point de Collecte 
<div class="videoChapters">
<div class="videoChaptersMain">

### Cet espace de travail est destiné à collecter du plastique propre et sans étiquette dans votre quartier afin de le transmettre à l'espace de travail Broyeur afin de le transformer par le recyclage.

Vous souhaitez donc créer un point de collecte. C'est super, et tellement nécessaire ! 
Les points de collecte sont un élément tellement crucial de l'univers Precious Plastic car ils sont les "capteurs" de tous les déchets plastiques qui attendent d'être recyclés. 
La mise en place d'un espace de collecte peut se décomposer en trois étapes principales : 

1. <b>Rechercher:</b> à quoi penser lors du choix de votre espace 
2. <b>Planifier:</b> comment organiser son espace efficacement pour collecter le plastique et engager vos voisins dans cette dynamique.
3. <b>Aménager:</b> transformer une pièce vide en Point de collecte.

Chacune de ces étapes est expliquée plus en détail ci-dessous. 

> Conseil d'expert : nous avons créé ce <a href="https://collect.preciousplastic.com/">site web (en anglais)</a> pour vous aider à communiquer avec les gens de votre quartier à propos du point de collecte de Precious Plastic. 

</div>
<div class="videoChaptersSidebar">

### Video Chapters

- 00:00 Introduction
- 00:50 Workspace Setup
- 01:25 Engagement
- 02:00 Labeling and Recording
- 03:01 Collection Logistics

</div>
</div>

### Prêt?

Avant de commencer, il y a quelques points à considérer. 

1. <b>Équipe:</b> rassemblez votre équipe (c'est plus fun entre amis !) 
2. <b>Espace:</b> le plastique est volumineux. Votre local est-il assez grand ? Nous travaillons avec un espace minimum de 5 x 6 mètres (30m²). 
3. <b>Emplacement:</b>est-il facile à trouver et à rejoindre ? 

![Collection Workspace](assets/spaces_collection.jpg)

# 🛠Construire !

L'objectif de la mise en place et de l'exploitation de cet espace Precious Plastic est de collecter et de trier autant de plastique propre que possible, vous devrez donc être assez organisé et ouvert pour tendre la main aux personnes de votre quartier. Nous vous recommandons d'utiliser de grands sacs ou conteneurs pour stocker le plastique, et il est important de séparer votre plastique en fonction du type (2 PEHD / 4 LDPE / 5 PP / 6 PS)  - alors assurez-vous de marquer les sacs pour garantir que tout va au bon endroit. 

![Stock de Plastiques](assets/PileOfPlastic.jpg)

Si vous êtes en mesure d'obtenir une source constante (mono-matériau) de plastique, par exemple les cuillères en plastique de votre magasin de crème glacée local, vous pouvez les stocker séparément au fur et à mesure que vous les collectez afin de maintenir la cohérence et ainsi fournir des espaces de travail “Broyeur” avec du plastique propre de haute qualité.

Une partie importante d’un point de collecte pour votre communauté locale est la communication ! Il est indispensable de s'engager à la fois sur le terrain et sur internet avec les personnes vivant dans votre quartier, donc proche de votre espace.
S’équiper d’un ordinateur pour utiliser les réseaux sociaux, faire des campagnes de collecte, vous faire connaître pour des partenariats et collaborations est un travail recommandé. Nous avons également créé ce <a href="https://collect.preciousplastic.com/">site web</a>pour vous aider à parler avec les gens de votre quartier du point de collecte Precious Plastic. 

> Conseil d'expert: nous avons réalisé <a href="https://youtu.be/7zm_xVx7TBs">cette vidéo</a> pour vous aider à apprendre à votre communauté locale comment nettoyer son plastique avant qu'il ne vous l'apporte. Cela les aidera à comprendre les différents types de plastique, comment les nettoyer, vous trouver et vous faire gagner beaucoup de temps ! *Si vous installez une station de lavage au point de collecte, ils pourront également le faire là en cas d'oubli. 

La façon dont vous collectez le plastique peut varier considérablement selon l'endroit où vous vivez, car cela dépend de nombreux facteurs culturels, géographiques et économiques différents. Assurez-vous de consulter <a href="https://community.preciousplastic.com/how-to">les tutoriels dédiés à la Collecte</a> (utilisez les filtres en haut) du monde entier et laissez-vous inspirer par les nombreuses façons créatives que les gens utilisent pour collecter le plastique dans leur région. 
Quelle que soit l'approche que vous adoptez, assurez-vous que toutes les personnes qui viennent dans votre espace de travail vivent une expérience agréable et conviviale. Et bien sûr, dans le plus pur style Precious Plastic, dans le kit de téléchargement, vous trouverez des affiches que vous pourrez accrocher. 

Une fois que vous avez collecté suffisamment de plastique, il est temps de le remettre à un espace de travail “Broyeur” à proximité. Assurez-vous de l'étiqueter correctement, afin que l'atelier auquel vous le transmettez sache quel type de plastique il reçoit. 
Avant de le transmettre, assurez-vous de peser le plastique et d'en conserver une trace afin de savoir combien de plastique vous collectez. 

**IMPORTANT:** Une fois par semaine ou par mois (selon la taille de votre opération), vous devez télécharger le total de kg de plastique que vous avez collecté sur <a href="https://upload.preciousplastic.com/"> upload.preciousplastic.com</a> afin que nous puissions suivre le tonnage total collecté par l'ensemble de la communauté Precious Plastic dans le monde 💪  


### Votre rôle dans l'univers Precious Plastic
| Ton badge | Ta mission |
|----------|----------------------|
| <img src="../../assets/universe/badge-collection-point.png" width="150"/> |  __Point de collecte__ <br> Les points de collecte collectent le plastique des voisins, des organisations et des entreprises pour qu'il soit utilisé par les espaces de travail “Broyeur” locaux.     |


### Que pouvez-vous faire d'autre? 

Considérer le plastique comme une source de beaux objets est une énorme motivation pour que les gens apportent leur plastique. C'est agréable d'avoir un présentoir pour montrer les produits précieux qui peuvent être fabriqués à partir du plastique que vous collectez. Et c'est tout ! 

Vos besoins changeront en fonction de votre propre situation, alors assurez-vous de vous adapter à ce qui est pertinent pour vous et votre communauté - et pensez à partager vos expériences afin que nous puissions tous grandir ensemble.

<b>Vous souhaitez partager vos commentaires, discuter de l'espace de travail Point de collecte ou en savoir plus sur la communauté ? 
Rendez-vous sur le <a href="https://discord.gg/JsFBRfHV">Discord</a></b>
