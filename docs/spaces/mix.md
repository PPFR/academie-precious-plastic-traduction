---
id: mix
title: Démarrez un atelier mixte
sidebar_label: Mix
---
<div class="videocontainer">
  <iframe width="800" height="400" src="https://www.youtube.com/embed/RmN0d41399w" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
</div>

<style>
:root {
  --highlight: #37b4a3;
  --hover: #37b4a3;
}
</style>

# Espace Atelier Mix

<div class="videoChapters">
<div class="videoChaptersMain">

###  Cet Espace Precious Plastic est l'endroit idéal pour apprendre et se lancer dans le recyclage du plastique et créer de beaux produits à partir de déchets plastiques.

Ok, vous voulez configurer un espace de travail Mix. Super. Nous allons vous expliquer comment en savoir plus sur le plastique, construire les machines et aménager l'espace pour créer de merveilleux objets à partir de déchets plastiques 🤙.

La mise en place d'un espace de travail Precious Plastic peut se décomposer en trois étapes principales : 

1. <b>Rechercher:</b> à quoi penser lors du choix de votre espace 
2. <b>Planifier:</b> comment organiser son espace efficacement pour l’adapter à toutes les machines.
3. <b>Aménager:</b>  transformer une pièce vide en un espace de travail Mix

Chacune de ces étapes est expliquée plus en détail ci-dessous.

</div>
<div class="videoChaptersSidebar">

### Video Chapters

- 00:00 Introduction
- 01:10 Machines
- 01:22 Sourcing Plastic
- 02:05 Shredding Time
- 03:01 Working With Machines
- 04:17 Logistics

</div>
</div>

## 🔍 Rechercher

La première étape de la mise en place d'un tel espace de travail consiste à **trouver un local.** Lors de la recherche, il y a quelques considérations à garder à l'esprit. 

1. <b>Taille:</b> Il est important de s'assurer que vous disposez de suffisamment d'espace pour vous déplacer librement avec vos machines et vos outils. L'espace optimal que nous avons conçu est d'environ 5 x 8 mètres minimum - vous pouvez chercher plus grand, mais pas plus petit.

2. <b>Électricité:</b> Les machines d'injection et d'extrusion peuvent fonctionner en monophasé, mais le broyeur fonctionne parfois en triphasé alors assurez-vous de vérifier les spécifications du moteur que vous prévoyez d'utiliser et les types de raccordements électriques lors du choix de votre local.

3. <b>Budget:</b> Tout en vous assurant que votre local répond à vos besoins fonctionnels, il est également important de garder à l'esprit comment il s'intégrera à votre budget (loyer, factures de consommation, assurance)


> Conseil d’expert : parfois, les villes offrent des loyers réduits aux entrepreneurs et associations ayant des missions ciblées (économie circulaire, sensibilisation, etc), il vaut donc la peine de contacter les accélérateurs d’activités locaux et les représentants de votre municipalité pour voir s'il existe des opportunités comme celle-ci dans votre région. 

## ✍️ Planifier

Vous avez trouvé votre local. Vous pouvez maintenant commencer à planifier la manière dont vous voulez organiser les éléments de votre espace de travail en fonction de votre plan de masse. 

Chaque espace est différent, il est donc important de personnaliser votre disposition pour qu'elle fonctionne au mieux pour vous. 

Pour vous aider, nous avons inclus des plans de démarrage, des modèles CAO et un simulateur de plan personnalisé dans le kit de téléchargement, que vous pouvez utiliser pour explorer différentes dispositions.


![Mix Workspace](assets/spaces_mix.jpg)

Une fois que vous avez dessiné un plan qui vous semble prometteur, testez-le sur site en traçant au sol  avec de la craie les limites de chaque élément. Modifiez si nécessaire jusqu’à satisfaction :-). 


## 🛠 Aménager

 Maintenant que votre plan d'étage est finalisé, il est temps de transformer votre pièce vide en un espace de travail Mix. 
 
 Commencez par placer vos éléments les plus essentiels : Machines, établi, outils de base...  Puis aménagez le reste du mobilier en commençant à voir ce qui convient le mieux à votre flux de travail.

> Conseil d’expert : il est préférable de garder les meubles de votre espace de travail sur roulettes afin de pouvoir les déplacer facilement au besoin.

Lorsque cela est possible, recherchez des pièces d'occasion. Si ce dont vous avez besoin n'est pas disponible d'occasion, achetez-le neuf ou construisez-le. Et privilégiez toujours la bonne qualité plutôt que le bon marché. 

Quelques éléments essentiels pour démarrer : 
    • Les différentes machines - bien sûr. Vous pouvez toutes les installer ou commencer par une combinaison qui correspond aux objectifs de votre espace de travail.
    • Récipients de stockage pour les déchets plastiques classés par type 
    • Récipients de stockage pour les paillettes de plastique - assurez-vous qu'ils sont solides et qu’ils rentrent sous le broyeur. Ainsi ils pourront être directement stockés après broyage. Gardez les types et les couleurs pures.
    • Moules simples – qu'il s'agisse d'une poutre, d'un bol ou de tout autre élément - cela dépend  des machines que vous utilisez. Mieux vaut commencer par quelque chose de simple pour vous familiariser avec le processus, puis construire à partir de là. Consultez les tutoriels pour vous inspirer. Consultez les tutoriels pour vous inspirer.
    • Outils de base - clés, truelle, pelle, marteau, burin, pince multiprise, tournevis, lunettes de sécurité et gants résistants à la chaleur.
    • Ventilation – masque filtrant, panier de ventilation, regardez la vidéo de sécurité pour en savoir plus. 
    • Table de travail
    • Conteneur de chutes. 


Une fois que vous avez acquis les bases, commencez à travailler avec la sheetpress pour expérimenter et quantifier votre flux de travail. Ainsi vous pourrez conserver ou modifier l’espace afin d'être plus efficace. 

N’hésitez pas à décorer votre local avec des plantes, des affiches, un bon éclairage pour le rendre agréable à vivre, pour vous comme pour ceux qui viendront le visiter. 

## Dans un conteneur maritime

En option, nous avons également créé une série de 4 vidéos où vous pouvez voir comment nous construisons un espace de travail Mix complet à l'intérieur d'un conteneur d'expédition de 40 pieds. Pour vous donner des conseils ou de l'inspiration sur la façon d'aménager votre espace :) 

1. Obtenez un conteneur :  https://youtu.be/IhYHzKHH0CQ
2. Aménagez l’intérieur : https://youtu.be/KQgzhtyaiHs
3. Peignez-le : https://youtu.be/noQWukX7lTI
4. Comment le faire fonctionner :  https://youtu.be/ME-eiYvbgCg
<iframe width="800" height="400" src="https://www.youtube.com/embed/IhYHzKHH0CQ" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>


### Votre rôle dans l'univers Precious Plastic
| Votre badge  |  Votre mission |
|----------|----------------------|
| <img src="../assets/universe/badge-workspace.png" width="150"/>        |  __Atelier__ <br> Un atelier Precious Plastic  est un endroit où le plastique est transformé à partir de déchets en matériaux ou produits de valeur. Il existe cinq espaces de travail différents : Broyeur, Extrusion, Sheetpress, Injection et Mix. |

## 👋 Partager

Vous avez construit votre espace. Super ! Nous apprenons tous ensemble, alors assurez-vous de partager tous les hacks ou développements qui vous ont aidé à améliorer votre espace 🙂 

<b>Vous souhaitez partager vos commentaires, discuter de l'espace de travail Injection ou en savoir plus sur la communauté ? 
Rendez-vous sur le [Discord](https://discordapp.com/invite/p92s237). Ici, nous répondons aux questions et donnons des conseils sur la configuration de votre espace de travail et sur son fonctionnement. </b>
