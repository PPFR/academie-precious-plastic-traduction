---
id: spaces
title: Espaces
sidebar_label: Intro
---


<style>
:root {
  --highlight: #37b4a3;
  --hover: #37b4a3;
}
</style>

![Community Badges](assets/comm_badges.jpg)

# Espaces : Ateliers Precious Plastic

Les espaces de travail sont les éléments constitutifs des ateliers Precious Plastic. 
Chaque espace prend en charge un élément du processus de recyclage, pour pouvoir être concentré et maîtriser un aspect à la fois. Différents espaces sont interconnectés et dépendent les uns des autres, collaborant et partageant les connaissances, le plastique et l'expertise pour s'attaquer au problème des déchets plastiques localement. 

Pour vous aider à démarrer du bon pied, nous avons conçu, construit et testé chaque espace Precious Plastic dans un souci de simplicité, de productivité et de qualité de vie. Dans les sections suivantes, vous trouverez un guide étape par étape avec des plans d'atelier, des modèles CAO et des recommandations d'outils pour vous aider à planifier les différents éléments de votre espace de recyclage du plastique.


### Lorsque vous êtes dans votre atelier, il y a quelques points généraux à garder à l'esprit : 

<b>1. Travaillez ensemble. </b> Trouvez un coéquipier. Ou deux. Avoir quelqu'un avec qui partager les tâches rend le travail plus amusant et plus facile à gérer. Il est préférable que vous et vos coéquipiers ayez des compétences complémentaires - ingénieur.e + designer + administrateur-trice, par exemple. 

<b>2. Personnalisez.</b> Votre contexte (localisation, espace, produits) aura une incidence sur la façon dont vous configurez votre atelier Precious Plastic. Utilisez les plans ici comme point de départ et n'hésitez pas à les adapter pour mieux répondre à vos besoins. 


<b>3. Restez simple.</b> Nous vous recommandons de commencer par les éléments les plus élémentaires de votre espace (machine, établi, outils de base) et de vous équipez au fur et à mesure que vous maîtrisez les machines et techniques de recyclage. De cette façon, vous construirez et n'achèterez que ce qui correspond à vos besoins. 

<b>4. Accueil.</b> Votre atelier Precious Plastic est plus qu'un centre de recyclage - c'est un outil de changement culturel. Soignez les petits détails pour en faire un endroit agréable pour vous, votre équipe et vos visiteurs. 

Ok, prêt à commencer ? **Consultez les chapitres suivants pour savoir comment configurer votre atelier  Precious Plastic.**

