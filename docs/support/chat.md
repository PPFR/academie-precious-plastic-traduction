---
id: chat
title: Chat
sidebar_label: Chat
---

<style>
:root {
  --highlight: #e1e1e1;
  --links: rgb(131, 206, 235);
  --hover: rgb(131, 206, 235);
}
</style>

# Chat & Support
Precious Plastic fait partie de [One Army](https://onearmy.earth), un groupe de personnes qui tentent de s'attaquer aux problèmes globaux. Nous utilisons Discord pour discuter, apprendre les uns des autres et répondre aux questions. Outre les aspects pratiques, c'est aussi un endroit idéal pour rencontrer des personnes partageant les mêmes idées et s'inspirer d'autres projets. Le plastique, c'est sympa, mais il y a plus à découvrir! Jetez un coup d'œil, [rejoignez notre Discord de plus de 10 000 personnes](https://discordapp.com/invite/rnx7m4t) et entrez en contact avec la communauté!

<iframe src="https://discordapp.com/widget?id=586676777334865928&theme=dark" width="350" height="300" allowtransparency="true" frameborder="0"></iframe>




<br>
<br>

<br>
<br>
<br>

![Discord](assets/support/onearmy-banner.png)
