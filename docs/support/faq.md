---
id: faq
title: FAQ
sidebar_label: FAQ
---
<style>
:root {
  --highlight: #e1e1e1;
  --links: rgb(131, 206, 235);
  --hover: rgb(131, 206, 235);
}
</style>

# Questions fréquentes

## 💬 Généralités

<details><summary><b>Comment débuter?</b></summary>
<p>

Notre [site principal](https://preciousplastic.com/) donne une vision globale de Precious Plastic. Les tutoriaux vidéo et les informations de l'[Academie](https://plastic.chiquette.fr/academy/intro.html) peuvent vous aider à apprendre comment commencer à recycler les plastiques. La [Carte](https://community.preciousplastic.com/map) et [Discord](https://discord.com/invite/rnx7m4t) peuvent vous mettre en relation avec les recycleurs de votre territoire. Pensez à épingler votre présence sur la carte quand vous serez actifs!
</p>
</details>

<details><summary><b>Comment puis-je partager avec Precious Plastic?</b></summary>
<p>

Precious Plastic est un projet open-source. Cela signifie que vous pouvez partager votre savoir avec le reste de la communauté pour aider à transmettre les bases du recyclage. Il y a deux façons de faire:
- Les «How-Tos», qui sont des manuels pas à pas qui montrent les trucs et astuces que vous avez découverts pendant votre voyage dans le monde du recyclage. Qu'il s'agisse du hack d'une machine, un nouveau produit, moule, hébergement d'évènement, ou un peu de tout, nous souhaiterions en entendre parler!
- Via [Discord](https://discord.com/invite/rnx7m4t), partagez votre savoir-faire pour aider les gens qui posent des questions. La meilleure chose est d'aider les nouveaux recycleurs à éviter des erreurs que vous avez pu faire de telle sorte qu'un recyclage de meilleure qualité puisse arriver plus souvent. Nous sommes très heureux que les gens partagent leur savoir et en sont remerciés.

</p>
</details>

<details><summary><b>Comment puis-je collaborer avec Precious Plastic?</b></summary>
<p>

Precious Plastic a une page dédiée à la [collaboration](https://preciousplastic.com/collabs.html). C'est sur ce document que nous réfléchissons avec nos partenaires à des actions sur le long terme pour créer des projets de recyclage partout dans le monde; vous pourrez en apprendre plus en la lisant. Si vous souhaitez commencer un Atelier, vous pouvez également consulter nos [kits de démarrage](https://preciousplastic.com/archived/starterkits/overview.html).

</p>
</details>

<details><summary><b>J'ai une idée pour Precious Plastic, où puis-je la partager?</b></summary>
<p>

Toutes les idées sont bienvenues et appréciées. Utilisez Discord pour éavluer vos idées initiales avec la communauté, nous avons des canaux pour les machines, les produits, les collections et bien plus.
</p>
</details>

<details><summary><b>Je souhaiterais travailler pour Precious Plastic, comment puis-je postuler?</b></summary>
<p>

Nous sommes une petite équipe, basée au Portugal, travaillant à distance. Quand nous avons des offres d'emploi ou des opportunités nous les postons sur le Discord ou sur les médias sociaux. Si vous souhaitez travailler ou effectuer un stage pour apprendre le recyclage, jetez un oeil à la Carte et demandez aux espaces de travail locaux pour des opportunités d'emploi dans votre zone.
</p>
</details>

<details><summary><b>Qu'est ce que l'Open Source?</b></summary>
<p>

"L'Open source" se réfère à quelque chose de modificable et de partageable par tous - son contenu est "ouvert". Le terme nous vient de l'industrie logicielle, l'open source étant du code source lisible par tous et modifiable. Aujourd'hui, ce terme peut aussi s'appliquer au hardware, aux produits, à la recherche, au design, en fait tout ce qui peut être collaboratif et non-propriétaire!
</p>
</details>

<details><summary><b>Quel est l'objectif de Precious Plastic?</b></summary>
<p>

L'objectif de Precious Plastic est de fournir une solution globale au problème des déchets plastiques. Nous fournissons des plans de machines, des conceptions de produits, des connaissances sur le plastique et l'industrie, des modèles commerciaux et un écosystème complet qui peut être adopté par quiconque souhaite prendre le problème en main. Tout ce que nous apprenons et publions en ligne est gratuit et ouvert pour que chacun puisse l'utiliser et le modifier.
</p>
</details>

<details><summary><b>Comment peut-on contributer?</b></summary>
<p>

Nous aimerions que vous rejoigniez notre communauté afin d'intégrer l'univers Precious Plastic. Nous travaillons sur des processus plus décentralisés et collaboratifs pour développer Precious Plastic, consultez notre Discord pour savoir comment vous pouvez contribuer. Si vous souhaitez nous apporter une contribution financière, vous pouvez devenir un [Patreon](https://www.patreon.com/one_army) ou visiter la page [support](https://preciousplastic.com/support.html) pour trouver différentes manières d'aider.
</p>
</details>



## ⚙️ Machines
<details><summary><b>Où se trouve le Kit de Téléchargement?</b></summary>
<p>

Le [Kit de Téléchargement](https://community.preciousplastic.com/academy/download) se trouve dans l'Academy. Il contient tout le nécessaire pour commencer à recycler du plastique, en plus de chaque  [Starterkit](https://preciousplastic.com/archived/starterkits/overview.html) ayant un kit de démarrage associé.
</p>
</details>

<details><summary><b>Vendez-vous les machines?</b></summary>
<p>

Non. Nous ne vendons pas de machines. Nous préfèrons que les gens les construisent localement ou fassent appel à un atelier de fabrication. Pur être très clair, NOUS NE VENDONS PAS DE MACHINES
:)
</p>
</details>

<details><summary><b>Où peut-on acheter des machines?</b></summary>
<p>

Vous pouvez acheter des machines ou des pièces sur le [Bazar Precious Plastic](https://bazar.preciousplastic.com/). Essayez autant que possible d'acheter localement afin de réduire les émisions liées au transport. Jetez un oeil à la Map sur notre plateforme Communautaire, ou sur le Bazar pour voir ce qui est disponible autour de vous.

</p>
</details>

<details><summary><b>Peut-on construire les machines et les vendre?</b></summary>
<p>

Oui, nous adorions que vous fassiez cela. Plus le nombre de personnes qui recyclent est grand, le mieux c'est. Le profit te revient mon ami! Vous pouvez vendre des machines via le [Bazar Precious Plastic](https://bazar.preciousplastic.com/).
</p>
</details>

<details><summary><b>Les machines sont-elles sûres à utiliser?</b></summary>
<p>

Si vous les construisez selon notre conception, cela ne devrait pas poser de problème. Dans l'ensemble, elles sont sans danger à l'utilisation. Cependant, une machine comme le broyeur peut causer de sérieux dégâts. Vous pouvez toujours customiser les machines pour les rendre plus sûres. Pour des astuces concernant la sécurité, jetez un oeil aux [How-tos](https://community.preciousplastic.com/how-to). Dans tous les cas, soyez toujours attentifs s'il vous plaît!
</p>
</details>

<details><summary><b>Quels sont les prérequis? (Espace, outils/équipement, budget, temps, personnes))</b></summary>
<p>

Ils diffèrent selon la machine, le meilleur endroit pour trouver cette information étant la section [Construire](https://community.preciousplastic.com/academy/build) de l'Academy. Si vous avez d'autres questions, posez les à la communauté sur  [Discord](https://discord.com/invite/rnx7m4t).

</p>
</details>

<details><summary><b>Quelle est la consommation électrique des machines?</b></summary>
<p>

Vous pouvez retrouver tous les détails techniques de fonctionnement sur les pages [starterkit](https://preciousplastic.com/starterkits/showcase/shredder) de notre site web.
</p>
</details>

<details><summary><b>J'ai une question technique, où puis-je obtenir de l'aide?</b></summary>
<p>

Vous pouvez utiliser [Discord](https://discord.com/invite/rnx7m4t). On y retrouve beaucoup de membres talentueux et informés ainsi que des ingénieurs de la communauté, certains pourront certainement vous aider!
</p>
</details>

## 🥤 Plastiques

<details><summary><b>Qu'est ce que le plastique?</b></summary>
<p>

Allez voir la section [Plastique](https://community.preciousplastic.com/academy/plastic/basics) de l'Academy.
</p>
</details>

<details><summary><b>Peut-on utiliser n'importe quel type de plastique avec les machines Precious Plastic?</b></summary>
<p>

Pas du tout. Nous recommandons d'utiliser les plastiques suivants: PEHD (#2), PP (#5) et PS (#6), car ils sont les plus adaptés aux machines Precious Plastic. Demandez au reste de la communauté à propos des autres types.
</p>
</details>

<details><summary><b>Est-il dangereux de travailler avec du plastique?</b></summary>
<p>

De manière générale, travailler avec du plastique est sans danger tant que vous respectez les précautions nécessaires. Toutefois, chaque type de plastique a ses propres prérequis. Référez-vous à la section  [Sécurité and Fumées](https://community.preciousplastic.com/academy/plastic/safety) de l'Academy. Le PVC est toxique, ne l'utilisez pas.
</p>
</details>

<details><summary><b>Comment reconnaître le type d'un plastique?</b></summary>
<p>

Heuresement, la pièce de plastique en question est marquée avec un chiffre de  à 1 à 7. Si elle l'est, youpi! Vous pouvez facilement regarder en ligne ou sur notre section [Fondamentaux du Plastique](https://community.preciousplastic.com/academy/plastic/basics) pour rentrer davantage dans le détail. Si la marque est absente, ca devient un peu plus compliqué. Il existe des méthodes comme le test de densité, l'identification infra-rouge, ou encore le test de la flamme. Vous pouvez également apprendre les types de plastique couramment utilisés pour les produits communs (les produits chimiques sont par exemple stockés dans des bouteilles en PEHD). Vous pouvez découvrir tout ces sujets dans la partie  [Super Pro du Plastique](https://community.preciousplastic.com/academy/plastic/nerdy) de l'Academy.

</p>
</details>

<details><summary><b>Combien de fois peut-on réutiliser du plastique?</b></summary>
<p>

Le plastique est un matériau assez résistant et, tant qu'il est trié et nettoyé correctement, il peut, en théorie, être recyclé plusieurs fois. Cela dépend du type de plastique, mais l'industrie dit qu'il peut être recyclé environ 10 fois. L'objectif reste de fabriquer des articles durables pour éviter qu'ils ne se retrouvent pas dans le sol, les océans ou l'atmosphère aussi longtemps que possible.
</p>
</details>


<details><summary><b>Precious Plastic recycle-t-il le PET?</b></summary>
<p>

Nous n'avons pas de solutions pour le PET pour le moment, mais foncez sur le [Discord](https://discord.com/invite/rnx7m4t) et aidez-nous à développer des solutions novatrices pour traiter le PET.
</p>
</details>

## 🏓 Produits

<details><summary><b>Où pouvons-nous acheter des articles Precious Plastic?</b></summary>
<p>

Vous pouvez en acheter à la communauté Precious Plastic sur le [Bazar](https://bazar.preciousplastic.com/).
</p>
</details>

<details><summary><b>Vendez-vous des produits?</b></summary>
<p>

Non, nous ne vendons pas de produits ou articles.</p>
</details>

<details><summary><b>J'ai une idée pour un produit, pouvez-vous le fabriquer pour moi?</b></summary>
<p>

Non, pour l'instant nous ne pouvons pas vous aider avec votre design produit. Vous pouvez utiliser la [Map](https://community.preciousplastic.com/map) afin de trouver un atelier pour vous aider, ou discuter avec des gens sur [Discord](https://discord.com/invite/rnx7m4t).</p>
</details>


## 👨‍👩‍👦‍👦 Communauté

<details><summary><b>Comment peut-on trouver des personnes dans ma zone?</b></summary>
<p>

Utilisez la [Map](https://community.preciousplastic.com/map) sur la plateforme communautaire pour trouver des personnes autour de vous!
</p>
</details>

<details><summary><b>Comment peut-on s'impliquer?</b></summary>
<p>

Notre plus grand souhait est de vous voir rejoindre la communauté et devenir partie prenante de l'Univers Precious Plastic. Jetez un oeil à l'[Univers Expliqué](https://community.preciousplastic.com/academy/universe/universe) dans l'Academy. Si vous souhaitez contribuer financièrement, vous pouvez aller sur [Patreon](https://www.patreon.com/davehakkens).
</p>
</details>

<details><summary><b>J'ai collecté du plastique, où puis-je l'apporter?</b></summary>
<p>

Pour la collecte, jetez on oeil à [cette page](https://collect.preciousplastic.com/) pour des conseils et astuces. Pour trouver des personnes désireuses de reprendre votre plastique, regardez la [Map](https://community.preciousplastic.com/map). Il y a des ateliers dans le monde entier, donc, avec un peu de chance, vous devriez pouvoir trouver un point de collecte dans votre zone.</p>
</details>

<details><summary><b>Peut-on utiliser le nom Precious Plastic?</b></summary>
<p>

Oui vous pouvez! Jetez un oeil aux conseils pour le branding et le style sur l'Academy, section [Style guides](https://community.preciousplastic.com/academy/universe/branding).
</p>
</details>

<details><summary><b>Qu'est ce qu'un atelier Vérifié et comment en devenir un?</b></summary>
<p>

Les ateliers Precious Plastic Vérifiés sont les projets qui poussent Precious Plastic à un tout autre niveau. Jetez un oeil au [Programme Communautaire](https://community.preciousplastic.com/academy/guides/community-program) sur l'Academy pour en savoir plus.
</p>
</details>

<details><summary><b>Quels sont les différents types d'épingle sur la carte?</b></summary>
<p>

Il y a 5 épingles différentes sur la carte: Atelier [Magasin de machines](https://preciousplastic.com/archived/starterkits/showcase/machine-shop.html), [Point de Collecte](https://preciousplastic.com/archived/starterkits/showcase/collection-point.html), [Point Communautaire](https://preciousplastic.com/archived/starterkits/showcase/community-point.html) and Membre (Envieux de démarrer). Il y a 5 types d'Ateliers différents: [Extrusion](https://preciousplastic.com/archived/starterkits/showcase/extrusion.html), qui ne comporte qu'une machine à extrusion; [Injection](https://preciousplastic.com/archived/starterkits/showcase/injection.html), qui ne comporte qu'une machine à Injection; [Broyeur](https://preciousplastic.com/archived/starterkits/showcase/shredder.html), qui ne comporte qu'un broyeur; [Presse à plaques](https://preciousplastic.com/archived/starterkits/showcase/sheetpress.html), qui ne comporte qu'une Presse à plaques et [Mix](https://preciousplastic.com/archived/starterkits/showcase/mix.html), une combinaison de n'importe quelles machines. L'épingle "Envie de Démarrer" est pour les personnes qui veulent se lancer dans leur zone et qui cherchent des personnes pour commencer un atelier/magasin/point communautaire/de collecte.
</p>
</details>


## 💰 Business

<details><summary><b>Dans quelle mesure Precious Plastic est-il réalisable en tant qu'entreprise?</b></summary>
<p>

Il existe de nombreuses manières de générer de l'argent avec Precious Plastic. Vous pouvez trouver une large variété de façons via notre page [Starterkits](http://preciousplastic.com/archived/starterkits/overview.html). Comme n'importe quelle entreprise, il peut être compliqué de démarrer, mais beaucoup d'ateliers à travers le monde sont des devenus des succès. Jetez un oeil à la sectiob [Business](https://community.preciousplastic.com/academy/business) de l'Academy pour plus d'information.
</p>
</details>

<details><summary><b>Quels sont les frais de démarrage d'un atelier Precious Plastic?</b></summary>
<p>

Les coûts de démarrage varient d'un endroit à un autre selon l'échelle de l'entreprise que vous voulez mettre en place. Vous pouvez trouver des estimations sur la page [Starterkits](http://preciousplastic.com/archived/starterkits/overview.html).
</p>
</details>

<details><summary><b>Comment vendre mes produits/machines?</b></summary>
<p>

Nous avons le Bazar Precious Plastic! C'est un marché en ligne pour la Communauté Precious Plastic pour vendre n'importe quel objet en lien avec Precious Plastic. Vous pouvez créer un compte et commencer à vendre des articles directement. Allez lire les recommendations et parcourir le [Bazar](https://bazar.preciousplastic.com/).
</p>
</details>

<details><summary><b>Comment définir un prix pour mes articles?</b></summary>
<p>

Pour des estimations de prix, et davantage de conseils business, jetez un oeil à la section [Business](https://community.preciousplastic.com/academy/business) et [Calculateur d'Atelier](https://community.preciousplastic.com/academy/business/workspacecalculator) dans l'Academy.
</p>
</details>

## 👩‍💻 Plateforme Communautaire

<details><summary><b>J'ai oublié mon mot de passe, login, ou les deux!</b></summary>
<p>

Si vous avez oublié votre mot de passe, vous pouvez utiliser le lien "Mot de passe oublié" sur la [Page d'accueil](https://community.preciousplastic.com/sign-in).
</p>
</details>

<details><summary><b>Comment validez-vous les nouveaux Espaces sur la carte?</b></summary>
<p>

Une équipe est chargée de passer en revue tous les nouveaux Ateliers et Points de Collecte ou Communautaire, afin de vérifier qu'ils répondent aux exigences requises pour interagir au sein de l'univers Precious Plastic pour garder la communauté active and pertinente.

</p>
</details>

<details><summary><b>Qu'est ce que le Programme Communautaire?</b></summary>
<p>

Le programme communautaire vise à aider et à récompenser les personnes et les projets qui contribuent le plus à la croissance du mouvement Precious Plastic. Il existe différents niveaux, exigences et récompenses. Jetez un coup d'œil ci-dessous pour comprendre comment vous pouvez en faire partie. Pour plus d'informations, consultez le chapitre [Community Program](https://community.preciousplastic.com/academy/guides/community-program) de l'Academy.
</p>
</details>

<details><summary><b>Existe-t-il une version traduite de l'Academy?</b></summary>
<p>

(NdT: Ceci est une traduction de la version EN de l'Academy Precious Plastic)
La traduction est demandée depuis les débuts mais il a été difficile à mettre en place. Nous avons du contenu dans de nombreux formats et plateformes différents. De plus, l'interface d'une communauté dans plusieurs langues différentes peut être plus déroutante qu'utile. Pour ces raisons, nous avons mis la traduction en attente. Mais pour l'instant, vous pouvez accéder aux canaux REGIONS sur [Discord](https://discord.com/invite/rnx7m4t) pour discuter dans votre langue.
</p>
</details>

## 🔆 À proprs du projet

<details><summary><b>Qui est derrière ce projet?</b></summary>
<p>

Precious Plastic a été initié par Dave Hakkens en 2013. L'équipe a évolué de multiples fois au fil des années, principalement pour travailler sur de nouvelles versions. Désormais, il y a une petite équipe consacrée  à temps plein sur le projet. Cette [équipe](https://preciousplastic.com/people/team.html) aide la communauté à grandir, et continuer le développement de produits et machines. Pour en savoir plus sur le projet, lisez notre [histoire](https://preciousplastic.com/about/history.html).

</p>
</details>

<details><summary><b>Pourquoi tout partager gratuitement?</b></summary>
<p>

Parce que nous sommes convaincus qu'ouvrir l'accès à la connaissance est la manière la plus efficace pour rapidement construire des solutions efficaces. De la base vers le haut, distruptif et axé sur la communauté.</p>
</details>

<details><summary><b>Creative commons, copyright et open source?</b></summary>
<p>

Jetez un oeil à notre page [open source](https://preciousplastic.com/about/open-source) pour plus d'information. Pour faire simple, tout est partagé sous la licence publique Creative Commons, vous êtes donc libre d'utiliser notre contenu!
</p>
</details>

<details><summary><b>Générez-vous des revenus avec ce projet?</b></summary>
<p>

Nous avons quelques sources de revenus avec le projet. Le principal étant les Collabs que nous réalisons avec des gros partenaires. Ces Collabs permettent de financer la construction et la maintenance des plateformes en ligne ainsi que la R&D de Precious Plastic. Nous sommes également financés par une combinaison de subventions, récompenses, donations et commissions du Bazar. Si vous souhaitez contribuer, allez sur la page [Support](https://preciousplastic.com/support.html).
</p>
</details>

## 💬 Contact

<details><summary><b>Je souhaiterais visiter votre atelier</b></summary>
<p>

Nous aimerions, mais ce n'est pas possible pour le moment :( Si vous voulez voir un atelier  Precious Plastic en action, allez voir sur la  Map et rensignez-vous sur les possibilités d'accueil des structures alentours.
</p>
</details>

<details><summary><b>Je suis bloqué sur un truc. Puis-je vous contacter pour vous poser une question?</b></summary>
<p>

[Discord](https://discord.com/invite/rnx7m4t) est le meilleur endroit pour poser vos questions et trouver des réponses de la communauté! Si vous êtes un supporter Patreon, vous avez accès au Patreon Helpdesk sur Discord.
</p>
</details>

<details><summary><b>Je réalise un projet de recherche et souhaiterais poser des questions sur le projet?</b></summary>
<p>

Malheuresement, nous ne pouvons pas répondre directement à vos questions puisque nous sommes une petite équipe. Toute notre documentation se trouve sur l'Academy, et vous pouvez parler à plein de personnes actives sur notre [Discord](https://discord.com/invite/rnx7m4t). Une autre bonne manière d'obtenir des informations est de contacter un espace local sur la [Map](https://community.preciousplastic.com/map).
</p>
</details>

<details><summary><b>Je suis intéressé pour donner/financer votre projet.</b></summary>
<p>

Nous adorerions discuter avec vous! Allez sur la page [Support](https://preciousplastic.com/support.html) pour découvrir les façons dont vous pouvez aider.</p>
</details>

### Ça fait beaucoup de questions! La façon la plus rapide d'obtenir une réponse que vous n'avez pas trouvée ici, est de foncer sur Discord. Vous trouverez un récapitulatif des canaux disponibles dans le chapitre suivant.
